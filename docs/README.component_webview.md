# component-webview

A prototype front end to display component data.

To run locally:

``` bash
podman run \
    --rm \
    --publish 8080:8080 \
    --volume .:/code:z \
    --env COMPONENTS_JSON_URL=... \
    quay.io/cki/component-webview:production
```
