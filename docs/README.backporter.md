# Backporter Webhook

## Purpose

This webhook attempts to automatically backport commits for a given JIRA Issue, with commit IDs
either provided via the JIRA Issue's Commit Hashes field or from an auto-detected lead stream
merge request's commits.

## Reporting

- Label prefix: n/a
- Comment header: n/a

This webhook does not run on GitLab Merge Requests, but rather on JIRA Issues, and then generates
merge requests as its output. The hook will leave `kwf-backport-success` or `kwf-backport-fail`
labels on the relevant JIRA Issue, as well as a comment in the JIRA Issue with further details.

## Triggering

This webhook actually has multiple trigger mechanisms. One of them is populating the Commit Hashes
field of a JIRA Issue, which also has a valid Fix Version set, and does not already have a merge
request linked, or a `kwf-backport-*` label on it. The other is via a cron job that looks for JIRA
Issues in non-lead-streams that have lead stream Issues linked to them with ready merge requests,
from which commits can be extracted and backported.

## Manual runs

You can run the webhook manually on a JIRA Issue with the command:

    python3 -m webhook.utils.backporter -j RHEL-101

Additionally, you can use the -T variable to run in pure Testing mode, where current issue state is
ignored, no data will be posted to the JIRA Issue, and no merge request will be created, but likely
results of the effort will be shown locally.

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

In addition, this webhook requires the following environment variable to be set:

- JIRA_TOKEN_AUTH: a [JIRA Personal Access Token](https://issues.redhat.com/secure/ViewProfile.jspa?selectedTab=com.atlassian.pats.pats-plugin:jira-user-personal-access-tokens)
