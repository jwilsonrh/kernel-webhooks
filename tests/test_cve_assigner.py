"""Tests for zstream_backporter."""
from unittest import mock

from cki_lib import owners
from cki_lib import yaml

from tests.helpers import KwfTestCase
from webhook.defs import JiraField
from webhook.rhissue import RHIssue
from webhook.utils import cve_assigner


@mock.patch.dict('os.environ', {'PUBLIC_INBOX': '/src/linux-cve-announce',
                                'OWNERS_YAML': '/path/to/owners.yaml',
                                'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'REQUESTS_CA_BUNDLE': '/path/to/certs'
                                })
class TestCVEAssigner(KwfTestCase):
    """Tests for the various cve_assigner functions."""

    def test_get_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-c", "CVE-1984-10001",
                                     "-p", "/src/linux-cve-announce2"]):
            args = cve_assigner._get_parser_args()
        self.assertEqual(args.public_inbox, '/src/linux-cve-announce2')
        self.assertEqual(args.cve, 'CVE-1984-10001')
        self.assertEqual(args.owners_yaml, '/path/to/owners.yaml')
        self.assertEqual(args.sentry_ca_certs, '/path/to/certs')
        with mock.patch("sys.argv", ["_get_parser_args", "-c", "CVE-1984-10002",
                                     "-o", "/a/b/c/owners.yaml"]):
            args = cve_assigner._get_parser_args()
        self.assertEqual(args.public_inbox, '/src/linux-cve-announce')
        self.assertEqual(args.cve, 'CVE-1984-10002')
        self.assertEqual(args.owners_yaml, '/a/b/c/owners.yaml')
        self.assertEqual(args.sentry_ca_certs, '/path/to/certs')

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_get_patch_data(self):
        mock_args = mock.Mock(spec_set=['cve', 'public_inbox'])
        mock_args.cve = 'CVE-1984-1234'
        mock_args.public_inbox = '/src/linux-cve-announce'
        mock_commit = mock.Mock(spec_set=['hexsha'])
        mock_commit.hexsha = 'abcdef0123456789'
        mbox_data = ("Issue introduced in 4.8 with commit abcd12345678 and fixed in 6.8 with "
                     "commit 13241324abcd\n"
                     "will be updated if fixes are backported, please check that for the most\n"
                     "up to date information about this issue.\n"
                     "\n"
                     "Affected files\n"
                     "==============\n"
                     "The file(s) affected by this issue are:\n"
                     "	drivers/misc/foobar.c\n"
                     "\n"
                     "Mitigation\n"
                     "==========\n")
        with mock.patch('builtins.open', mock.mock_open(read_data=mbox_data)):
            files, sha = cve_assigner.get_patch_data(mock_args, mock_commit)
            self.assertEqual(files, ['drivers/misc/foobar.c'])
            self.assertEqual(sha, '13241324abcd')

        mbox_data = ("Fixed in 6.8 with commit 13241324fedc\n"
                     "will be updated if fixes are backported, please check that for the most\n"
                     "up to date information about this issue.\n"
                     "\n"
                     "Affected files\n"
                     "==============\n"
                     "The file(s) affected by this issue are:\n"
                     "	drivers/misc/widget.c\n"
                     "\n"
                     "Mitigation\n"
                     "==========\n")
        with mock.patch('builtins.open', mock.mock_open(read_data=mbox_data)):
            files, sha = cve_assigner.get_patch_data(mock_args, mock_commit)
            self.assertEqual(files, ['drivers/misc/widget.c'])
            self.assertEqual(sha, '13241324fedc')

        mbox_data = ("Fixed in 6.8-rc3 with commit 13241324fedc\n"
                     "will be updated if fixes are backported, please check that for the most\n"
                     "up to date information about this issue.\n"
                     "\n"
                     "Affected files\n"
                     "==============\n"
                     "The file(s) affected by this issue are:\n"
                     "	drivers/misc/widget.c\n"
                     "\n"
                     "Mitigation\n"
                     "==========\n")
        with mock.patch('builtins.open', mock.mock_open(read_data=mbox_data)):
            files, sha = cve_assigner.get_patch_data(mock_args, mock_commit)
            self.assertEqual(files, ['drivers/misc/widget.c'])
            self.assertEqual(sha, '13241324fedc')

    @mock.patch('git.Repo')
    def test_search_for_cve_info(self, mock_repo):
        mock_repo.git.log.return_value = ('abc1234 CVE-1984-1999: foo: fix the flaw\n'
                                          'def5678 REJECTED: CVE-1984-1999: foo: fix the flaw\n')
        mock_commit = mock.Mock()
        mock_repo.commit.return_value = mock_commit
        commit, rej = cve_assigner.search_for_cve_info(mock_repo, 'CVE-1984-1999')
        self.assertEqual(commit, mock_commit)
        self.assertTrue(rej)

    @mock.patch('webhook.utils.cve_assigner.make_rhissues')
    @mock.patch('webhook.utils.cve_assigner.fetch_issues')
    def test_parse_jira_data(self, mock_fi, mock_mrhi):
        mock_args = mock.Mock(spec_set=['cve'])
        mock_args.cve = 'CVE-1984-1999'
        mock_sst = mock.Mock(spec_set=['jira_component'])
        mock_sst.jira_component = 'kernel / Platform Enablement / x86_64'
        mock_issue = self.make_jira_issue('RHEL-102')
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, mrs=None)
        mock_fi.return_value = []
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='INFO') as logs:
            cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("No jira issues found for CVE-1984-1999", logs.output[-1])
        mock_fi.return_value = [mock_issue]
        mock_mrhi.return_value = [mock_rhissue]
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn(("Issue RHEL-102 component mismatch. Jira: 'kernel-rt' != "
                           "Docs: 'kernel-rt / Platform Enablement / x86_64'"), logs.output[-2])
        mock_issue = self.make_jira_issue('RHEL-101')
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, mrs=None)
        mock_fi.return_value = [mock_issue]
        mock_mrhi.return_value = [mock_rhissue]
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("Issue RHEL-101 components match", logs.output[-2])
        mock_sst.jira_component = 'kernel / foo / bar'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='INFO') as logs:
            cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("Issue RHEL-101 component mismatch", logs.output[-2])
            self.assertIn(f"Issues to update: ['{mock_issue.key}']", logs.output[-1])
        mock_issue = self.make_jira_issue('RHEL-103')
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, mrs=None)
        mock_fi.return_value = [mock_issue]
        mock_mrhi.return_value = [mock_rhissue]
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.parse_jira_data(mock_args, mock_sst)
            self.assertIn("Issue RHEL-103 is Closed", logs.output[-2])

    def test_find_zstream_maintainer_jira_user(self):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: RHEL 8.11 Kernel Maintainer\n"
                       "   labels:\n"
                       "     name: redhat\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: Shadow Man\n"
                       "       email: shadowman@redhat.com\n"
                       "       gluser: shadowman\n"
                       "   jiraComponent: kernel / core\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - Makefile.rhelver\n")
        parser = owners.Parser(yaml.load(contents=owners_yaml))
        mock_jira = mock.Mock()
        mock_jira_user = mock.Mock()
        mock_jira_user.name = 'rhn-support-shadowman'
        mock_jira.search_users.return_value = [mock_jira_user]
        mock_rhi = mock.Mock()
        mock_rhi.jira = mock_jira
        mock_rhi.ji_fix_version.major = 8
        mock_rhi.ji_fix_version.minor = 11

        retval = cve_assigner.find_zstream_maintainer_jira_user(mock_rhi, parser)
        self.assertEqual('rhn-support-shadowman', retval.name)

        mock_jira.search_users.return_value = []
        retval = cve_assigner.find_zstream_maintainer_jira_user(mock_rhi, parser)
        self.assertIsNone(retval)

    def test_get_valid_components(self):
        mock_jira = mock.Mock()
        comp1 = mock.Mock()
        comp1.name = 'kernel-rt / foo'
        comp2 = mock.Mock()
        comp2.name = 'kernel-rt / bar'
        mock_jira.project_components.return_value = [comp1, comp2]
        comp_list = cve_assigner.get_valid_components('kernel-rt', mock_jira)
        self.assertCountEqual(comp_list, ['kernel-rt / foo', 'kernel-rt / bar'])

    @mock.patch('cki_lib.misc.is_production_or_staging')
    @mock.patch('jira.Issue.update')
    def test_update_jira_issue(self, mock_update, mock_prod):
        mock_issue = self.make_jira_issue('RHEL-101')
        mock_prod.return_value = False
        cve_assigner._update_jira_issue(mock_issue, {}, None, {})
        mock_update.assert_not_called()

        mock_prod.return_value = True
        cve_assigner._update_jira_issue(mock_issue, {}, None, {})
        mock_update.assert_called_with(fields={}, update={})

        mock_jira_user = mock.Mock()
        mock_jira_user.name = 'rhn-support-shadowman'
        cve_assigner._update_jira_issue(mock_issue, {}, mock_jira_user, {})
        mock_update.assert_called_with(fields={}, assignee={'name': 'rhn-support-shadowman'},
                                       update={})

    @mock.patch('webhook.utils.cve_assigner.find_zstream_maintainer_jira_user')
    @mock.patch('webhook.utils.cve_assigner.get_valid_components')
    @mock.patch('webhook.utils.cve_assigner._update_jira_issue')
    def test_update_jira_data(self, mock_update, mock_components, mock_zmaint):
        mock_components.return_value = ['kernel-rt / foo / bar']
        mock_zmaint.return_value = None
        mock_issue = self.make_jira_issue('RHEL-101')
        mock_jira = mock.Mock()
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, jira=mock_jira, mrs=None)

        mock_subsystem = mock.Mock(spec_set=['jira_component'])
        mock_subsystem.jira_component = 'kernel / foo / bar'
        rhissues = [mock_rhissue]
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        expected_fields = {JiraField.components: [{"name": "kernel / foo / bar"}],
                           JiraField.Commit_Hashes: "abcd1234abcd"}
        all_checkboxes = [{"id": "32051"}, {"id": "32052"}, {"id": "32053"},
                          {"id": "32054"}, {"id": "32055"}, {"id": "32850"}]
        expected_update = {JiraField.Reset_Contacts: [{'set': all_checkboxes}]}
        mock_update.assert_called_with(mock_issue, expected_fields, None, expected_update)

        mock_issue = self.make_jira_issue('RHEL-102')
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, jira=mock_jira, mrs=None)
        rhissues = [mock_rhissue]
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        expected_fields = {JiraField.components: [{"name": "kernel-rt / foo / bar"}]}
        mock_update.assert_called_with(mock_issue, expected_fields, None, expected_update)

        mock_fv = mock.Mock()
        mock_update.reset_mock()
        mock_fv.name = "rhel-1.25.0"
        mock_issue.fields.fixVersions = [mock_fv]
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, jira=mock_jira, mrs=None)
        rhissues = [mock_rhissue]
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        mock_update.assert_not_called()

        mock_fv.name = "rhel-8.6.0.z"
        mock_issue.fields.fixVersions = [mock_fv]
        mock_rhissue = RHIssue.new_from_ji(ji=mock_issue, jira=mock_jira, mrs=None)
        rhissues = [mock_rhissue]
        most_checkboxes = [{"id": "32052"}, {"id": "32053"}, {"id": "32055"}, {"id": "32850"}]
        expected_update = {JiraField.Reset_Contacts: [{'set': most_checkboxes}]}
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        expected_fields = {JiraField.components: [{"name": "kernel-rt / foo / bar"}]}
        mock_update.assert_called_with(mock_issue, expected_fields, None, expected_update)

        mock_issue.fields.assignee.emailAddress = 'kernel-mgr@redhat.com'
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        expected_update = {JiraField.Reset_Contacts: [{'set': all_checkboxes}]}
        mock_update.assert_called_with(mock_issue, expected_fields, None, expected_update)

        mock_jira_user = mock.Mock()
        mock_jira_user.name = 'rhn-support-shadowman'
        mock_zmaint.return_value = mock_jira_user
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        expected_update = {JiraField.Reset_Contacts: [{'set': most_checkboxes}]}
        expected_fields = {JiraField.components: [{"name": "kernel-rt / foo / bar"}],
                           JiraField.Pool_Team: [{'id': '15230'}]}
        mock_update.assert_called_with(mock_issue, expected_fields,
                                       mock_jira_user, expected_update)

        mock_components.return_value = []
        cve_assigner.update_jira_data(rhissues, mock_subsystem, 'abcd1234abcd', 'mock_owners')
        mock_update.assert_called_with(mock_issue, {JiraField.Pool_Team: [{'id': '15230'}]},
                                       mock_jira_user, expected_update)

    @mock.patch('webhook.utils.cve_assigner.get_owners_parser')
    def test_find_best_matching_component(self, mock_gop):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       gluser: user4\n"
                       "       restricted: false\n"
                       "   jiraComponent: kernel / foo\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n"
                       " - subsystem: Some More Specific Subsystem\n"
                       "   labels:\n"
                       "     name: SomeMSSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       gluser: user4\n"
                       "       restricted: false\n"
                       "   jiraComponent: kernel / foo / bar\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        parser = owners.Parser(yaml.load(contents=owners_yaml))
        subsystems = parser.get_matching_subsystems(['redhat/file.c'])

        subsystem = cve_assigner.find_best_matching_component(subsystems)
        self.assertEqual("kernel / foo / bar", subsystem.jira_component)

        subsystems = parser.get_matching_subsystems(['unknown/file.c'])
        subsystem = cve_assigner.find_best_matching_component(subsystems)
        self.assertIsNone(subsystem)

    @mock.patch('webhook.utils.cve_assigner.update_jira_data', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.cve_assigner.parse_jira_data', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.cve_assigner.find_best_matching_component')
    @mock.patch('webhook.utils.cve_assigner.get_patch_data')
    @mock.patch('webhook.utils.cve_assigner.search_for_cve_info')
    @mock.patch('webhook.utils.cve_assigner.get_owners_parser')
    @mock.patch('webhook.kgit.fetch_all', mock.Mock(return_value=True))
    @mock.patch('git.Repo', mock.Mock(return_value=True))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_main(self, mock_gop, mock_sfci, mock_gpd, mock_find_component):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       gluser: user4\n"
                       "       restricted: false\n"
                       "   jiraComponent: kernel / foobar\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        parser = owners.Parser(yaml.load(contents=owners_yaml))
        mock_gop.return_value = parser
        mock_args = mock.Mock(spec_set=['cve', 'public_inbox', 'owners_yaml'])
        mock_args.cve = ''
        mock_args.public_inbox = ''
        mock_commit = mock.Mock(spec_set=['hexsha', 'summary'])
        mock_commit.hexsha = 'abcdef012345'
        mock_commit.summary = 'CVE-1984-10001: fix the exploitable flaw'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='WARNING') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("No valid public inbox git found, aborting!", logs.output[-1])

        mock_args.public_inbox = '/src/linux-cve-announce'
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='WARNING') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("No valid CVE provided, aborting!", logs.output[-1])

        mock_args.cve = 'CVE-1984-10001'
        mock_sfci.return_value = None, True
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("Fetching latest upstream mbox data...", logs.output[-3])
            self.assertIn("CVE-1984-10001 was REJECTED upstream", logs.output[-2])
            self.assertIn("No commit found, exiting", logs.output[-1])

        mock_sfci.return_value = mock_commit, False
        mock_gpd.return_value = ['redhat/file.c'], 'abcd1234'
        mock_find_component.return_value = parser.get_matching_subsystems(['redhat/file.c'])[0]
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='DEBUG') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("Got subsystem of Some Subsystem for path(s) ['redhat/file.c']",
                          logs.output[-3])
            self.assertIn("Got UCID of abcd1234", logs.output[-2])
            self.assertIn("Jira Component mapping: 'kernel / foobar'", logs.output[-1])

        mock_gpd.return_value = ['unknown/file.c'], 'abcd1234'
        mock_find_component.return_value = None
        with self.assertLogs('cki.webhook.utils.cve_assigner', level='WARNING') as logs:
            cve_assigner.main(mock_args)
            self.assertIn("Unable to find a matching subsystem for paths ['unknown/file.c']",
                          logs.output[-1])
