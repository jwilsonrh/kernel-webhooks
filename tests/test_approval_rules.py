"""Tests for the approval rules classes."""
from copy import deepcopy
from unittest import mock

from tests.helpers import KwfTestCase
from tests.test_users import USERS
from webhook import approval_rules
from webhook.defs import ALL_MEMBERS_APPROVAL_RULE
from webhook.users import UserCache

X86_RULE = {'approvalsRequired': 1,
            'eligibleApprovers': USERS,
            'id': 'gid://gitlab/ApprovalMergeRequestRule/86',
            'name': 'x86',
            'sourceRule': None,
            'type': 'REGULAR'}

ORIGINAL_ALL_MEMBERS = {'approvalsRequired': 2,
                        'eligibleApprovers': [],
                        'id': 'gid://gitlab/ApprovalProjectRule/65432',
                        'name': ALL_MEMBERS_APPROVAL_RULE,
                        'type': 'ANY_APPROVER'}

ALL_MEMBERS_RULE = {'approvalsRequired': 1,
                    'eligibleApprovers': [],
                    'id': 'gid://gitlab/ApprovalMergeRequestRule/12345',
                    'name': ALL_MEMBERS_APPROVAL_RULE,
                    'sourceRule': ORIGINAL_ALL_MEMBERS,
                    'type': 'ANY_APPROVER'}


class TestApprovalRuleType(KwfTestCase):
    """Tests for the ApprovalRuleType enum."""

    def test_approvalruletype(self):
        """Returns the expected ApprovalRuleType."""
        rule_types = ('ANY_APPROVER', 'CODE_OWNER', 'REGULAR', 'REPORT_APPROVER')

        self.assertEqual(len(approval_rules.ApprovalRuleType), len(rule_types))
        for rule_type in rule_types:
            approval_rule_type = getattr(approval_rules.ApprovalRuleType, rule_type)
            self.assertIs(approval_rules.ApprovalRuleType[rule_type], approval_rule_type)


class TestApprovalRule(KwfTestCase):
    """Tests for the ApprovalRule class."""

    def test_init_regular(self):
        """Returns an ApprovalRule with useful properties."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        user_cache = UserCache(mock_graphql, namespace)
        input_dict = deepcopy(X86_RULE)
        approved_by_user = user_cache.get_by_dict(USERS[1])
        mr_approved_by = {approved_by_user}
        x86_rule = approval_rules.make_mr_approval_rule(input_dict, user_cache, mr_approved_by)

        self.assertIs(x86_rule.approved, True)
        self.assertEqual(len(x86_rule.approved_by), 1)
        self.assertEqual({auser for auser in x86_rule.eligible_approvers},
                         {user for user in user_cache.data.values()})
        self.assertEqual(x86_rule.gid, 'gid://gitlab/ApprovalMergeRequestRule/86')
        self.assertEqual(x86_rule.name, X86_RULE['name'])
        self.assertEqual(x86_rule.approvals_required, 1)
        self.assertIs(x86_rule.source_rule, None)
        self.assertIs(x86_rule.type, approval_rules.ApprovalRuleType.REGULAR)
        self.assertIs(x86_rule.status, approval_rules.ApprovalRuleStatus.SATISFIED)

        self.assertIn('SATISFIED', str(x86_rule))

    def test_init_any_approver(self):
        """Returns an ApprovalRule with useful properties."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        user_cache = UserCache(mock_graphql, namespace)
        input_dict = deepcopy(ALL_MEMBERS_RULE)
        approved_by_user = user_cache.get_by_dict(USERS[1])
        mr_approved_by = {approved_by_user}
        any_member_rule = \
            approval_rules.make_mr_approval_rule(input_dict, user_cache, mr_approved_by)

        self.assertIs(any_member_rule.approved, True)
        self.assertEqual(len(any_member_rule.approved_by), 1)
        self.assertEqual(len(any_member_rule.eligible_approvers), 0)
        self.assertEqual(any_member_rule.gid, 'gid://gitlab/ApprovalMergeRequestRule/12345')
        self.assertEqual(any_member_rule.name, ALL_MEMBERS_RULE['name'])
        self.assertEqual(any_member_rule.approvals_required, 1)
        self.assertIs(any_member_rule.type, approval_rules.ApprovalRuleType.ANY_APPROVER)
        self.assertIs(any_member_rule.status, approval_rules.ApprovalRuleStatus.SATISFIED)

        self.assertIn('eligible: ∞', str(any_member_rule))

        source_rule = any_member_rule.source_rule

        self.assertIs(source_rule.approved, False)
        self.assertEqual(len(source_rule.approved_by), 1)
        self.assertEqual(len(source_rule.eligible_approvers), 0)
        self.assertEqual(source_rule.gid, 'gid://gitlab/ApprovalProjectRule/65432')
        self.assertEqual(source_rule.name, ORIGINAL_ALL_MEMBERS['name'])
        self.assertEqual(source_rule.approvals_required, 2)
        self.assertIs(source_rule.type, approval_rules.ApprovalRuleType.ANY_APPROVER)
        self.assertIs(source_rule.status, approval_rules.ApprovalRuleStatus.UNSATISFIED)

        self.assertIn('eligible: ∞', str(source_rule))

    def test_from_entry(self):
        """Creates a valid ApprovalRule from an owners Entry object."""
        test_session = self.base_session()
        test_session.get_graphql = mock.Mock()
        test_session.get_graphql.return_value.find_member.side_effect = USERS + [None]
        namespace = 'group/project'
        user_cache = UserCache(test_session, namespace)

        users = deepcopy(USERS)
        for user in users:
            user['gluser'] = user.pop('username')
        reviewers = [users[0]]
        maintainers = users[1:] + [{'gluser': 'missing_user', 'name': 'Missing User'}]
        entry_spec = {'required_approvals': True,
                      'reviewers': reviewers,
                      'maintainers': maintainers,
                      'subsystem_label': 'cool_subsystem'}
        mock_entry = mock.Mock(spec=entry_spec.keys(), **entry_spec)

        entry_rule = \
            approval_rules.make_owners_approval_rule(mock_entry, user_cache, mr_approved_by=None)

        self.assertIs(entry_rule.approved, False)
        self.assertEqual(entry_rule.approved_by, set())
        self.assertEqual(len(entry_rule.eligible_approvers), 3)
        self.assertEqual(entry_rule.name, entry_spec['subsystem_label'])
        self.assertEqual(entry_rule.approvals_required, 1)
        self.assertIs(entry_rule.type, approval_rules.ApprovalRuleType.REGULAR)
        self.assertIs(entry_rule.status, approval_rules.ApprovalRuleStatus.UNSATISFIED)

    @mock.patch('webhook.approval_rules.is_production_or_staging', mock.Mock(return_value=True))
    def test_create(self):
        """Make sure a gitlab approval_rules.create call looks sane."""
        mock_glmr = mock.Mock()
        mock_glmr.state = "opened"

        test_session = self.base_session()
        test_session.get_graphql = mock.Mock()
        test_session.get_graphql.return_value.find_member.side_effect = USERS + [None]
        namespace = 'group/project'
        user_cache = UserCache(test_session, namespace)

        users = deepcopy(USERS)
        for user in users:
            user['gluser'] = user.pop('username')
        reviewers = []
        maintainers = [users[0]]
        entry_spec = {'required_approvals': True,
                      'reviewers': reviewers,
                      'maintainers': maintainers,
                      'subsystem_label': 'cool_subsystem'}
        mock_entry = mock.Mock(spec=entry_spec.keys(), **entry_spec)

        entry_rule = approval_rules.make_owners_approval_rule(mock_entry, user_cache)
        entry_rule.create(mock_glmr)
        expected_rule_data = {'name': 'cool_subsystem',
                              'approvals_required': 1,
                              'usernames': ['test_user']}
        mock_glmr.approval_rules.create.assert_called_with(data=expected_rule_data)
