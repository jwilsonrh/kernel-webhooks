"""Test the SAST webhook."""

from contextlib import contextmanager
from unittest.mock import Mock
from unittest.mock import patch

from requests import HTTPError
import responses

from tests.helpers import KwfTestCase
from webhook import sast


class TestSastGetSastItem(KwfTestCase):
    """Test the get_sast_item function in the SAST webhook."""

    def test_not_numeric_mr_id(self):
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    "asdasdsad")
        self.assertEqual(result, None)

    def test_not_valid_url(self):
        result = sast.get_sast_item("lakhsdlahdlashd",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_requests_get_returns_none(self, mock_req):
        mock_req.return_value = None
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_requests_get_throws_exception(self, mock_req):
        mock_req.side_effect = NotImplementedError
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_requests_req_is_none(self, mock_sess):
        ret_val = Mock()
        ret_val.get.return_value = None
        mock_sess.return_value = ret_val
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    def _dummy_raise_for_status(self):
        raise HTTPError("Oh no! An errror occurred!")

    def _dummy_json(self):
        return {"success": True}

    @patch("webhook.sast.get_session")
    def test_req_raise_for_status_throws_exception(self, mock_req):
        req = Mock()
        req.status_code = 1
        req.raise_for_status.side_effect = NotImplementedError
        ret_val = Mock()
        ret_val.get.return_value = req
        mock_req.return_value = ret_val
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, None)

    @patch("webhook.sast.get_session")
    def test_successful_json(self, mock_req):
        req = Mock()
        req.status_code = 200
        req.raise_for_status.return_value = self._dummy_json()
        req.json.return_value = self._dummy_json()
        ret_val = Mock()
        ret_val.get.return_value = req
        mock_req.return_value = ret_val
        result = sast.get_sast_item("http://localhost:3000",
                                    sast.UrlSubitem.STATUS,
                                    1000)
        self.assertEqual(result, self._dummy_json())


class TestFindingsToGitlabHtml(KwfTestCase):
    """Test the findings_to_gitlab_html function in the SAST webhook"""

    def test_none_param(self):
        result = sast.findings_to_gitlab_html(None, None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html({"findings": []}, None)
        self.assertEqual(result, "")

    def test_param_not_dict_or_not_correct(self):
        result = sast.findings_to_gitlab_html(3, None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html("asdasd", None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html([1, 2, "asdsad"], None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html((3, "asdsd", True), None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html({"no_findings_here": True}, None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html({"findings": True}, None)
        self.assertEqual(result, "")

    def test_items_not_correct(self):
        result = sast.findings_to_gitlab_html({"findings": [4]}, None)
        self.assertEqual(result, "")
        result = sast.findings_to_gitlab_html({"findings": [""]}, None)
        self.assertEqual(result, "")

    def test_mr_id_not_correct(self):
        result = sast.findings_to_gitlab_html({"findings": ["Real error"]}, True)
        self.assertEqual(result, "")
        sast_mr = Mock()
        sast_mr.iid = 1
        sast_mr.project.name = -1
        result = sast.findings_to_gitlab_html({"findings": ["Real error"]}, sast_mr)
        self.assertEqual(result, "")
        sast_mr.url = "asdasdads"
        result = sast.findings_to_gitlab_html({"findings": ["Real error"]}, sast_mr)
        self.assertEqual(result, "")
        sast_mr.project.name = "rhel-9"
        result = sast.findings_to_gitlab_html({"findings": ["Real error"]}, sast_mr)
        self.assertEqual(result, "")

    def test_correct_items(self):
        sast_mr = Mock()
        sast_mr.iid = 5
        sast_mr.url = "https://gitlab.com/redhat/rhel/src/kernel/rhel-9/-/merge_requests/5"
        sast_mr.project.name = "rhel-9"
        test_finding = """Error: OVERRUN (CWE-119):
aa/bbbb/dddd.c:5432:2: overrun-buffer-val: Overrunning struct type page of 1 64-byte """ + \
"""elements by passing it to a function which accesses it at element index 1 (byte offset 127).
# 5430|   	struct very_cool_struct *struct_ptr;
# 5431|   
# 5432|-> 	struct_ptr = some_init_function(another_function(*param, 8));
# 5433|   	if (struct_ptr) {
# 5434|   		return return_value;"""  # noqa: E128, W291
        info_txt = "The following list contains findings from performing a Static Analysis" + \
                   " Security Testing (SAST) on this MR. For more details on what needs to " + \
                   "be done, please review [our FAQ]" + \
                   "(https://docs.google.com/document/d/" + \
                   "1giNDNRzDh2C8EO5zdNaadACYGlWzhRgUkpMJujNFCZc). " + \
                   "This activity is currently non-gating, but the RHEL " + \
                   "Security Architects encourage you to perform it prior to " + \
                   "merging.\n\n"
        expected_result = info_txt + """\n<details>
    <summary>
        Error: OVERRUN (<a href="https://cwe.mitre.org/data/definitions/119.html" target="_blank">CWE-119</a>): aa/bbbb/dddd.c:5432&nbsp;&nbsp;&nbsp;<a href="https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?pid=12331126&issuetype=3&labels=RHEL&labels=SecArch&security=11697&priority=10200&summary=False+positive+finding+in+kernel+MR&description=Please%2C+add+the+following+OSH+error+from+%5Brhel-9+MR+5%7Chttps%3A%2F%2Fgitlab.com%2Fredhat%2Frhel%2Fsrc%2Fkernel%2Frhel-9%2F-%2Fmerge_requests%2F5%5D+to+the+list+of+known+false+positives%3A%0A%0AError%3A+OVERRUN+%28CWE-119%29%3A+aa%2Fbbbb%2Fdddd.c%3A5432%0A%0AReason%3A+%7BTYPE_THE_REASON_FOR_FALSE_POSITIVE%7D">&#10007;</a>
        &nbsp;&nbsp;&nbsp;<a href="https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?pid=12332745&issuetype=1&labels=SAST&labels=Kernel&security=11694&priority=10200&summary=Kernel+rhel-9+MR+5%3A+Error%3A+OVERRUN+%28CWE-119%29%3A+aa%2Fbbbb%2Fdddd.c%3A5432">&#10003;</a>
    </summary>
    <pre>
Error: OVERRUN (CWE-119):
aa/bbbb/dddd.c:5432:2: overrun-buffer-val: Overrunning struct type page of 1 64-byte """ + \
"""elements by passing it to a function which accesses it at element index 1 (byte offset 127).
# 5430|   	struct very_cool_struct *struct_ptr;
# 5431|   
# 5432|-> 	struct_ptr = some_init_function(another_function(*param, 8));
# 5433|   	if (struct_ptr) {
# 5434|   		return return_value;
    </pre>
</details>"""  # noqa: E501, E128, W291
        result = sast.findings_to_gitlab_html({"findings": [test_finding]}, sast_mr)
        self.assertEqual(result, expected_result)


class TestUpdateMr(KwfTestCase):
    """Test the update_mr function in the SAST webook."""

    def _gl_mr_labels_wrong(self, mr_id):
        ret = Mock()
        ret.labels = ""
        return ret

    def _gl_mr_labels_wrong_tems(self, mr_id):
        ret = Mock()
        ret.labels = [3, 4, 5]
        return ret

    def test_fields_none_or_wrong(self):
        sast.update_mr(None, None, None, None, None)
        sast.update_mr("", None, None, None, None)
        sast.update_mr(None, "", None, None, None)
        sast.update_mr(None, None, "", None, None)
        sast.update_mr("", "", "", None, None)
        gl_project = Mock()
        gl_project.mergerequests = None
        sast.update_mr("", gl_project, 1, None, None)
        gl_project.mergerequests = Mock()
        gl_project.mergerequests.get = None
        sast.update_mr("", gl_project, 1, None, None)
        gl_project.mergerequests.get = 1
        sast.update_mr("", gl_project, 1, None, None)
        gl_project.mergerequests.get = lambda x: Mock()
        sast.update_mr("", gl_project, 1, None, None)
        gl_project.mergerequests.get = self._gl_mr_labels_wrong
        sast.update_mr("", gl_project, 1, None, None)
        gl_project.mergerequests.get = self._gl_mr_labels_wrong_tems
        sast.update_mr("", gl_project, 1, None, None)

    def _gl_mr_labels_correct(self, mr_id):
        ret = Mock()
        ret.labels = ["label1", "label2", "SAST::label3"]
        ret.discussions.list.return_value = []
        return ret

    def _gl_mr_labels_comment_correct(self, mr_id):
        ret = Mock()
        ret.labels = ["label1", "label2", "SAST::label3"]
        disc = Mock()
        disc.attributes = {
            "notes": [{
                "author": {
                    "username": "dummy_username"
                },
                "body": "**Kernel SAST Scan Report** ~\"SAST::None\"\n\nDummy comment and more!!!"
            }]
        }
        ret.discussions.list.return_value = [
            disc
        ]
        return ret

    @patch("webhook.sast.common")
    def test_adding_or_not_labels(self, comm):
        comm = Mock()
        comm.add_label_to_merge_request = lambda a, b, c: ""
        gl_project = Mock()
        gl_project.mergerequests = Mock()
        gl_project.mergerequests.get = self._gl_mr_labels_correct
        sast.update_mr("", gl_project, 1, None, None)
        sast.update_mr("", gl_project, 1, "label4", None)

    def test_adding_or_not_comment(self):
        gl_project = Mock()
        gl_project.mergerequests = Mock()
        gl_project.mergerequests.get = self._gl_mr_labels_correct
        gl_project.manager.gitlab.user.username = "dummy_username"
        sast.update_mr("", gl_project, 1, None, "Dummy comment")
        sess = Mock()
        sess.update_webhook_comment = None
        sast.update_mr(sess, gl_project, 1, None, "Dummy comment")
        sess.update_webhook_comment = ""
        sast.update_mr(sess, gl_project, 1, None, "Dummy comment")
        sess.update_webhook_comment = lambda a, b, bot_name, identifier: ""
        sast.update_mr(sess, gl_project, 1, None, "Dummy comment")
        sast.update_mr(sess, gl_project, 1, None, None)
        gl_project.mergerequests.get = self._gl_mr_labels_comment_correct
        sast.update_mr(sess, gl_project, 1, None, "Dummy comment")


class TestProcessGlEvent(KwfTestCase):
    """Test the process_gl_event function in the SAST webhook."""

    class TestMr():
        """Mocks the webhook.sast.MR class"""
        project = Mock()
        gl_project = Mock()

        @classmethod
        def new(cls, session, url):
            return cls(None, None, None, url)

        def __init__(self, gql, gli, rhp, url):
            items = url.split("/-/merge_requests/")
            self.project.name = items[0]
            self.iid = int(items[1])

    def _dummy_get_sast_item(url, typ, iid):
        if typ == sast.UrlSubitem.STATUS:
            if iid == 0:
                return {"status": "finished", "amount": 0}
            elif iid == -1:
                return None
            elif iid < -1:
                return {"status": "pending", "amount": 0}
            else:
                return {"status": "finished", "amount": iid}
        else:
            if iid == 1:
                return {"findings": ["\n"]}
            elif iid == 2:
                return None
            else:
                return {"findings": ["Item1", "Item2", "Item3"]}

    @patch('webhook.sast.MR', wraps=TestMr)
    @patch('webhook.sast.get_sast_item', wraps=_dummy_get_sast_item)
    @patch('webhook.sast.update_mr')
    def test_mocked_mr(self, mr, get_sast_item, update_mr_fn):
        session = Mock()
        session.graphql = Mock()
        session.gl_instance = Mock()
        session.rh_projects = Mock()
        session.args.sast_target_rhel = "rhel-9"
        event = Mock()
        event.kind = Mock()
        event.kind.name = ""

        event.mr_url = "some_url/-/merge_requests/34"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/-1"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/-2"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/0"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/1"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/2"
        sast.process_gl_event(None, session, event)

        event.mr_url = "rhel-9/-/merge_requests/3"
        sast.process_gl_event(None, session, event)


class TestMain(KwfTestCase):
    """Test the main function of the SAST webhook."""

    @contextmanager
    def assertNotRaises(self, exc_type):
        try:
            yield None
        except exc_type:
            raise self.failureException('{} raised'.format(exc_type.__name__))

    def test_main_sast_rest_api_url_set(self):
        with self.assertRaises(RuntimeError):
            sast.main(["--sast-rest-api-url", "some-url"])

    @patch("webhook.session.SessionRunner.run", return_value=Mock())
    def test_main_all_needed_set(self, sr_run):
        with self.assertNotRaises(Exception):
            sast.main(["--sast-rest-api-url", "some-url", "--rabbitmq-routing-key", "some-key"])

    @responses.activate
    def test_main_run_nudger_correct(self):
        responses.get("https://gitlab.com/api/v4/user", json={})
        responses.get("https://gitlab.com/api/v4/projects/redhat%2Fcentos-stream%2Fsrc%2F" +
                      "kernel%2Fcentos-stream-9", json={"id": 3})
        responses.get("https://gitlab.com/api/v4/projects/3/merge_requests?state=opened&" +
                      "per_page=100", json=[{"iid": 4652, 'web_url':
                                             "https://gitlab.com/redhat/centos-stream/src/" +
                                             "kernel/centos-stream-9/-/merge_requests/4652"}])
        CURRENT_USER = {'email': 'bot@example.com',
                        'gid': 'gid://gitlab/User/7654321',
                        'name': 'Bot User',
                        'username': 'bot_user'}
        AUTHOR1 = {'email': 'test_user1@example.com',
                   'gid': 'gid://gitlab/User/1111',
                   'name': 'Test User1',
                   'username': 'test_user1'}
        MR1_QUERY = {'author': AUTHOR1,
                     'approved': False,
                     'commitCount': 2,
                     'description': 'This is a dummy NR',
                     'draft': False,
                     'files': [{'path': 'include/linux.h'}],
                     'headPipeline': {'id': 'gid://gitlab/Ci::Pipeline/12345678'},
                     'global_id': 'gid://gitlab/MergeRequest/12121212',
                     'labels': {'nodes': []},
                     'project': {'id': 'gid://gitlab/Project/24152864'},
                     'state': 'opened',
                     'sourceBranch': 'feature',
                     'targetBranch': 'main',
                     'title': 'Another test MR'}
        MR1_QUERY_RESULT = {'currentUser': CURRENT_USER, 'project': {'mr': MR1_QUERY}}
        responses.post("https://gitlab.com/api/graphql", json={"data": MR1_QUERY_RESULT})
        responses.get("https://gitlab.com/api/v4/projects/3/merge_requests/4652", json={})
        with self.assertNotRaises(Exception):
            sast.main(["--sast-rest-api-url", "some-url", "--gl-projects",
                      ["redhat/centos-stream/src/kernel/centos-stream-9"]])

    def test_main_run_nudger_proj_not_in_metadata(self):
        with self.assertRaises(ValueError):
            sast.main(["--sast-rest-api-url", "some-url", "--gl-projects",
                      ["some/custom/namespace/project"]])

    @patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_sandbox.yaml',
                               'CKI_DEPLOYMENT_ENVIRONMENT': 'production'})
    def test_main_run_nudger_proj_sandbox(self):
        with self.assertNotRaises(Exception):
            sast.main(["--sast-rest-api-url", "some-url", "--gl-projects",
                      ["redhat/rhel/src/kernel/rhel-8-sandbox"]])

    @patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'})
    def test_main_run_nudger_staging_env(self):
        with self.assertNotRaises(Exception):
            sast.main(["--sast-rest-api-url", "some-url", "--gl-projects",
                      ["redhat/centos-stream/src/kernel/centos-stream-9"]])
