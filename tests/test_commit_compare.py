"""Webhook interaction tests."""
import copy
from dataclasses import dataclass
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import mock

from tests import fakes
from tests.helpers import KwfTestCase
from webhook import cdlib
from webhook import commit_compare
from webhook import defs
from webhook.base_mr_mixins import DependsMixin
from webhook.common import get_arg_parser
from webhook.description import Description as Desc
from webhook.session import SessionRunner
from webhook.session_events import create_event
from webhook.users import User

# a hunk of upstream linux kernel commit ID 1fc70edb7d7b5ce1ae32b0cf90183f4879ad421a
PATCH_A = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_A += "index blahblah..blahblah 100644\n"
PATCH_A += "--- a/include/linux/netdevice.h\n"
PATCH_A += "+++ b/include/linux/netdevice.h\n"
PATCH_A += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_A += "        unsigned short          type;\n"
PATCH_A += "        unsigned short          hard_header_len;\n"
PATCH_A += "        unsigned char           min_header_len;\n"
PATCH_A += "+       unsigned char           name_assign_type;\n"
PATCH_A += "\n"
PATCH_A += "        unsigned short          needed_headroom;\n"
PATCH_A += "        unsigned short          needed_tailroom;\n"

PATCH_B = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_B += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_B += "--- a/include/linux/netdevice.h\n"
PATCH_B += "+++ b/include/linux/netdevice.h\n"
PATCH_B += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_B += "        unsigned short          type;\n"
PATCH_B += "        unsigned short          hard_header_len;\n"
PATCH_B += "        unsigned char           min_header_len;\n"
PATCH_B += "+       unsigned char           name_assign_type;\n"
PATCH_B += "\n"
PATCH_B += "        unsigned short          needed_headroom;\n"
PATCH_B += "        unsigned short          needed_tailroom;\n"

PATCH_C = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_C += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_C += "--- a/include/linux/netdevice.h\n"
PATCH_C += "+++ b/include/linux/netdevice.h\n"
PATCH_C += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_C += "        unsigned short          type;\n"
PATCH_C += "        unsigned short          hard_header_len;\n"
PATCH_C += "        unsigned char           min_header_len;\n"
PATCH_C += "+       RH_KABI_EXTEND(unsigned char name_assignee_type)\n"
PATCH_C += "\n"
PATCH_C += "        unsigned short          needed_headroom;\n"
PATCH_C += "        unsigned short          needed_tailroom;\n"

PATCH_D = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netdevice.h\n"
PATCH_D += "+++ b/include/linux/netdevice.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"
PATCH_D += "diff --git a/include/linux/netlink.h b/include/linux/netlink.h\n"
PATCH_D += "index 313803d6c781..9fdb3ebef306 100644\n"
PATCH_D += "--- a/include/linux/netlink.h\n"
PATCH_D += "+++ b/include/linux/netlink.h\n"
PATCH_D += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_D += "        unsigned short          type;\n"
PATCH_D += "        unsigned short          hard_header_len;\n"
PATCH_D += "        unsigned char           min_header_len;\n"
PATCH_D += "+       unsigned char           name_assign_type;\n"
PATCH_D += "\n"
PATCH_D += "        unsigned short          needed_headroom;\n"
PATCH_D += "        unsigned short          needed_tailroom;\n"

PATCH_E = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_E += "new file mode 100644\n"
PATCH_E += "index blahblah..blahblah 100644\n"
PATCH_E += "--- /dev/null\n"
PATCH_E += "+++ b/include/linux/netdevice.h\n"
PATCH_E += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_E += "        unsigned short          type;\n"
PATCH_E += "        unsigned short          hard_header_len;\n"
PATCH_E += "        unsigned char           min_header_len;\n"
PATCH_E += "+       unsigned char           name_assign_type;\n"
PATCH_E += "\n"
PATCH_E += "        unsigned short          needed_headroom;\n"
PATCH_E += "        unsigned short          needed_tailroom;\n"

PATCH_F = "diff --git a/include/linux/netdevice.h b/include/linux/netdevice.h\n"
PATCH_F += "deleted file mode 100644\n"
PATCH_F += "index blahblah..blahblah 100644\n"
PATCH_F += "--- a/include/linux/netdevice.h\n"
PATCH_F += "+++ /dev/null\n"
PATCH_F += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_F += "        unsigned short          type;\n"
PATCH_F += "        unsigned short          hard_header_len;\n"
PATCH_F += "        unsigned char           min_header_len;\n"
PATCH_F += "+       unsigned char           name_assign_type;\n"
PATCH_F += "\n"
PATCH_F += "        unsigned short          needed_headroom;\n"
PATCH_F += "        unsigned short          needed_tailroom;\n"

PATCH_G = "diff --git a/include/linux/netdevice.h b/include/linux/foobar.h\n"
PATCH_G += "similarity index xx%\n"
PATCH_G += "rename from include/linux/netdevice.h\n"
PATCH_G += "rename to include/linux/foobar.h\n"
PATCH_G += "index blahblah..blahblah 100644\n"
PATCH_G += "--- a/include/linux/netdevice.h\n"
PATCH_G += "+++ b/include/linux/foobar.h\n"
PATCH_G += "@@ -1955,6 +1955,7 @@ struct net_device {\n"
PATCH_G += "        unsigned short          type;\n"
PATCH_G += "        unsigned short          hard_header_len;\n"
PATCH_G += "        unsigned char           min_header_len;\n"
PATCH_G += "+       unsigned char           name_assign_type;\n"
PATCH_G += "\n"
PATCH_G += "        unsigned short          needed_headroom;\n"
PATCH_G += "        unsigned short          needed_tailroom;\n"

PATCH_H = "@@ -0,0 +1,117 @@\n"
PATCH_H += "+.. SPDX-License-Identifier: GPL-2.0\n+\n+====================\n"
PATCH_H += "+Kernel Testing Guide\n+====================\n+\n+\n"
PATCH_H += "+There are a number of different tools for testing the Linux kernel, so knowing\n"
PATCH_H += "+when to use each of them can be a challenge. This document provides a rough\n"
PATCH_H += "+overview of their differences, and how they fit together.\n+\n+\n"
PATCH_H += "+Writing and Running Tests\n+=========================\n+\n"
PATCH_H += "+The bulk of kernel tests are written using either the kselftest or KUnit\n"
PATCH_H += "+frameworks. These both provide infrastructure to help make running tests and\n"
PATCH_H += "+groups of tests easier, as well as providing helpers to aid in writing new\n"
PATCH_H += "+tests.\n+\n"
PATCH_H += "+If you're looking to verify the behaviour of the Kernel — particularly specific\n"
PATCH_H += "+parts of the kernel — then you'll want to use KUnit or kselftest.\n+\n+\n"
PATCH_H += "+The Difference Between KUnit and kselftest\n"
PATCH_H += "+------------------------------------------\n+\n"
PATCH_H += "+KUnit (Documentation/dev-tools/kunit/index.rst) is an entirely in-kernel system\n"
PATCH_H += "+for \"white box\" testing: because test code is part of the kernel, it can access\n"
PATCH_H += "+internal structures and functions which aren't exposed to userspace.\n+\n"
PATCH_H += "+KUnit tests therefore are best written against small, self-contained parts\n"
PATCH_H += "+of the kernel, which can be tested in isolation. This aligns well with the\n"
PATCH_H += "+concept of 'unit' testing.\n+\n"
PATCH_H += "+For example, a KUnit test might test an individual kernel function (or even a\n"
PATCH_H += "+single codepath through a function, such as an error handling case), rather\n"
PATCH_H += "+than a feature as a whole.\n+\n"
PATCH_H += "+This also makes KUnit tests very fast to build and run, allowing them to be\n"
PATCH_H += "+run frequently as part of the development process.\n+\n"
PATCH_H += "+There is a KUnit test style guide which may give further pointers in\n"
PATCH_H += "+Documentation/dev-tools/kunit/style.rst\n+\n+\n"
PATCH_H += "+kselftest (Documentation/dev-tools/kselftest.rst), on the other hand, is\n"
PATCH_H += "+largely implemented in userspace, and tests are normal userspace scripts or\n"
PATCH_H += "+programs.\n+\n"
PATCH_H += "+This makes it easier to write more complicated tests, or tests which need to\n"
PATCH_H += "+manipulate the overall system state more (e.g., spawning processes, etc.).\n"
PATCH_H += "+However, it's not possible to call kernel functions directly from kselftest.\n"
PATCH_H += "+This means that only kernel functionality which is exposed to userspace somehow\n"
PATCH_H += "+(e.g. by a syscall, device, filesystem, etc.) can be tested with kselftest.  To\n"
PATCH_H += "+work around this, some tests include a companion kernel module which exposes\n"
PATCH_H += "+more information or functionality. If a test runs mostly or entirely within the\n"
PATCH_H += "+kernel, however,  KUnit may be the more appropriate tool.\n+\n"
PATCH_H += "+kselftest is therefore suited well to tests of whole features, as these will\n"
PATCH_H += "+expose an interface to userspace, which can be tested, but not implementation\n"
PATCH_H += "+details. This aligns well with 'system' or 'end-to-end' testing.\n+\n"
PATCH_H += "+For example, all new system calls should be accompanied by kselftest tests.\n+\n"
PATCH_H += "+Code Coverage Tools\n+===================\n+\n"
PATCH_H += "+The Linux Kernel supports two different code coverage measurement tools. These\n"
PATCH_H += "+can be used to verify that a test is executing particular functions or lines\n"
PATCH_H += "+of code. This is useful for determining how much of the kernel is being tested,\n"
PATCH_H += "+and for finding corner-cases which are not covered by the appropriate test.\n+\n"
PATCH_H += "+:doc:`gcov` is GCC's coverage testing tool, which can be used with the kernel\n"
PATCH_H += "+to get global or per-module coverage. Unlike KCOV, it does not record per-task\n"
PATCH_H += "+coverage. Coverage data can be read from debugfs, and interpreted using the\n"
PATCH_H += "+usual gcov tooling.\n+\n"
PATCH_H += "+:doc:`kcov` is a feature which can be built in to the kernel to allow\n"
PATCH_H += "+capturing coverage on a per-task level. It's therefore useful for fuzzing and\n"
PATCH_H += "+other situations where information about code executed during, for example, a\n"
PATCH_H += "+single syscall is useful.\n+\n+\n+Dynamic Analysis Tools\n"
PATCH_H += "+======================\n+\n"
PATCH_H += "+The kernel also supports a number of dynamic analysis tools, which attempt to\n"
PATCH_H += "+detect classes of issues when they occur in a running kernel. These typically\n"
PATCH_H += "+each look for a different class of bugs, such as invalid memory accesses,\n"
PATCH_H += "+concurrency issues such as data races, or other undefined behaviour like\n"
PATCH_H += "+integer overflows.\n+\n"
PATCH_H += "+Some of these tools are listed below:\n+\n"
PATCH_H += "+* kmemleak detects possible memory leaks. See\n"
PATCH_H += "+  Documentation/dev-tools/kmemleak.rst\n"
PATCH_H += "+* KASAN detects invalid memory accesses such as out-of-bounds and\n"
PATCH_H += "+  use-after-free errors. See Documentation/dev-tools/kasan.rst\n"
PATCH_H += "+* UBSAN detects behaviour that is undefined by the C standard, like integer\n"
PATCH_H += "+  overflows. See Documentation/dev-tools/ubsan.rst\n"
PATCH_H += "+* KCSAN detects data races. See Documentation/dev-tools/kcsan.rst\n"
PATCH_H += "+* KFENCE is a low-overhead detector of memory issues, which is much faster than\n"
PATCH_H += "+  KASAN and can be used in production. See Documentation/dev-tools/kfence.rst\n"
PATCH_H += "+* lockdep is a locking correctness validator. See\n"
PATCH_H += "+  Documentation/locking/lockdep-design.rst\n"
PATCH_H += "+* There are several other pieces of debug instrumentation in the kernel, many\n"
PATCH_H += "+  of which can be found in lib/Kconfig.debug\n+\n"
PATCH_H += "+These tools tend to test the kernel as a whole, and do not \"pass\" like\n"
PATCH_H += "+kselftest or KUnit tests. They can be combined with KUnit or kselftest by\n"
PATCH_H += "+running tests on a kernel with these tools enabled: you can then be sure\n"
PATCH_H += "+that none of these errors are occurring during the test.\n+\n"
PATCH_H += "+Some of these tools integrate with KUnit or kselftest and will\n"
PATCH_H += "+automatically fail tests if an issue is detected.\n+\n"

EXPECTED_REPORT = ("\n\nUpstream Commit ID Readiness Report\n"
                   "\n"
                   "|P num   |Sub CID |UCIDs   |Match     |Notes   |\n"
                   "|:-------|:-------|:-------|:---------|:-------|\n"
                   "|1|348742bd|RHELonly|n/a       |3|\n"
                   "|2|1cd738b1|0c55f51a|Diffs     |2|\n"
                   "|4|1508d8fb|6cb6a4a2|Merge commit||\n"
                   "|5|6cb6a4a3|Posted|n/a       |1|\n"
                   "1. This commit has Upstream Status as Posted, but we're not able to "
                   "auto-compare it.  Reviewers should take additional care when reviewing "
                   "these commits.\n"
                   "2. This commit differs from the referenced upstream commit and should be "
                   "evaluated accordingly.\n"
                   "3. This commit has Upstream Status as RHEL-only and has no corresponding "
                   "upstream commit.  The author of this MR should verify if this commit has "
                   "to be applied to "
                   "[future versions of RHEL](https://gitlab.com/cki-project/kernel-ark). "
                   "Reviewers should take additional care when reviewing these commits.\n"
                   "\n"
                   "Total number of commits analyzed: **5**\n"
                   "Please verify differences from upstream.\n"
                   "* Patches that match upstream 100% not shown in table\n"
                   "\n"
                   "Merge Request passes upstream commit ID validation.\n")

EXPECTED_REPORT_EMPTY = ("\n\nUpstream Commit ID Readiness Report\n"
                         "\n"
                         "Patches all match upstream 100%\n"
                         "\n"
                         "Total number of commits analyzed: **5**\n"
                         "\n"
                         "Merge Request passes upstream commit ID validation.\n")


def create_commits_mr(mr_url):
    """Return a fresh CommitsMR object."""
    session = SessionRunner.new('commit_compare', args='')
    gl_instance = fakes.FakeGitLab()
    gl_instance.user = User(username='cki-kwf-bot')
    session.get_gl_instance = mock.Mock(return_value=gl_instance)

    return commit_compare.CommitsMR.new(session, mr_url, linux_src='/somewhere/fake')


@dataclass(repr=False)
class JsonCommitsMR:
    """Simple collection of file names of json data and users to build CommitsMR from."""

    mr_url: defs.GitlabURL
    basemr: str
    commits: str
    diffs: str


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml',
                                'OWNERS_YAML': 'tests/assets/owners-20240711.yaml'})
@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommitCompare(KwfTestCase):
    """Test for the commit_compare webhook."""

    # For mocking subprocess.run
    _mocked_runs = []
    _mocked_calls = []

    PATH_WITH_NAMESPACE = 'redhat/centos-stream/src/kernel/centos-stream-9'
    PROJECT_URL = f'https://gitlab.com/{PATH_WITH_NAMESPACE}'
    MR_URL = f'{PROJECT_URL}/-/merge_requests/4776'
    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': PROJECT_URL,
                                 'path_with_namespace': PATH_WITH_NAMESPACE
                                 },
                     'object_attributes': {'target_branch': 'main',
                                           'iid': 2,
                                           'url': MR_URL},
                     'changes': {'labels': {'previous': [],
                                            'current': []}},
                     'description': 'dummy description',
                     'state': 'opened',
                     'user': {'username': 'test_user'}
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'archived': False,
                                'web_url': PROJECT_URL,
                                'path_with_namespace': PATH_WITH_NAMESPACE
                                },
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main',
                                      'iid': 2,
                                      'url': MR_URL},
                    'description': 'dummy description',
                    'state': 'opened',
                    'user': {'username': 'test_user'}
                    }

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    def populate_responses(self, json_commits_mr) -> None:
        # Make sure we're starting with an empty set of responses
        self.responses.reset()

        # Standard user authentication
        self.response_gl_auth()
        # Standard GraphQL user data response
        self.response_gql_user_data()

        mr_endpoint = ''
        # Set up the CommitsMR query responses.
        variables = {'namespace': json_commits_mr.mr_url.namespace,
                     'mr_id': str(json_commits_mr.mr_url.id)}

        basemr = self.load_yaml_asset(
            path=json_commits_mr.basemr,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(commit_compare.CommitsMR.MR_QUERY, variables=variables, query_result=basemr)

        commits = self.load_yaml_asset(
            path=json_commits_mr.commits,
            sub_module='gitlab_graphql_api'
        )
        self.add_query(commit_compare.CommitsMR.COMMIT_QUERY,
                       variables=variables, query_result=commits)

        diffs = self.load_yaml_asset(
            path=json_commits_mr.diffs,
            sub_module='gitlab_graphql_api'
        )
        diffs_variables = {
            'namespace': json_commits_mr.mr_url.namespace,
            'mr_id': str(json_commits_mr.mr_url.id),
            'per_page': 10,
            'with_diffs': True,
            'before': '',
        }
        self.add_query(commit_compare.CommitsMR.COMMIT_QUERY,
                       variables=diffs_variables, query_result=diffs)

        proj_name = json_commits_mr.mr_url.namespace.split('/')[-1]
        project = self.load_yaml_asset(
            path=f'project-{proj_name}.json',
            sub_module='gitlab_rest_api'
        )

        proj_id = basemr['data']['project']['mr']['project']['id'].split('/')[-1]
        proj_endpoint = "https://gitlab.com/api/v4/projects"
        proj_path = json_commits_mr.mr_url.namespace.replace("/", "%2F")

        self.responses.get(f'{proj_endpoint}/{proj_id}', json=project)
        self.responses.get(f'{proj_endpoint}/{proj_path}', json=project)

        merge_request = self.load_yaml_asset(
            path=f'mr-{json_commits_mr.mr_url.id}-{proj_name}.json',
            sub_module='gitlab_rest_api'
        )
        mr_endpoint = f'{proj_endpoint}/{proj_id}/merge_requests/{json_commits_mr.mr_url.id}'
        self.responses.get(mr_endpoint, json=merge_request)

    def setup_cs9_mr(self, mr_id) -> commit_compare.CommitsMR:
        mr_url = defs.GitlabURL('https://gitlab.com/redhat/centos-stream/src/kernel/'
                                f'centos-stream-9/-/merge_requests/{mr_id}')
        proj_name = mr_url.namespace.split('/')[-1]

        basemr = f'commitsmr_{proj_name}_{mr_id}-basemr.json'
        commits = f'commitsmr_{proj_name}_{mr_id}-commits.json'
        diffs = f'commitsmr_{proj_name}_{mr_id}-commits-diffs.json'
        json_commits_mr = JsonCommitsMR(mr_url=mr_url, basemr=basemr, commits=commits, diffs=diffs)
        self.populate_responses(json_commits_mr)

        return create_commits_mr(mr_url)

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.commit_compare.CommitsMR.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_merge_request(self, sdiff, udiff, dep_data, ext_deps, gcc):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'os-build'
        payload['object_attributes']['action'] = 'open'
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        gcc.return_value = 100, 50
        self._test_payload(True, payload=payload)

    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.get_dependencies_data')
    @mock.patch('webhook.commit_compare.CommitsMR.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    def test_ucid_re_evaluation(self, sdiff, udiff, dep_data, ext_deps):
        """Check handling of commit ID re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-commit-id-evaluation"
        payload["object_attributes"]["noteable_type"] = "MergeRequest"
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        dep_data.return_value = (False, 'abcd')
        ext_deps.return_value = []
        self._test_payload(True, payload)
        payload["object_attributes"]["state"] = "closed"
        self._test_payload(False, payload)
        payload["project"]["archived"] = True
        self._test_payload(False, payload)

    def test_match_description(self):
        """Check that we get sane strings back for match types."""
        output = commit_compare.Match.FULL.description
        self.assertEqual(output, "100% match")
        output = commit_compare.Match.PARTIAL.description
        self.assertEqual(output, "Partial")
        output = commit_compare.Match.DIFFS.description
        self.assertEqual(output, "Diffs")
        output = commit_compare.Match.KABI.description
        self.assertEqual(output, "kABI Diffs")
        output = commit_compare.Match.RHELONLY.description
        self.assertEqual(output, "n/a")
        output = commit_compare.Match.POSTED.description
        self.assertEqual(output, "n/a")
        output = commit_compare.Match.CONFIG.description
        self.assertEqual(output, "Config")
        # No match
        output = commit_compare.Match.NOUCID.description
        self.assertEqual(output, "No UCID")
        output = commit_compare.Match.UNRECOGNIZED.description
        self.assertEqual(output, "Unrecognized UCID")

    def test_find_kabi_hints(self):
        """Check that we can find a kabi hint in a patch hunk."""
        output = commit_compare.find_kabi_hints('desc', PATCH_A)
        self.assertFalse(output)
        output = commit_compare.find_kabi_hints('desc', PATCH_C)
        self.assertTrue(output)
        description = "This patch is pretty basic, nothing to see"
        output = commit_compare.find_kabi_hints(description, 'nada')
        self.assertFalse(output)
        description = "This patch contains kABI work-arounds"
        output = commit_compare.find_kabi_hints(description, 'nada')
        self.assertTrue(output)
        description = "This patch references genksyms"
        output = commit_compare.find_kabi_hints(description, 'nada')
        self.assertTrue(output)
        patch = "we're using an RH_RESERVED field here"
        output = commit_compare.find_kabi_hints('nada', patch)
        self.assertTrue(output)

    def test_ucid_compare(self):
        """Check that diff engine actually compares things properly."""
        # compare two identical patches (should be equal)
        output = cdlib.compare_commits(PATCH_A, PATCH_A)
        self.assertEqual(output, [])
        # compare two patches with only context differences (should be equal)
        output = cdlib.compare_commits(PATCH_A, PATCH_B)
        self.assertEqual(output, [])
        # compare two patches with actual differences (should NOT be equal)
        output = cdlib.compare_commits(PATCH_A, PATCH_C)
        self.assertNotEqual(output, [])

    @mock.patch.dict(
        'os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_private.yaml'}
    )
    @mock.patch('webhook.cdlib.get_dependencies_data', return_value=(False, "abcd"))
    def test_run_zstream_comparison_checks(self, gdd):
        """Make sure z-stream comparisons look sane."""
        zcommit = mock.Mock(sha="1234567890ab",
                            description=Desc("Y-Commit: abc012345678"),
                            parent_ids=['abc'])
        zdifflist = []
        path = {'oldPath': 'include/linux/netdevice.h',
                'newPath': 'include/linux/netdevice.h',
                'newFile': "false",
                'renamedFile': "false",
                'deletedFile': "false",
                'aMode': '100644',
                'bMode': '100644',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        zdifflist.append(path)
        zcommit.diff = zdifflist

        ycommit = mock.Mock(id="abc012345678",
                            message="commit a1b2c3d4e5f6",
                            parent_ids=['xyz'])
        ydifflist = []
        ypath = {'old_path': 'include/linux/netdevice.h',
                 'new_path': 'include/linux/netdevice.h',
                 'new_file': False,
                 'renamed_file': False,
                 'deleted_file': False,
                 'a_mode': '100644',
                 'b_mode': '100644',
                 'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                         "        unsigned short          type;\n"
                         "        unsigned short          hard_header_len;\n"
                         "        unsigned char           min_header_len;\n"
                         "+       unsigned char           name_assign_type;\n"
                         "\n"
                         "        unsigned short          needed_headspace;\n"
                         "        unsigned short          needed_tailroom;\n"}
        ypath2 = {'old_path': 'include/linux/netdevice.h',
                  'new_path': 'include/linux/netdevice.h',
                  'new_file': False,
                  'renamed_file': False,
                  'deleted_file': False,
                  'a_mode': '100644',
                  'b_mode': '100644',
                  'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                          "        unsigned short          type;\n"
                          "        unsigned short          hard_header_len;\n"
                          "        unsigned char           min_header_len;\n"
                          "+       unsigned char           name_assign_type2;\n"
                          "\n"
                          "        unsigned short          needed_headspace;\n"
                          "        unsigned short          needed_tailroom;\n"}
        ydifflist.append(ypath2)
        ycommit.diff.return_value = ydifflist

        cmr = mock.Mock()
        merge_request = mock.Mock()
        project = mock.Mock()
        cmr.rhcommits = []
        cmr.rhcommits.append(commit_compare.RHCommit(zcommit))
        cmr.gl_mr = merge_request
        project.commits.get.return_value = ycommit
        project.id = 12345
        cmr.gl_project = project

        # Don't run on main
        cmr.target_branch = "main"
        cmr.zcompare_notes = ""
        commit_compare.CommitsMR.run_zstream_comparison_checks(cmr)
        self.assertEqual("", cmr.zcompare_notes)

        # Don't run on branch without zstream
        cmr.target_branch = "8.9"
        cmr.branch = mock.Mock()
        cmr.branch.zstream = False
        cmr.zcompare_notes = ""
        commit_compare.CommitsMR.run_zstream_comparison_checks(cmr)
        self.assertEqual("", cmr.zcompare_notes)

        cmr.target_branch = "8.6"
        cmr.zcompare_notes = ""
        cmr.branch.zstream = True
        commit_compare.CommitsMR.run_zstream_comparison_checks(cmr)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab do not match",
                      cmr.zcompare_notes)

        ydifflist = []
        ydifflist.append(ypath)
        ycommit.diff.return_value = ydifflist
        cmr.zcompare_notes = ""
        commit_compare.CommitsMR.run_zstream_comparison_checks(cmr)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab match 100%",
                      cmr.zcompare_notes)

        # ydiff is > 3x the size of zdiff
        ydifflist = []
        ydifflist.append(ypath)
        ydifflist.append(ypath2)
        ydifflist.append(ypath2)
        ydifflist.append(ypath2)
        ycommit.diff.return_value = ydifflist
        cmr.zcompare_notes = ""
        commit_compare.CommitsMR.run_zstream_comparison_checks(cmr)
        self.assertIn("Y-Commit abc012345678 and Z-Commit 1234567890ab do not match",
                      cmr.zcompare_notes)
        self.assertIn("more than 3x the size", cmr.zcompare_notes)

        zcommit.description = Desc("There is no spoon")
        cmr.zcompare_notes = ""
        commit_compare.CommitsMR.run_zstream_comparison_checks(cmr)
        self.assertIn("Z-Commit 1234567890ab has no Y-Commit reference", cmr.zcompare_notes)

    def test_commit_id_report(self):
        """Check on reporting functions."""
        cmr = mock.Mock()
        cmr.merge_request = mock.Mock()
        cmr.commit_count = 4
        cmr.notes = ""
        commit1 = mock.Mock(sha='6cb6a4a3ede59f047febaeaa801f164954541ff0')
        commit2 = mock.Mock(sha='12345678aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
        commit3 = mock.Mock(sha='1cd738b13ae9b29e03d6149f0246c61f76e81fcf')
        commit4 = mock.Mock(sha='348742bd816fbbcda2f8243bf234bf1c91788082')
        rhcommit1 = mock.Mock(commit=commit1, ucids=['Posted'],
                              match=commit_compare.Match.POSTED, notes=['1'])
        rhcommit2 = mock.Mock(commit=commit2, ucids=['abcdef01aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
                              match=commit_compare.Match.FULL, notes=[])
        rhcommit3 = mock.Mock(commit=commit3, ucids=['0c55f51a8c4bade7b2525047130f3925f1dd42bb'],
                              match=commit_compare.Match.DIFFS, notes=['2'])
        rhcommit4 = mock.Mock(commit=commit4, ucids=['RHELonly'],
                              match=commit_compare.Match.RHELONLY, notes=['3'])
        cmr.rhcommits = [rhcommit1, rhcommit2, rhcommit3, rhcommit4]
        cmr.errors = "You screwed up SO bad here...\n"
        cmr.zcompare_notes = ""
        report = commit_compare.CommitsMR.commit_id_report(cmr)
        self.assertIn("348742bd816fbbcda2f8243bf234bf1c91788082|RHELonly", report)
        self.assertIn("1cd738b13ae9b29e03d6149f0246c61f76e81fcf|[0c55f51a]", report)
        self.assertIn("6cb6a4a3ede59f047febaeaa801f164954541ff0|Posted", report)
        self.assertIn("Total number of commits analyzed: **4**", report)

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.cdlib.is_rhconfig_commit', mock.Mock(return_value=False))
    @mock.patch('webhook.cdlib.get_diff_from_gql')
    @mock.patch('webhook.commit_compare.CommitsMR.update_commit_match_data')
    @mock.patch('webhook.commit_compare.CommitsMR.get_upstream_diff')
    @mock.patch('webhook.commit_compare.CommitsMR.no_upstream_commit_data')
    def test_validate_commit_ids(self, no_ucid, get_udiff, ucom, get_sdiff):
        """Check that we properly flesh out RHCommit dataclass elements."""
        cmr = mock.Mock()
        cmr.notes = []
        cmr.gl_project = mock.Mock()
        cmr.linux_src = "/src/linux"
        mpcget = mock.MagicMock(id="1234", author_email="johndoe@redhat.com")
        mpcget.diff.return_value = PATCH_A
        cmr.gl_project.commits.get.return_value = mpcget
        cmr.nids = {'kabi': 0, 'unknown': 0}
        cmr.no_upstream_commit_data = no_ucid
        cmr.get_upstream_diff = get_udiff
        cmr.update_commit_match_data = ucom
        c1 = mock.Mock(sha="abcdef012345", author_email="jdoe@redhat.com", description=Desc(""))
        c2 = mock.Mock(sha="fedcba012345", author_email="jdoe@redhat.com", description=Desc(""))
        c3 = mock.Mock(sha="fedcba012345", author_email="jdoe@redhat.com", description=Desc(""))
        c4 = mock.Mock(sha="aaacba012346", author_email="jdoe@redhat.com", description=Desc(""))
        c5 = mock.Mock(sha="aaacba012346", author_email="jdoe@redhat.com",
                       description=Desc("Touches kABI"))
        cmr.rhcommits = []
        cmr.rhcommits.append(commit_compare.RHCommit(c1))
        cmr.rhcommits.append(commit_compare.RHCommit(c2))
        cmr.rhcommits.append(commit_compare.RHCommit(c3))
        cmr.rhcommits.append(commit_compare.RHCommit(c4))
        cmr.rhcommits.append(commit_compare.RHCommit(c5))
        cmr.rhcommits[0].ucids = ['RHELonly']
        cmr.rhcommits[1].ucids = ['deadbeefdead']
        cmr.rhcommits[2].ucids = ['Posted']
        cmr.rhcommits[3].ucids = ['-']
        cmr.rhcommits[4].ucids = ['RHELonly']

        # No diffs returned, upstream patches don't exist
        get_udiff.return_value = (None, "")
        get_sdiff.return_value = ("", [])
        commit_compare.CommitsMR.validate_commit_ids(cmr)
        self.assertEqual(no_ucid.call_count, 4)
        self.assertEqual(ucom.call_count, 2)
        get_udiff.assert_called_once()

        # Reset counters and run again w/an upstream diff returned
        no_ucid.call_count = 0
        get_udiff.call_count = 0
        cmr.rhcommits = []
        cmr.rhcommits.append(commit_compare.RHCommit(c2))
        cmr.rhcommits[0].ucids = ['deadbeefdead']
        get_udiff.return_value = (PATCH_A, 'johndoe@redhat.com')
        get_sdiff.return_value = (PATCH_A, ['include/linux/netdevice.h'])
        commit_compare.CommitsMR.validate_commit_ids(cmr)
        no_ucid.assert_not_called()
        self.assertEqual(ucom.call_count, 3)
        get_udiff.assert_called_once()

    @mock.patch('webhook.commit_compare.CommitsMR.prep_temp_ark_branch', mock.Mock())
    @mock.patch('webhook.commit_compare.CommitsMR.evaluate_config', mock.Mock(return_value=False))
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.cdlib.is_rhconfig_commit', mock.Mock(return_value=False))
    @mock.patch('webhook.cdlib.make_dependencies_label')
    @mock.patch('webhook.commit_compare.CommitsMR.get_upstream_diff')
    @mock.patch('webhook.cdlib.get_submitted_diff')
    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._filter_mr_labels')
    @mock.patch('webhook.common.extract_dependencies')
    def test_check_on_ucids_rhdocs(self, ext_deps, f_labels, c_labels, sdiff, udiff, set_dep):
        """Check that a commit that belongs to docs are properly evaluated."""
        self.maxDiff = None
        cmr = mock.Mock(notes=[])
        cmr.commit_count = 3
        merge_request = mock.MagicMock()
        merge_request.author = {'id': 1}
        merge_request.iid = 2
        project = mock.MagicMock()
        sdiff.return_value = "SUBMITTED_DIFF_OUTPUT", []
        udiff.return_value = "UPSTREAM_DIFF_OUTPUT", "xyz@example.com"
        set_dep.return_value = 'Dependencies::4567'
        commit = mock.Mock(id="4567", author_email="shadowman@redhat.com",
                           message="1\n", parent_ids=["1234"])
        commit.diff = mock.Mock(return_value=[{'newPath': 'redhat/rhdocs/info/owners.yaml'}])
        merge_request.diff_refs['start_sha'] = 'abbadabba'
        merge_request.labels = ['Dependencies::4567']
        merge_request.commits.return_value = [commit]
        cmr.merge_request = merge_request
        cmr.project = project
        cmr.project.mergerequests.get.return_value = merge_request
        cmr.rhcommits = []
        cmr.commits_with_diffs = {}
        cmr.commits_with_diffs[commit.id] = commit
        cmr.nids = {'rhelonly': 0}
        cmr.ucids = []
        cmr.omitted = []
        cmr.fixes = {}
        cmr.errors = ''
        cmr.first_dep_sha = None
        ext_deps.return_value = []
        f_labels.return_value = ([], [])
        commit_compare.CommitsMR.check_upstream_commit_ids(cmr)

    @mock.patch('git.Repo')
    def test_get_upstream_diff_commit_not_found(self, mocked_repo):
        """Test the code that does not find the coomit ID."""
        cmr = mock.Mock()
        cmr.repo = mocked_repo.return_value
        ucid = "a1b2c3d4e5"
        filelist = []
        mocked_repo.return_value.commit.side_effect = Exception('Commit not found!')
        self.assertEqual(commit_compare.CommitsMR.get_upstream_diff(cmr, ucid, filelist),
                         (None, ""))

    @mock.patch('git.Repo')
    def test_get_upstream_diff_commit_found_but_filelist_empty(self, mocked_repo):
        """Test the code that does not find the coomit ID."""
        cmr = mock.Mock()
        cmr.repo = mocked_repo.return_value
        ucid = "a1b2c3d4e5"
        filelist = []
        commit = mock.MagicMock()
        commit.author.email = "shadowman@redhat.com"
        mocked_repo.return_value.commit.return_value = commit
        mocked_repo.return_value.git.diff.return_value = PATCH_D
        self.assertEqual(commit_compare.CommitsMR.get_upstream_diff(cmr, ucid, filelist),
                         (PATCH_D, "shadowman@redhat.com"))

    @mock.patch('git.Repo')
    def test_get_upstream_diff_full(self, mocked_repo):
        """Test the code that does not find the coomit ID."""
        cmr = mock.Mock()
        cmr.repo = mocked_repo.return_value
        ucid = "a1b2c3d4e5"
        filelist = ['include/linux/netdevice.h']
        commit = mock.MagicMock()
        commit.author.email = "shadowman@redhat.com"
        mocked_repo.return_value.commit.return_value = commit
        mocked_repo.return_value.git.diff.return_value = PATCH_D
        self.assertEqual(commit_compare.CommitsMR.get_upstream_diff(cmr, ucid, filelist),
                         (PATCH_B, "shadowman@redhat.com"))

    def test_commit_notes(self):
        cmr = self.setup_cs9_mr(4776)

        rhcommit = mock.Mock(ucids=[], match=commit_compare.Match.FULL)
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.NOUCID)
        self.assertEqual(cmr.notes[0], commit_compare.Match.NOUCID.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.NOUCID], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.NOUCID)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.RHELONLY)
        self.assertEqual(cmr.notes[0], commit_compare.Match.RHELONLY.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.RHELONLY], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.RHELONLY)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.POSTED)
        self.assertEqual(cmr.notes[0], commit_compare.Match.POSTED.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.POSTED], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.POSTED)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.UNRECOGNIZED)
        self.assertEqual(cmr.notes[0], commit_compare.Match.UNRECOGNIZED.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.UNRECOGNIZED], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.UNRECOGNIZED)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.KABI)
        self.assertEqual(cmr.notes[0], commit_compare.Match.KABI.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.KABI], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.KABI)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.DIFFS)
        self.assertEqual(cmr.notes[0], commit_compare.Match.DIFFS.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.DIFFS], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.DIFFS)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.PARTIAL)
        self.assertEqual(cmr.notes[0], commit_compare.Match.PARTIAL.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.PARTIAL], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.PARTIAL)

        cmr.notes = []
        cmr.update_commit_match_data(rhcommit, commit_compare.Match.CONFIG)
        self.assertEqual(cmr.notes[0], commit_compare.Match.CONFIG.footnote)
        self.assertEqual(cmr.nids[commit_compare.Match.CONFIG], '1')
        self.assertEqual(rhcommit.match, commit_compare.Match.CONFIG)

        rhcommit.ucids = ["-"]
        cmr.no_upstream_commit_data(rhcommit)
        self.assertEqual(rhcommit.match, commit_compare.Match.NOUCID)

        rhcommit.ucids = ["RHELonly"]
        cmr.no_upstream_commit_data(rhcommit)
        self.assertEqual(rhcommit.match, commit_compare.Match.RHELONLY)

        rhcommit.ucids = ["Posted"]
        cmr.no_upstream_commit_data(rhcommit)
        self.assertEqual(rhcommit.match, commit_compare.Match.POSTED)

    def test_labels_and_scopes(self):
        cmr = self.setup_cs9_mr(4776)

        cmr.errors = "There are some errors"
        self.assertEqual(defs.MrScope.NEEDS_REVIEW, cmr.overall_commits_scope)
        self.assertEqual("CommitRefs::NeedsReview", cmr.commit_compare_label)

        cmr.errors = ""
        rhcommit = mock.Mock()
        rhcommit.commit.sha = '4321'
        rhcommit.ucids = ['8765']
        rhcommit.match = commit_compare.Match.NOUCID
        rhcommit.notes = []
        cmr.rhcommits = [rhcommit]
        self.assertFalse(cmr.approved)
        self.assertEqual(defs.MrScope.MISSING, cmr.overall_commits_scope)
        self.assertEqual("CommitRefs::Missing", cmr.commit_compare_label)
        self.assertFalse(cmr.needs_config_ark_label)

        rhcommit.match = commit_compare.Match.FULL
        self.assertTrue(cmr.approved)
        self.assertEqual(defs.MrScope.OK, cmr.overall_commits_scope)
        self.assertEqual("CommitRefs::OK", cmr.commit_compare_label)
        self.assertFalse(cmr.needs_config_ark_label)
        self.assertFalse(cmr.needs_kabi_label)

        rhcommit.match = commit_compare.Match.CONFIG
        rhcommit2 = mock.Mock()
        rhcommit2.commit.sha = '5432'
        rhcommit2.ucids = ['9876']
        rhcommit2.match = commit_compare.Match.KABI
        rhcommit2.notes = []
        cmr.rhcommits.append(rhcommit2)
        self.assertTrue(cmr.needs_config_ark_label)
        self.assertTrue(cmr.needs_kabi_label)

        expected_labels = ["CommitRefs::OK", "Dependencies::OK", "KABI", "Config::ARK::Mismatch"]
        self.assertCountEqual(expected_labels, cmr.expected_labels)

    def test_get_diff_from_gql(self):
        """Test that we can properly extract patch content from a submitted diff."""
        commit = []
        path = {'oldPath': 'include/linux/netdevice.h',
                'newPath': 'include/linux/netdevice.h',
                'newFile': "false",
                'renamedFile': "false",
                'deletedFile': "false",
                'aMode': '100644',
                'bMode': '100644',
                'diff': "@@ -1955,6 +1955,7 @@ struct net_device {\n"
                        "        unsigned short          type;\n"
                        "        unsigned short          hard_header_len;\n"
                        "        unsigned char           min_header_len;\n"
                        "+       unsigned char           name_assign_type;\n"
                        "\n"
                        "        unsigned short          needed_headroom;\n"
                        "        unsigned short          needed_tailroom;\n"}
        commit.append(path)
        output = cdlib.get_diff_from_gql(commit)
        self.assertEqual(output[0], PATCH_A)
        self.assertEqual(output[1], ["include/linux/netdevice.h"])
        commit = []
        path['newFile'] = "true"
        commit.append(path)
        output = cdlib.get_diff_from_gql(commit)
        self.assertEqual(output[0], PATCH_E)
        commit = []
        path['newFile'] = "false"
        path['deletedFile'] = "true"
        commit.append(path)
        output = cdlib.get_diff_from_gql(commit)
        self.assertEqual(output[0], PATCH_F)
        commit = []
        path['newFile'] = "false"
        path['deletedFile'] = "false"
        path['renamedFile'] = "true"
        path['newPath'] = 'include/linux/foobar.h'
        commit.append(path)
        output = cdlib.get_diff_from_gql(commit)
        self.assertEqual(output[0], PATCH_G)

    def test_partial_diff(self):
        """Check that we can match partial diffs."""
        filelist = []
        filelist.append("include/linux/netdevice.h")
        pdiff = cdlib.get_partial_diff(PATCH_D, filelist)
        output = cdlib.compare_commits(pdiff, PATCH_A)
        self.assertEqual(output, [])

    def test_has_upstream_commit_hash(self):
        """Check that we get expected return values from different match types."""
        m1 = commit_compare.Match.FULL
        m2 = commit_compare.Match.PARTIAL
        m3 = commit_compare.Match.DIFFS
        m4 = commit_compare.Match.NOUCID
        m5 = commit_compare.Match.RHELONLY
        m6 = commit_compare.Match.POSTED
        self.assertTrue(m1.has_upstream_commit_hash)
        self.assertTrue(m2.has_upstream_commit_hash)
        self.assertTrue(m3.has_upstream_commit_hash)
        self.assertFalse(m4.has_upstream_commit_hash)
        self.assertFalse(m5.has_upstream_commit_hash)
        self.assertFalse(m6.has_upstream_commit_hash)

    def test_extract_upstream_commit_id(self):
        cmr = mock.Mock()
        cmr.ucids = set()
        cmr.omitted = set()
        cmr.errors = ''
        c1 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 1",
                       description=Desc("1\ncommit 1234567890abcdef1234567890abcdef12345678"),
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567", author_email="xyz@example.com",
                       title="This is commit 2",
                       description=Desc("2\ncommit a012345"),
                       parent_ids=["1234"])
        c3 = mock.Mock(id="abcd", author_email="shadowman2021@redhat.com",
                       title="This is commit 3",
                       description=Desc("2\n"
                                        "Upstream Status: git://linux-nfs.org/~bfields/linux.git"),
                       parent_ids=["5678"])
        c4 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 4",
                       description=Desc("1\ncommit 1234567890abcdef1234567890abcdef12345678"
                                        "\ncommit a234567890abcdef1234567890abcdef12345678"
                                        "\ncommit b234567890abcdef1234567890abcdef12345678"
                                        "\ncommit c234567890abcdef1234567890abcdef12345678"
                                        "\ncommit d234567890abcdef1234567890abcdef12345678"
                                        "\ncommit e234567890abcdef1234567890abcdef12345678"),
                       parent_ids=["abcd"])
        c5 = mock.Mock(id="8765", author_email="developer@redhat.com",
                       title="This is commit 5",
                       description=Desc("5\ncommit  1234567890"),
                       short_sha="abcdefg1", parent_ids=["1234"])
        c6 = mock.Mock(id="9745", author_email="developer@redhat.com",
                       title="This is commit 6",
                       description=Desc("6\n (cherry picked from commit 1234567890abcdef)"),
                       short_sha="abcdefg2", parent_ids=["1235"])
        c8 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       description=Desc("1\n(cherry picked from commit "
                                        "1234567890abcdef1234567890abcdef12345678)"
                                        "\n (cherry picked from commit "
                                        "2234567890abcdef1234567890abcdef12345678)"
                                        "\n(cherry picked from commit   "
                                        "3234567890abcdef1234567890abcdef12345678)"),
                       parent_ids=["abcd"])
        c9 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                       title="This is commit 8",
                       description=Desc("1\ncommit 1234567890abcdef1234567890abcdef12345678"
                                        "\ncommit 2234567890abcdef1234567890abcdef12345678"
                                        "\n commit 3234567890abcdef1234567890abcdef12345678"
                                        "\ncommit 4234567890abcdef1234567890abcdef12345678"
                                        "\ncommit  5234567890abcdef1234567890abcdef12345678"),
                       parent_ids=["abcd"])
        c10 = mock.Mock(id="1234567890", author_email="jdoe@redhat.com",
                        title="This is a commit",
                        description=Desc("XYZ\n"
                                         "Upstream Status: https://github.com/torvalds/linux.git"),
                        parent_ids=["abcdef0123"])
        c11 = mock.Mock(id="1234", author_email="jdoe@redhat.com",
                        title="This is commit 11",
                        description=Desc("1\n"
                                         "Upstream Status: RHEL Only\n"),
                        parent_ids=["abcd"])
        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c1)
        self.assertEqual(commit_list, ["1234567890abcdef1234567890abcdef12345678"])

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c2)
        self.assertEqual(commit_list, [])

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c3)
        self.assertEqual(commit_list, ['Posted'])

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c4)
        self.assertEqual(commit_list, ['RHELonly'])

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c5)
        self.assertEqual(commit_list, ['1234567890'])
        self.assertEqual(cmr.errors,
                         '\n* Incorrect format git commit line in abcdefg1.  \n'
                         '`Expected:` `commit 1234567890`  \n'
                         '`Found   :` `commit  1234567890`  \n'
                         'Git hash only has 10 characters, expected 40.  \n')

        cmr.errors = ''
        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c6)
        self.assertEqual(commit_list, ['1234567890abcdef'])
        self.assertEqual(cmr.errors,
                         '\n* Incorrect format git cherry-pick line in abcdefg2.  \n'
                         '`Expected:` `(cherry picked from commit 1234567890abcdef)`  \n'
                         '`Found   :` ` (cherry picked from commit 1234567890abcdef)`  \n'
                         'Git hash only has 16 characters, expected 40.  \n')

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c8)
        self.assertEqual(commit_list, ["1234567890abcdef1234567890abcdef12345678"])

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c9)
        self.assertEqual(commit_list, ["1234567890abcdef1234567890abcdef12345678",
                                       "2234567890abcdef1234567890abcdef12345678",
                                       "4234567890abcdef1234567890abcdef12345678"])

        # This commit should NOT be considered Posted, as Linus' tree always needs a hash
        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c10)
        self.assertEqual(commit_list, [])

        commit_list = commit_compare.CommitsMR.extract_upstream_commit_id(cmr, c11)
        self.assertEqual(commit_list, ['RHELonly'])

    def _test_payload(self, result, payload):
        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}):
            self.clear_caches()
            self._test(result, payload)
        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'development'}):
            self.clear_caches()
            self._test(result, payload)

    @mock.patch.object(DependsMixin, '_set_mr_blocks', mock.Mock())
    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment')
    @mock.patch('webhook.commit_compare.CommitsMR.remove_labels')
    @mock.patch('webhook.commit_compare.CommitsMR.add_labels')
    def _test(self, result, payload, add_labels, remove_labels, update_webhook_comment):
        # setup dummy gitlab data
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.Mock(target_branch=target)
        merge_request.iid = 4776

        # We're not actually using the CommitsMR object set up, just the responses
        self.setup_cs9_mr(merge_request.iid)

        parser = get_arg_parser('COMMIT_COMPARE')
        parser.add_argument('--linux-src')
        args = parser.parse_args('--linux-src /src/linux'.split())
        args.disable_user_check = True

        mock_session = SessionRunner.new('commit_compare', args, commit_compare.HANDLERS)
        mock_session.rh_projects = mock.Mock()
        event = create_event(mock_session, {'message-type': 'gitlab'}, payload)
        event.gl_mr = merge_request

        commit_compare.process_gl_event({}, mock_session, event)

        if result:
            add_labels.assert_not_called()
            remove_labels.assert_not_called()
            update_webhook_comment.assert_called()

    def test_read_config_content(self):
        worktree_dir = '/tmp/worktree'
        filename = 'redhat/configs/common/generic/CONFIG_RV'
        arkfilename = 'redhat/configs/rhel/generic/CONFIG_RV'

        with mock.patch('os.path.exists', return_value=True):
            with mock.patch('builtins.open', mock.mock_open()) as m:
                commit_compare.fetch_file_content(worktree_dir, filename, arkfilename)
                m.assert_called_with(f'{worktree_dir}/{arkfilename}', 'r', encoding='ascii')
                handle = m()
                handle.read.assert_called()

    def test_read_config_content_that_no_exists(self):
        worktree_dir = '/foo/bar/baz'
        filename = 'redhat/configs/common/generic/CONFIG_RV'
        arkfilename = 'redhat/configs/rhel/generic/CONFIG_RV'

        self.assertEqual(commit_compare.fetch_file_content(worktree_dir, filename, arkfilename),
                         (None, None))

    def test_compare_config_diffs(self):
        sha = "abcd12345678"
        new_cfg = ""
        common_cfg = ""
        ark_cfg = ""

        msg = commit_compare.compare_config_diffs(sha, new_cfg, common_cfg, ark_cfg)
        self.assertEqual("", msg)

        new_cfg = "CONFIG_FOO=m"
        msg = commit_compare.compare_config_diffs(sha, new_cfg, common_cfg, ark_cfg)
        self.assertIn("RHEL commit abcd12345678 (`CONFIG_FOO=m`) does not match", msg)

        common_cfg = "CONFIG_FOO=m"
        msg = commit_compare.compare_config_diffs(sha, new_cfg, common_cfg, ark_cfg)
        self.assertEqual("", msg)

        common_cfg = ""
        ark_cfg = "CONFIG_FOO=m"
        msg = commit_compare.compare_config_diffs(sha, new_cfg, common_cfg, ark_cfg)
        self.assertEqual("", msg)

        ark_cfg = "# CONFIG_FOO is not set"
        msg = commit_compare.compare_config_diffs(sha, new_cfg, common_cfg, ark_cfg)
        self.assertIn("RHEL commit abcd12345678 (`CONFIG_FOO=m`) does not match", msg)

    def test_get_config_change(self):
        path = {'oldPath': 'not/redhat/configs/CONFIG_FOO',
                'newPath': '/dev/null',
                'newFile': False,
                'renamedFile': False,
                'deletedFile': True,
                'aMode': '100644',
                'bMode': '100644',
                'diff': "@@ -0,0 +1 @@\n"
                        "+CONFIG_FOO=m\n"}
        setting, file = commit_compare.get_config_change(path)
        self.assertEqual('', setting)
        self.assertIsNone(file)

        path['oldPath'] = 'redhat/configs/CONFIG_FOO'
        setting, file = commit_compare.get_config_change(path)
        self.assertIsNone(setting)
        self.assertEqual('redhat/configs/CONFIG_FOO', file)

        path['deletedFile'] = False
        path['renamedFile'] = True
        setting, file = commit_compare.get_config_change(path)
        self.assertEqual('', setting)
        self.assertIsNone(file)

        path['newFile'] = True
        path['renamedFile'] = False
        path['oldPath'] = '/dev/null'
        path['newPath'] = 'redhat/configs/CONFIG_FOO'
        setting, file = commit_compare.get_config_change(path)
        self.assertEqual("CONFIG_FOO=m", setting)
        self.assertEqual('redhat/configs/CONFIG_FOO', file)

        path['newPath'] = 'not/redhat/configs/CONFIG_FOO'
        setting, file = commit_compare.get_config_change(path)
        self.assertEqual('', setting)
        self.assertIsNone(file)

    @mock.patch('webhook.kgit.branch_delete')
    @mock.patch('webhook.kgit.worktree_remove')
    def test_clean_up_temp_ark_branch(self, mock_remove, mock_delete):
        cmr = mock.Mock()
        cmr.linux_src = '/src/linux'
        cmr.worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        cmr.ark_branch = 'temp-merge-branch'
        commit_compare.CommitsMR.clean_up_temp_ark_branch(cmr)
        mock_remove.assert_called_with(cmr.linux_src, cmr.worktree_dir)
        mock_delete.assert_called_with(cmr.linux_src, cmr.ark_branch)

    @mock.patch('os.path.exists', mock.Mock(return_value=False))
    @mock.patch('webhook.commit_compare.CommitsMR.clean_up_temp_ark_branch')
    def test_handle_stale_worktree(self, clean_up):
        cmr = mock.Mock()
        cmr.linux_src = '/src/kernel'
        cmr.ark_branch = 'kernel-ark/os-build'
        cmr.clean_up_temp_ark_branch = clean_up

        m_args = ['git', 'branch', '-D', f'{cmr.ark_branch}-save']
        self._add_run_result(m_args, 4, 'Uhhh yeah it exploded')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            commit_compare.CommitsMR.handle_stale_worktree(cmr)

        with mock.patch('webhook.kgit.branch_delete') as kgbd:
            commit_compare.CommitsMR.handle_stale_worktree(cmr)
            kgbd.assert_called_with(cmr.linux_src, f"{cmr.ark_branch}-save")

    @mock.patch('subprocess.run', mock.Mock())
    @mock.patch('webhook.commit_compare.CommitsMR.prep_temp_ark_branch', mock.Mock())
    def _test_validate_config(self, mocked_read, diffs):
        """Check if a config matches with upstream repo."""
        cmr = self.setup_cs9_mr(4776)

        c = mock.Mock(sha="abcdef012345", author_email="jdoe@redhat.com", description=Desc(""))
        c.diff = diffs

        cmr.rhcommits = []
        cmr.rhcommits.append(commit_compare.RHCommit(c))

        cmr.evaluate_config(c)
        report = cmr.config_differences_report()
        if cmr.commit_config_mismatches:
            self.assertIn("further review may be necessary", report)
        else:
            self.assertEqual("", report)
        return bool(cmr.commit_config_mismatches)

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_addition_match(self, read, add_label):
        """Check if a config matches with rhel configs in upstream repo."""
        read.return_value = "CONFIG_RV=y"
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'newFile': True,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_addition_match_in_ark(self, read, add_label):
        """Check if a config matches with ark configs in upstream repo."""
        read.side_effect = [None, "CONFIG_RV=y"]
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': True,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_change_match(self, read, add_label):
        """Check if a config matches with upstream repo."""
        read.return_value = "CONFIG_MEMTEST=y"
        diffs = [{'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': False,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +1 @@\n"
                          "-# CONFIG_MEMTEST is not set\n"
                          "+CONFIG_MEMTEST=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_validate_config_change_mismatch(self, read, add_label):
        """Check if a config matches with upstream repo."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': False,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +1 @@\n"
                          "-# CONFIG_MEMTEST is not set\n"
                          "+CONFIG_MEMTEST=y\n"}]

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.assertTrue(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_validate_config_addition_that_does_not_exist_in_both(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        read.side_effect = [None, None, None, None]
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'newFile': True,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"},
                 {"newPath": "kernel/rh_messages.c",
                  "oldPath": "kernel/rh_messages.c",
                  "aMode": "100644",
                  "bMode": "100644",
                  "newFile": False,
                  "renamedFile": False,
                  "deletedFile": False,
                  "generated_file": None,
                  "diff": "@@ -163,7 +163,7 @@ void mark_tech_preview(const char *msg, struct "
                          "module *mod)\n \tif (msg)\n \t\tstr = msg;\n #ifdef CONFIG_MODULES\n"
                          "-\telse if (mod && mod->name)\n+\telse if (mod)\n "
                          "\t\tstr = mod->name;\n #endif\n \n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
                self.assertTrue(self._test_validate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_validate_config_addition_that_exists_but_not_enabled(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        read.return_value = "# CONFIG_RV is not set"
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': True,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            # Skip staging test here just to cover a different branch
            self.assertTrue(self._test_validate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_validate_config_addition_does_not_exist(self, read, add_label):
        """Check if a config file exists but it does not match with commit."""
        diffs = [{'oldPath': '/dev/null',
                  'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': True,
                  'renamedFile': False,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -0,0 +1 @@\n"
                          "+CONFIG_RV=y\n"}]

        exp = "A CONFIG change in RHEL commit abcdef012345 (`CONFIG_RV=y`) does not match ARK"
        with self.assertLogs('cki.webhook.commit_compare', level='DEBUG') as logs:
            with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
                self.assertTrue(self._test_validate_config(read, diffs))
            self.assertIn(exp, "\n".join(logs.output))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_deletion_match(self, read, add_label):
        """Check if a config deletion matches with upstream repo."""
        read.return_value = None
        diffs = [{'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': False,
                  'renamedFile': False,
                  'deletedFile': True,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +0,0 @@\n"
                          "-CONFIG_RV=y\n"}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment',
                mock.Mock(return_value=None))
    def test_validate_config_deletion_mismatch(self, read, add_label):
        """Check if a config deletion mismatches with upstream repo."""
        read.return_value = 'CONFIG_RV=y'
        diffs = [{'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'oldPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'newFile': False,
                  'renamedFile': False,
                  'deletedFile': True,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': "@@ -1 +0,0 @@\n"
                          "-CONFIG_RV=y\n"}]

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.assertTrue(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_config_rename_match(self, read, add_label):
        """Check if a config renaming matches with upstream repo."""
        read.return_value = None
        diffs = [{'newPath': 'redhat/configs/common/generic/CONFIG_RV',
                  'oldPath': 'redhat/configs/rhel/generic/CONFIG_RV',
                  'newFile': False,
                  'renamedFile': True,
                  'deletedFile': False,
                  'aMode': '100644',
                  'bMode': '100644',
                  'diff': ""}]

        self.assertFalse(self._test_validate_config(read, diffs))

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.commit_compare.read_config_content')
    def test_validate_documentation_skip(self, read, add_label):
        """Check if a config renaming matches with upstream repo."""
        read.return_value = None
        diffs = [{'newPath': 'Documentation/dev-tools/testing-overview.rst',
                  'oldPath': 'Documentation/dev-tools/testing-overview.rst',
                  'newFile': True,
                  'renamedFile': False,
                  'deletedFile': False,
                  'generated_file': None,
                  'aMode': '0',
                  'bMode': '100644',
                  'diff': PATCH_H}]

        self.assertFalse(self._test_validate_config(read, diffs))

    def test_validate_x86_64_file_map(self):
        """Check if a config inside a x86_64 directory in ARK match with downstream versions."""
        filename = "redhat/configs/rhel/generic/x86/x86_64/CONFIG_NVRAM"
        commonfilename_exp = "redhat/configs/common/generic/x86/CONFIG_NVRAM"
        arkfilename_exp = "redhat/configs/rhel/generic/x86/CONFIG_NVRAM"

        commonfilename, arkfilename = commit_compare.x86_filename_replacement(filename)
        self.assertEqual(commonfilename, commonfilename_exp)
        self.assertEqual(arkfilename, arkfilename_exp)

        filename = "redhat/configs/common/generic/x86/x86_64/CONFIG_GENERIC_CPU"
        commonfilename_exp = "redhat/configs/common/generic/x86/CONFIG_GENERIC_CPU"
        arkfilename_exp = "redhat/configs/rhel/generic/x86/CONFIG_GENERIC_CPU"

        commonfilename, arkfilename = commit_compare.x86_filename_replacement(filename)
        self.assertEqual(commonfilename, commonfilename_exp)
        self.assertEqual(arkfilename, arkfilename_exp)

        filename = "redhat/configs/common/generic/CONFIG_RV"
        rhelfilename = filename.replace('common', 'rhel')
        commonfilename, arkfilename = commit_compare.x86_filename_replacement(filename)
        # We don't expect any change
        self.assertEqual(commonfilename, filename)
        self.assertEqual(arkfilename, rhelfilename)
