"""Tests for the common main() function bits."""
import argparse
import importlib
import os
import typing
from unittest import mock

from tests.helpers import KwfTestCase
from webhook.common import get_arg_parser


def get_common_args(webhook_prefix: str) -> list[str]:
    """Return the list of arguments from common.get_arg_parser."""
    # Exclude --help since it is not provided as an attribute of the parsed args.
    return [option_str for
            action in get_arg_parser(webhook_prefix)._actions for
            option_str in action.option_strings if
            option_str.startswith('--') and
            option_str != '--help']


class TestMain(KwfTestCase):
    """Tests for the common main() bits."""

    # Additional arguments that each hook is expected to have. Used by hook_main_test.
    EXTRA_ARGUMENTS: dict[str, list[str]] = {
        'ack_nack': ['--rhkernel-src'],
        'commit_compare': ['--linux-src'],
        'fixes': ['--linux-src', '--kerneloscope-server-url', '--no-kerneloscope'],
        'mergehook': ['--rhkernel-src', '--kerneloscope-server-url', '--no-kerneloscope'],
        'sast': ['--sast-rest-api-url', '--gl-projects'],
        'umb_bridge': ['--rabbitmq-sender-exchange', '--rabbitmq-sender-route', '--no-send-queue']
    }

    class MainTest(typing.NamedTuple):
        """Parameters for TestMain.hook_main_test."""

        # Name of the webhook module to test the main function of.
        hook_name: str
        # Argument string to pass to the main function for this test (will be split).
        args_to_pass_in: str = ''
        # Environment vars to set for the test.
        environment: dict[str, str] | None = None
        # List of arguments expected to be reported as missing.
        arg_errors: list[str] | None = None
        # The type of Exception the test is expected to raise, if any.
        raises_exception: Exception | None = None
        # Whether the hook will call SessionRunner.run() in the successful case.
        calls_sessionrunner_run: bool = True

    def hook_main_test(self, test: MainTest) -> None:
        """Test a hook's main() function by validating it has the expected args."""
        # Common argument names provided by webhook.common.get_arg_parser..
        common_args = get_common_args(test.hook_name.upper())
        self.assertGreater(len(common_args), 0,
                           msg='Expect at least one argument from get_arg_parser.')

        # Import the hook module.
        hook_module = importlib.import_module(f'webhook.{test.hook_name}')

        # Clear the environment so it only has the MainTest.environment values.
        patched_env = mock.patch.dict(os.environ, test.environment or {}, clear=True)
        self.addCleanup(patched_env.stop)
        patched_env.start()

        # Patch out argparse.ArgumentParser.error() so we can see if and how it was called.
        patch_argparse_error = mock.patch.object(argparse.ArgumentParser, 'error')
        self.addCleanup(patch_argparse_error.stop)
        patched_argparse_error = patch_argparse_error.start()

        # Patch out common.init_sentry() so we can confirm it is being called.
        patch_init_sentry = mock.patch('webhook.common.init_sentry')
        self.addCleanup(patch_init_sentry.stop)
        patched_init_sentry = patch_init_sentry.start()

        # Patch out SessionRunner.run() so we can confirm when it is called.
        patch_session_runner_run = mock.patch.object(hook_module.SessionRunner, 'run')
        self.addCleanup(patch_session_runner_run.stop)
        patched_session_runner_run = patch_session_runner_run.start()

        # Patch the SessionRunner class with wraps so we can see how it is called.
        patch_session_runner = mock.patch(
            f'webhook.{test.hook_name}.SessionRunner',
            wraps=hook_module.SessionRunner
        )
        self.addCleanup(patch_session_runner.stop)
        patched_session_runner = patch_session_runner.start()

        # Testing begins here.

        # If the test raises an exception then make sure it is the right one and return.
        if test.raises_exception:
            with self.assertRaises(test.raises_exception):
                hook_module.main(test.args_to_pass_in.split())
            return

        # Call the hook's main() function with the test.args_to_pass_in.
        hook_module.main(test.args_to_pass_in.split())

        # Make sure we all are calling common.init_sentry().
        patched_init_sentry.assert_called_once()

        # If argparse errors are expected then check for them and return.
        if test.arg_errors:
            patched_argparse_error.assert_called_once()
            argparse_error_str = patched_argparse_error.call_args.args[0]

            for arg_error in test.arg_errors:
                with self.subTest(expected_argument_error=arg_error):
                    msg = f'Expected to find {arg_error} in error string: "{argparse_error_str}"'

                    self.assertIn(arg_error, argparse_error_str, msg=msg)
            return

        # argparse errors were not expected so make sure error() was not called.
        patched_argparse_error.assert_not_called()

        # Confirm SessionRunner.new() was called with the expected parameters.
        patched_session_runner.new.assert_called_once_with(
            test.hook_name,
            args=mock.ANY,
            handlers=hook_module.HANDLERS
        )

        # Get the argparse.Namespace instance so we can confirm which arguments it has.
        argparse_namespace = patched_session_runner.new.call_args.kwargs['args']

        # Get the args attribute names and convert them back into option string format.
        found_args = {f'--{arg.replace("_", "-")}' for arg in argparse_namespace.__dict__.keys()}

        # Confirm all the common_args are in found_args.
        for arg_name in common_args:
            with self.subTest():
                self.assertIn(arg_name, found_args,
                              msg=f'Expected to find {arg_name} in {found_args}')

        # Confirm the remaining found_args == self.EXTRA_ARGUMENTS.
        extra_args = found_args - set(common_args)
        self.assertCountEqual(
            extra_args,
            self.EXTRA_ARGUMENTS.get(test.hook_name, []),
            msg='Expected every argument not provided by common.get_arg_parser()'
                ' to be in MainTest.EXTRA_ARGUMENTS'
        )

        # Confirm that SessionRunner.run() was called.
        if test.calls_sessionrunner_run:
            patched_session_runner_run.assert_called_once()

    def test_ack_nack_missing_args(self) -> None:
        """Argparse reports an error for ack_nack due to missing --rhkernel-src."""
        self.hook_main_test(self.MainTest(
            hook_name='ack_nack',
            arg_errors=['--rhkernel-src']
        ))

    def test_ack_nack_good_via_env(self) -> None:
        """Required --rhkernel-src argument value for ack_nack is provided via RHKERNEL_SRC env."""
        self.hook_main_test(self.MainTest(
            hook_name='ack_nack',
            environment={'RHKERNEL_SRC': '/path'},
        ))

    def test_ack_nack_good_via_args(self) -> None:
        """Required --rhkernel-src argument value for ack_nack is provided via the argument."""
        self.hook_main_test(self.MainTest(
            hook_name='ack_nack',
            args_to_pass_in='--rhkernel-src /beep/boop/kernel'
        ))

    def test_buglinker(self) -> None:
        """Buglinker has no extra options and just works."""
        self.hook_main_test(self.MainTest(
            hook_name='buglinker',
        ))

    def test_ckihook(self) -> None:
        """Ckihook has no extra options and just works."""
        self.hook_main_test(self.MainTest(
            hook_name='ckihook',
        ))

    def test_commit_compare_missing_args(self) -> None:
        """Argparse reports an error for commit_compare due to missing --linux-src."""
        self.hook_main_test(self.MainTest(
            hook_name='commit_compare',
            arg_errors=['--linux-src']
        ))

    def test_commit_compare_good_via_env(self) -> None:
        """Required --linux-src argument value for commit-compare is provided via LINUX_SRC env."""
        self.hook_main_test(self.MainTest(
            hook_name='commit_compare',
            environment={'LINUX_SRC': '/linux-src'},
        ))

    def test_commit_compare_good_via_args(self) -> None:
        """Required --linux-src argument value for commit-compare is provided via the argument."""
        self.hook_main_test(self.MainTest(
            hook_name='commit_compare',
            args_to_pass_in='--linux-src /path/to/linux'
        ))

    def test_fixes_missing_args(self) -> None:
        """Argparse reports an error for fixes due to missing --linux-src."""
        self.hook_main_test(self.MainTest(
            hook_name='fixes',
            arg_errors=['--linux-src']
        ))

    def test_fixes_good_via_env(self) -> None:
        """Required --linux-src argument value for fixes is provided via LINUX_SRC env."""
        self.hook_main_test(self.MainTest(
            hook_name='fixes',
            environment={'LINUX_SRC': '/linux-src',
                         'KERNELOSCOPE_SERVER_URL': 'https://kerneloscope.example.com'},
        ))

    def test_fixes_good_via_args(self) -> None:
        """Required --linux-src argument value for fixes is provided via the argument."""
        self.hook_main_test(self.MainTest(
            hook_name='fixes',
            args_to_pass_in=('--linux-src /path/to/linux '
                             '--kerneloscope-server-url https://kerneloscope.example.com')
        ))

    def test_jirahook(self) -> None:
        """Jirahook has no extra options and just works."""
        self.hook_main_test(self.MainTest(
            hook_name='jirahook',
            args_to_pass_in='--repo-path /path/to/linux'
        ))

    def test_mergehook_missing_args(self) -> None:
        """Argparse reports an error for mergehook due to missing --rhkernel-src."""
        self.hook_main_test(self.MainTest(
            hook_name='mergehook',
            arg_errors=['--rhkernel-src']
        ))

    def test_mergehook_good_via_env(self) -> None:
        """Required arguments for mergehook provided via env."""
        self.hook_main_test(self.MainTest(
            hook_name='mergehook',
            environment={'RHKERNEL_SRC': '/path',
                         'KERNELOSCOPE_SERVER_URL': 'https://kerneloscope.example.com'},
        ))

    def test_mergehook_good_via_args(self) -> None:
        """Required arguments for mergehook provided via cli arguments."""
        self.hook_main_test(self.MainTest(
            hook_name='mergehook',
            args_to_pass_in=('--rhkernel-src /beep/boop/kernel '
                             '--kerneloscope-server-url https://kerneloscope.example.com')
        ))

    def test_sast_missing_args(self) -> None:
        """Argparse reports an error for sast due to missing arguments."""
        self.hook_main_test(self.MainTest(
            hook_name='sast',
            arg_errors=['--sast-rest-api-url']
        ))

    def test_sast_nudger_good_via_env(self) -> None:
        """Required arguments for sast nudger are provided via the environment."""
        with mock.patch('webhook.sast.nudger') as patched_nudger:
            self.hook_main_test(self.MainTest(
                hook_name='sast',
                calls_sessionrunner_run=False,
                environment={
                    'KERNEL_SAST_AUTOMATION_SERVER_REST_API': 'https://example.com/rest/',
                    'GL_PROJECTS': 'group/project'
                },
            ))

        patched_nudger.assert_called_once()

    def test_sast_good_via_args(self) -> None:
        """Required arguments for sast are provided via arguments."""
        self.hook_main_test(self.MainTest(
            hook_name='sast',
            args_to_pass_in='--sast-rest-api-url https://example.com --gl-projects group/project'
        ))

    def test_signoff(self) -> None:
        """Signoff hook has no extra options and just works."""
        self.hook_main_test(self.MainTest(
            hook_name='signoff',
        ))

    def test_sprinter(self) -> None:
        """Sprinter has no extra options and just works."""
        self.hook_main_test(self.MainTest(
            hook_name='sprinter',
        ))

    def test_subsystems_missing_args(self) -> None:
        """Subsystems raises an exception due to empty --repo-path value."""
        self.hook_main_test(self.MainTest(
            hook_name='subsystems',
            raises_exception=RuntimeError
        ))

    def test_subsystems_good_via_env(self) -> None:
        """Required --repo-path argument value for subsystems is provided via REPO_PATH env."""
        self.hook_main_test(self.MainTest(
            hook_name='subsystems',
            environment={'REPO_PATH': '/data/repo'},
        ))

    def test_subsystems_good_via_args(self) -> None:
        """Required --reop-path argument value for subsystems is provided via the argument."""
        self.hook_main_test(self.MainTest(
            hook_name='subsystems',
            args_to_pass_in='--repo-path /beep/boop/kernel'
        ))

    def test_umb_bridge_missing_routing_key(self) -> None:
        """Umb_bridge raises an exception when rabbitmq_routing_key is not set."""
        self.hook_main_test(self.MainTest(
            hook_name='umb_bridge',
            raises_exception=RuntimeError
        ))

    def test_umb_bridge_no_send_queue(self) -> None:
        """Using --no-send-queue in production causes an exception."""
        self.hook_main_test(self.MainTest(
            hook_name='umb_bridge',
            environment={
                'CKI_DEPLOYMENT_ENVIRONMENT': 'production',
                'UMB_BRIDGE_ROUTING_KEYS': 'gitlab.com.cki-project.kernel-ark.merge_request',
                'UMB_BRIDGE_SENDER_EXCHANGE': 'exchange_string',
                'UMB_BRIDGE_SENDER_ROUTE': 'sender route string',
            },
            args_to_pass_in='--no-send-queue',
            raises_exception=RuntimeError
        ))

    def test_umb_bridge_good_via_env(self) -> None:
        """Required argument values for umb_bridge are provided via the environment."""
        with mock.patch('webhook.umb_bridge.MRInfoCache') as patched_mrinfo_cache:
            self.hook_main_test(self.MainTest(
                hook_name='umb_bridge',
                environment={
                    'UMB_BRIDGE_ROUTING_KEYS': 'gitlab.com.cki-project.kernel-ark.merge_request',
                    'UMB_BRIDGE_SENDER_EXCHANGE': 'exchange_string',
                    'UMB_BRIDGE_SENDER_ROUTE': 'sender route string',
                },
            ))

        patched_mrinfo_cache.new.assert_called_once()

    def test_umb_bridge_good_via_args(self) -> None:
        """Required arguments for umb_bridge are provided via command line."""
        with mock.patch('webhook.umb_bridge.MRInfoCache') as patched_mrinfo_cache:
            self.hook_main_test(self.MainTest(
                hook_name='umb_bridge',
                args_to_pass_in='--rabbitmq-routing-key '
                                'gitlab.com.cki-project.kernel-ark.merge_request '
                                '--rabbitmq-sender-exchange senderExchange '
                                '--rabbitmq-sender-route senderRoute'
            ))

        patched_mrinfo_cache.new.assert_called_once()
