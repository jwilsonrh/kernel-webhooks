"""Tests for merge_conflicts_check."""
import os
from unittest import mock

from tests.helpers import KwfTestCase
from webhook import defs
from webhook.utils import merge_conflicts_check


class TestMergeConflictsChecker(KwfTestCase):
    """Tests for the various helper functions."""

    MOCK_MR = {'iid': '66',
               'title': 'This is the title of my MR',
               'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/66',
               'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
               'targetBranch': 'main',
               'author': {'username': 'shadowman',
                          'name': 'Shadow Man',
                          'email': 'shadowman@redhat.com'},
               'labels': {'nodes': [{'title': 'Merge::OK',
                                     'description': 'This MR can be merged'}]},
               'draft': False}

    MOCK_MR2 = {'iid': '67',
                'title': 'This is the title of my 2nd MR',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/67',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'targetBranch': 'main',
                'author': {'username': 'shadowman',
                           'name': 'Shadow Man',
                           'email': 'shadowman@redhat.com'},
                'labels': {'nodes': [{'title': 'Merge::Warning',
                                      'description': 'This MR conflicts with other MRs'}]},
                'draft': False}

    MOCK_MR3 = {'iid': '68',
                'title': 'This is the title of my 3rd MR',
                'webUrl': 'https://gitlab.com/foo/bar/blah/-/merge_requests/68',
                'project': {'name': 'blah', 'fullPath': 'foo/bar/blah'},
                'targetBranch': 'main',
                'author': {'username': 'shadowman',
                           'name': 'Shadow Man',
                           'email': 'shadowman@redhat.com'},
                'labels': {'nodes': [{'title': 'Merge::OK',
                                      'description': 'This MR can be merged'}]},
                'draft': False}

    MOCK_MR_LIST = {f"foo/bar/{MOCK_MR['project']['name']}_{MOCK_MR['targetBranch']}":
                    [MOCK_MR, MOCK_MR2]}

    GQL_MRS = {'project':
               {'id': 'gid://gitlab/Project/1234',
                'mergeRequests':
                {'pageInfo': {'hasNextPage': False, 'endCursor': 'eyJjc'},
                 'nodes': [MOCK_MR, MOCK_MR2]}}}

    def test_get_open_mrs(self):
        mock_gql = mock.Mock()
        mock_gql.check_query_results.return_value = self.GQL_MRS
        namespace = 'foo/bar/blah'
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            merge_conflicts_check.get_open_mrs(mock_gql, namespace)
            self.assertIn(f"Project {namespace} MRs found: {self.MOCK_MR_LIST}", logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_prep_branch_for_merges(self):
        rhkernel_src = '/src/linux'
        mock_proj = mock.Mock()
        mock_proj.name = 'blah'
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        ret = merge_conflicts_check.prep_branch_for_merges(rhkernel_src, mr_params)
        self.assertEqual(ret, '/src/blah-main-megamerge/')

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_try_merging_all(self):
        mrs = [self.MOCK_MR2, self.MOCK_MR3]
        mock_proj = mock.Mock(name='blah')
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main'}
        ret = merge_conflicts_check.try_merging_all(mrs, mr_params)
        self.assertEqual(ret, [])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook.MergeMR.check_for_replicants')
    @mock.patch('webhook.kgit.branch_mergeable')
    def test_check_pending_conflicts(self, mock_mergeable, mock_replicants):
        mock_mmr = mock.Mock()
        mock_mmr.project_remote = 'blah'
        mock_mmr.target_branch = 'main'
        mock_mmr.worktree_dir = 'blah_main'
        mock_mmr.iid = '66'
        mock_mmr.check_for_replicants = mock_replicants
        mock_replicants.return_value = False
        conflict_mrs = [self.MOCK_MR2]
        mock_mergeable.return_value = True
        ret = merge_conflicts_check.check_pending_conflicts(mock_mmr, conflict_mrs)
        self.assertFalse(ret)
        mock_mergeable.return_value = False
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            ret = merge_conflicts_check.check_pending_conflicts(mock_mmr, conflict_mrs)
            self.assertIn("MR 67 can't be merged by itself, skipping conflict check",
                          logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook.MergeMR.update_mr')
    @mock.patch('webhook.utils.merge_conflicts_check.Projects')
    @mock.patch('webhook.utils.merge_conflicts_check.check_pending_conflicts')
    @mock.patch('webhook.mergehook.MergeMR.check_for_merge_conflicts')
    @mock.patch('webhook.mergehook.MergeMR.new')
    @mock.patch('webhook.session.SessionRunner.update_webhook_comment', mock.Mock())
    def test_find_direct_conflicts(self, mock_mmr_new, mock_check, mock_checkp,
                                   mock_projects, mock_update):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(testing=False)
        conflict_mrs = [self.MOCK_MR3]
        mock_mmr = mock.Mock()
        mock_mmr.check_for_merge_conflicts = mock_check
        mock_mmr.update_mr = mock_update
        mock_mmr_new.return_value = mock_mmr
        mock_mmr.merge_label = "Merge::OK"
        mock_mr = mock.Mock()
        mock_proj = mock.Mock(name='blah')
        mock_proj.mergerequests.get.return_value = mock_mr
        mock_projects = mock.Mock()
        mock_projects.return_value.get_project_by_id.return_value = mock.Mock(confidential=False)
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main',
                     'merge_dir': '/src/foo-bar-blah_main',
                     'proj_tb': 'blah_main', 'namespace': 'blah'}
        # MR has conflicts with target branch
        mock_check.return_value = True
        merge_conflicts_check.find_direct_conflicts(mock_session, conflict_mrs, mr_params)
        mock_update.assert_called_once()
        mock_checkp.assert_not_called()
        self.assertEqual(mock_mmr.merge_label, defs.MERGE_CONFLICT_LABEL)
        # MR has conflicts with other MRs
        mock_check.return_value = False
        mock_checkp.return_value = True
        mock_update.reset_mock()
        merge_conflicts_check.find_direct_conflicts(mock_session, conflict_mrs, mr_params)
        mock_update.assert_called_once()
        self.assertEqual(mock_mmr.merge_label, defs.MERGE_WARNING_LABEL)

    @mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS':
                                    'tests/assets/rh_projects_private.yaml'})
    @mock.patch('webhook.kgit.hard_reset', mock.Mock())
    @mock.patch('webhook.mergehook.MergeMR.new')
    def test_check_for_duplicated_backports(self, mock_merge_mr):
        mock_session = mock.Mock()
        mock_session.args.rhkernel_src = '/path/to/kernel-ark'
        mock_session.args.kerneloscope_server_url = 'http://kerneloscope.example.com'
        mock_proj = mock.Mock(name='rhel-9-sandbox', id='56792')
        namespace = 'redhat/rhel/src/kernel/rhel-9-sandbox'
        mr_params = {'gl_project': mock_proj,
                     'merge_dir': '/src/rhel-9-sandbox-main-merge',
                     'proj_tb': 'rhel-9-sandbox_main'}
        mr_id = 23
        mr_url = f'https://gitlab.com/{namespace}/-/merge_requests/{mr_id}'
        mreq = {'iid': mr_id, 'webUrl': mr_url}
        mock_mmr = mock.Mock()
        mock_mmr.worktree_dir = ''
        mock_merge_mr.return_value = mock_mmr
        mock_mmr.check_for_existing_backports.return_value = True
        retval = merge_conflicts_check.check_for_duplicated_backports(mock_session,
                                                                      [mreq], mr_params)
        self.assertEqual(retval, [mreq])

    @mock.patch('webhook.kgit.clean_up_temp_merge_branch')
    def test_clean_up_temp_merge_branches(self, mock_cleanup):
        args = mock.Mock(rhkernel_src='/src/kernel')
        merge_dirs = {'foo/bar/blah_main': '/src/foo-bar-blah_main'}
        merge_conflicts_check.clean_up_temp_merge_branches(args, merge_dirs)
        mock_cleanup.assert_called_with('/src/kernel', 'foo-bar-blah_main',
                                        '/src/foo-bar-blah_main')

    @mock.patch('webhook.utils.merge_conflicts_check.add_label_to_merge_request')
    def test_set_merge_ok_labels(self, mock_add_label):
        args = mock.Mock(testing=False)
        mock_mrs = [self.MOCK_MR2]
        mock_mr = mock.Mock()
        mock_proj = mock.Mock(name='blah')
        mock_proj.mergerequests.get.return_value = mock_mr
        mr_params = {'gl_project': mock_proj, 'target_branch': 'main', 'namespace': 'blah'}
        merge_conflicts_check.set_merge_ok_labels(args, mock_mrs, mr_params)
        mock_add_label.assert_called_with(mock_proj, self.MOCK_MR2['iid'],
                                          [f'Merge::{defs.READY_SUFFIX}'])
        mock_add_label.reset_mock()
        merge_conflicts_check.set_merge_ok_labels(args, [], mr_params)
        mock_add_label.assert_not_called()

    @mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': '/etc/pki/certs/whatever.ca'})
    def test_parser_args(self):
        with mock.patch("sys.argv", ["_get_parser_args", "-r", "/src/linux"]):
            args = merge_conflicts_check._get_parser_args()
            self.assertFalse(args.testing)
            self.assertEqual(args.rhkernel_src, '/src/linux')
            self.assertEqual(args.sentry_ca_certs, '/etc/pki/certs/whatever.ca')

    def test_main_no_src(self):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(rhkernel_src=None)
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='DEBUG') as logs:
            merge_conflicts_check.main(mock_session)
            self.assertIn("No path to RH Kernel source git found, aborting!", logs.output[-1])

    @mock.patch('webhook.kgit.fetch_remote', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.get_open_mrs')
    @mock.patch('webhook.utils.merge_conflicts_check.GitlabGraph')
    def test_main_no_open_mrs(self, mock_glgraph, mock_mrs):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(rhkernel_src='/src/linux',
                                      projects=['cki-project/kernel-ark'])
        mock_mrs.return_value = {}
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='INFO') as logs:
            merge_conflicts_check.main(mock_session)
            self.assertIn("Fetching from git remotes to ensure we have the latest data needed",
                          logs.output[-3])
            self.assertIn("Finding open MRs for projects: ['cki-project/kernel-ark']",
                          logs.output[-2])
            self.assertIn("No open MRs to process.", logs.output[-1])

    @mock.patch('webhook.kgit.fetch_remote', mock.Mock())
    @mock.patch('webhook.kgit.branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.prep_branch_for_merges',
                mock.Mock(return_value='foo/bar/blah'))
    @mock.patch('webhook.utils.merge_conflicts_check.try_merging_all',
                mock.Mock(return_value=True))
    @mock.patch('webhook.utils.merge_conflicts_check.find_direct_conflicts', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.set_merge_ok_labels', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.clean_up_temp_merge_branches', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.filter_out_conflicting', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.check_for_duplicated_backports', mock.Mock())
    @mock.patch('webhook.utils.merge_conflicts_check.get_instance')
    @mock.patch('webhook.utils.merge_conflicts_check.get_open_mrs')
    @mock.patch('webhook.utils.merge_conflicts_check.GitlabGraph')
    def test_main_open_mrs(self, mock_glgraph, mock_mrs, mock_get_instance):
        mock_session = mock.Mock()
        mock_session.args = mock.Mock(rhkernel_src='/src/linux',
                                      projects=['foo/bar/blah'])
        mock_mrs.return_value = self.MOCK_MR_LIST
        mock_instance = mock.Mock()
        mock_instance.projects.git.return_value = mock.Mock()
        mock_get_instance.return_value = mock_instance
        with self.assertLogs('cki.webhook.utils.merge_conflicts_check', level='INFO') as logs:
            merge_conflicts_check.main(mock_session)
            self.assertIn("Finding open MRs for projects: ['foo/bar/blah']",
                          logs.output[-1])
