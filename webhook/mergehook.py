"""Ensure a merge request is mergeable."""
from dataclasses import dataclass
from dataclasses import field
import datetime
from functools import cached_property
from os import environ
import re
import subprocess
import sys
from textwrap import dedent
import typing

from cki_lib import logger
from cki_lib import misc

from . import common
from . import defs
from . import fragments
from . import kgit
from .base_mr_mixins import BaseMR
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import GitRepoMixin
from .base_mr_mixins import KerneloscopeMixin
from .cdlib import extract_ucid
from .session import SessionRunner

if typing.TYPE_CHECKING:
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.mergehook')


def find_mr_fileset(mreq):
    """Find the given MR's file list."""
    fileset = {file['path'] for file in mreq['files']}
    LOGGER.debug("Found MR %s files (%d): %s", mreq['iid'], len(fileset), fileset)
    return fileset


def find_orig_mr_fileset(mr_list, base_mr_id):
    """Find the original MR's file list."""
    fileset = set()
    for mreq in mr_list:
        if mreq['iid'] == str(base_mr_id):
            fileset = find_mr_fileset(mreq)

    return fileset


def build_mr_conflict_string(mr_entry):
    """Build MR conflict report string."""
    mr_id = int(mr_entry['iid'])
    author = mr_entry['author']['username']
    title = mr_entry['title']

    return f"MR !{mr_id} from @{author} (`{title}`) conflicts with this MR.  \n"


def event_days_old(event: typing.Union['GitlabMREvent', 'GitlabNoteEvent']) -> int:
    """Return how old the given event is in days, or 0 if unknown."""
    # If the event doesn't have a timestamp then look for the last time the MR was updated.
    if not (event_timestamp := event.timestamp):
        if updated_at := event.merge_request.get('updated_at'):
            with misc.only_log_exceptions():
                event_timestamp = datetime.datetime.strptime(updated_at, '%Y-%m-%d %H:%M:%S UTC')
                event_timestamp = event_timestamp.replace(tzinfo=datetime.UTC)
    # If for some reason we don't have a timestamp then we should just assume it is not old.
    if not event_timestamp:
        return 0
    event_age = datetime.datetime.now(tz=event_timestamp.tzinfo) - event_timestamp
    return event_age.days


@dataclass
class CommitData:
    """Per-MR-commit data storage class."""

    rhelcid: str = ''
    files: set = field(default_factory=set)
    patch_id: str = ''


@dataclass(repr=False)
class MergeMR(KerneloscopeMixin, GitRepoMixin, CommitsMixin, BaseMR):
    """Represent the merge request the mergehook is running against."""

    confidential: bool = False  # Is this firing for a -private namespace project?
    merge_label: str = defs.Label(f'Merge::{defs.READY_SUFFIX}')
    conflicts: list[str] = field(default_factory=list, init=False)
    reverts: set = field(default_factory=set, init=False)
    overrides: set = field(default_factory=set, init=False)
    # key is upstream commit hash
    commits_data: dict[str, CommitData] = field(default_factory=dict, init=False)

    # Get all open MRs for target branch in namespace
    MERGE_MR_QUERY_BASE = dedent("""
    query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
      project(fullPath: $namespace) {
        id @include(if: $first)
        mergeRequests(state: opened, after: $after, targetBranches: $branches, not: {labels: ["NoCommits", "Oversize"]}) {
          pageInfo {hasNextPage endCursor}
          nodes {
            iid
            author {username}
            title
            targetBranch
            project {fullPath}
            ...MrLabels
            ...MrFiles
          }
        }
      }
    }
    """)  # noqa: E501

    MERGE_MR_QUERY = MERGE_MR_QUERY_BASE + fragments.MR_LABELS + fragments.MR_FILES

    @cached_property
    def project_remote(self) -> str:
        """The name of the project's git remote."""
        return self.gl_project.name if not self.confidential else f'{self.gl_project.name}-private'

    def do_git_setup(self) -> None:
        """Set up git worktree to work in."""
        self.fetch_remote(self.project_remote)
        self.prep_worktree(self.project_remote)

    def fetch_mr_list(self):
        """Fetch the list of relevant MRs and their files via graphql query."""
        query_params = {'namespace': self.namespace,
                        'branches': self.target_branch}
        result = self.session.graphql.check_query_results(
            self.session.graphql.client.query(self.MERGE_MR_QUERY, query_params,
                                              paged_key="project/mergeRequests"),
            check_keys={'project'})

        unfiltered_mr_list = misc.get_nested_key(result, 'project/mergeRequests/nodes')
        orig_mr_fileset = find_orig_mr_fileset(unfiltered_mr_list, self.iid)

        if not orig_mr_fileset:
            LOGGER.warning("Originating MR %s appears to have no files", self.iid)
            return []

        filtered_mr_list = [mreq for mreq in unfiltered_mr_list
                            if mreq['iid'] != str(self.iid) and
                            orig_mr_fileset.intersection(find_mr_fileset(mreq))]

        LOGGER.debug("Returning mr list of %s", filtered_mr_list)
        return filtered_mr_list

    def build_commit_data(self, commit) -> CommitData:
        """Build out a CommitData object for a given commit."""
        files = set(commit.stats.files.keys())
        patch_id = kgit.patch_id(self.worktree_dir, commit.hexsha)
        return CommitData(rhelcid=commit.hexsha, files=files, patch_id=patch_id)

    def get_commits_data(self, mr_branch, target_branch, skip_deps=False) -> dict[str, CommitData]:
        """Return commits_data with per-commit upstream commit IDs and patch-id."""
        merge_base = self.repo.merge_base(mr_branch, target_branch)[0].hexsha
        commits = list(self.repo.iter_commits(f'{merge_base}..{mr_branch}'))
        commits_data = {}
        for commit in commits:
            if skip_deps and commit.hexsha == self.first_dep_sha:
                break
            ucids = extract_ucid(commit.message)
            commit_data = self.build_commit_data(commit)
            for ucid in ucids:
                commits_data[ucid] = commit_data

        LOGGER.debug("Commits Data for %s (%s): %s", mr_branch, len(commits_data), commits_data)
        return commits_data

    def find_ignore_duplicates(self):
        """Add intentionally overridden duplicate backport hashes to self.overrides set."""
        overrides_pattern = r'^\s*(?P<pfx>Ignore-duplicate:)[\s]+(?P<hash>[a-f0-9]{8,40})'
        overrides_re = re.compile(overrides_pattern, re.IGNORECASE)
        for line in self.description.text.split('\n'):
            gitref = overrides_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                self.overrides.add(githash)
        LOGGER.debug('List of %d Ignore-duplicate-backports: %s',
                     len(self.overrides), self.overrides)

    def check_for_replicants(self, second_iid) -> bool:
        """Check if this MR has any backports of the same commit(s) as us."""
        LOGGER.info("Looking for dupes in %s", second_iid)
        second_branch = f"refs/remotes/{self.project_remote}/merge-requests/{second_iid}"
        target_branch = f"{self.project_remote}/{self.target_branch}"
        second_data = self.get_commits_data(second_branch, target_branch)

        duplicates = set()
        for sha, commit_data in self.commits_data.items():
            if sha in second_data:
                if commit_data.rhelcid == second_data[sha].rhelcid:
                    LOGGER.debug("Skipping %s, exact same git object present in both MR %s and %s",
                                 commit_data.rhelcid, self.iid, second_iid)
                    continue

                LOGGER.warning("Patch %s (MR %s) and patch %s (MR %s) backport the same "
                               "upstream commit (%s)", commit_data.rhelcid,
                               self.iid, second_data[sha].rhelcid, second_iid, sha)
                if files_overlap := commit_data.files & second_data[sha].files:
                    LOGGER.debug("Patches touch the same files: %s", files_overlap)
                else:
                    continue

                conflict_str = (f"MR !{self.iid} patch {commit_data.rhelcid} and MR !{second_iid} "
                                f"patch {second_data[sha].rhelcid} are both backporting upstream "
                                f"commit {sha}, and both are touching {files_overlap}, ")
                if commit_data.patch_id == second_data[sha].patch_id:
                    LOGGER.debug("Commit %s has the same patch-id as %s",
                                 commit_data.rhelcid, second_data[sha].rhelcid)
                    conflict_str += "with the same git patch-id value (identical backports).\n\n"
                else:
                    LOGGER.debug("Commit %s has a different patch-id than already committed %s",
                                 commit_data.rhelcid, second_data[sha].rhelcid)
                    conflict_str += "but with different git patch-id values. THIS IS BAD.\n\n"
                self.conflicts.append(conflict_str)
                duplicates.add(sha)

        if duplicates:
            LOGGER.info("Flagging duplicate upstream backports for %s", duplicates)

        return bool(duplicates)

    def check_for_other_merge_conflicts(self) -> bool:
        """Check to see if there are any other pending MRs that conflict with this MR."""
        LOGGER.info("Checking other pending %s MRs for conflicts with MR %d",
                    self.target_branch, self.iid)

        has_conflicts = False
        mr_list = self.fetch_mr_list()
        # Save the base mr-is-merged as a branch, so we can quickly reset after each OTHER mr
        save_branch = f"{self.work_branch}-save"
        kgit.branch_copy(self.worktree_dir, save_branch)

        target_branch = f"{self.project_remote}/{self.target_branch}"
        for mr_entry in mr_list:
            mr_id = int(mr_entry['iid'])
            if mr_id == self.iid:
                continue
            # First, make sure this MR can actually be merged by itself
            if {'title': defs.MERGE_CONFLICT_LABEL} in mr_entry['labels']['nodes']:
                LOGGER.info("MR %d has conflicts label on it, skipping it", mr_id)
                continue
            if not kgit.branch_mergeable(self.worktree_dir, target_branch,
                                         f'{self.project_remote}/merge-requests/{mr_id}'):
                LOGGER.info("MR %d can't be merged by itself, skipping conflict check", mr_id)
                continue

            if mr_id in self.description.depends_mrs:
                LOGGER.info("MR %d is a stated dependency of MR %d", mr_id, self.iid)

            # Now, try to merge it on top of this MR, so we can see if it has any conflicts
            kgit.hard_reset(self.worktree_dir, save_branch)
            has_conflicts = self.check_for_replicants(mr_entry['iid'])
            try:
                kgit.merge(self.worktree_dir, f'{self.project_remote}/merge-requests/{mr_id}')
            except subprocess.CalledProcessError as err:
                if mr_id in self.description.depends_mrs:
                    self.conflicts.append("CONFLICT: your dependency MR has different hashes from "
                                          "the ones included in your MR. Please rebase this MR on "
                                          f"top of the current version of !{mr_id}.")
                self.conflicts.append(build_mr_conflict_string(mr_entry))
                self.conflicts.append(err.output)
                has_conflicts = True

        kgit.branch_delete(self.worktree_dir, save_branch)
        return has_conflicts

    def find_reverts(self) -> None:
        """Find any commits that are reverting an earlier commit."""
        reverts_pattern = r'^This reverts commit (?P<hash>[a-f0-9]{8,40}).'
        reverts_re = re.compile(reverts_pattern, re.IGNORECASE)
        for sha, commit in self.commits.items():
            if sha == self.first_dep_sha:
                break
            if not commit.title.startswith("Revert"):
                continue
            for line in commit.description.text.splitlines():
                if gitref := reverts_re.match(line):
                    self.reverts.add(gitref.group('hash'))

    def check_for_merge_conflicts(self) -> bool:
        """Check to see if this MR can be cleanly merged to the target branch."""
        target_branch = f"{self.project_remote}/{self.target_branch}"
        LOGGER.info("Checking for merge conflicts when merging %d into %s.",
                    self.iid, target_branch)
        kgit.create_worktree_timestamp(self.worktree_dir)
        # Now try to merge the MR to the worktree
        try:
            kgit.merge(self.worktree_dir, f'{self.project_remote}/merge-requests/{self.iid}')
        except subprocess.CalledProcessError as err:
            kgit.hard_reset(self.worktree_dir, target_branch)
            self.conflicts.append(f"MR !{self.iid} cannot be merged to {target_branch}  \n")
            self.conflicts.append(err.output)
            return True
        return False

    def check_for_existing_backports(self) -> bool:
        """Check to see if any commits in this MR were already backported."""
        duplicates = set()
        mr_branch = f"refs/remotes/{self.project_remote}/merge-requests/{self.iid}"
        target_branch = f"{self.project_remote}/{self.target_branch}"
        self.commits_data = self.get_commits_data(mr_branch, target_branch, skip_deps=True)
        self.find_reverts()
        self.find_ignore_duplicates()

        for sha, commit_data in self.commits_data.items():
            if not self.session.args.no_kerneloscope and self.branch.use_kerneloscope:
                possible_dupes = self.find_already_backported(sha)
            else:
                possible_dupes = self.find_already_backported_orig(target_branch, sha)
            if possible_dupes and 'revert' in possible_dupes[-1]['subject'].lower():
                for possible_dupe in possible_dupes:
                    self.reverts.add(possible_dupe['commit'])
            for possible_dupe in possible_dupes:
                LOGGER.info("Found possible dupe: %s", possible_dupe)
                pd_sha = possible_dupe['commit']
                commit = self.repo.commit(pd_sha)
                if not set(commit.stats.files.keys()) & commit_data.files:
                    LOGGER.info("Same upstream commit %s referenced in %s, but no overlapping "
                                "files with %s", sha, pd_sha, commit_data.rhelcid)
                    continue
                if commit.hexsha in self.reverts:
                    LOGGER.warning("Commit %s found as reverted, not flagging dupe.", commit.hexsha)
                    continue
                if commit.hexsha in self.overrides:
                    LOGGER.warning("Duplicate commit %s in MR overrides, not flagging dupe.",
                                   commit.hexsha)
                    continue
                patch_id = kgit.patch_id(self.worktree_dir, commit.hexsha)
                rhelcid = commit_data.rhelcid
                duplicates.add(sha)
                if patch_id != commit_data.patch_id:
                    LOGGER.debug("Commit %s has a different patch-id than already committed %s",
                                 rhelcid, commit.hexsha)
                    self.conflicts.append(f"MR commit {rhelcid} was already backported in "
                                          f"{target_branch} with a different patch-id "
                                          f"than submission, as {commit.hexsha}  \n")
                else:
                    LOGGER.debug("Commit %s has the same patch-id as %s",
                                 commit_data.rhelcid, commit.hexsha)
                    self.conflicts.append(f"MR commit {rhelcid} was already backported in "
                                          f"{target_branch} with the same patch-id "
                                          f"as submission, as {commit.hexsha}  \n")

        LOGGER.info("Duplicate backports already in tree: %s", duplicates)
        return bool(duplicates)

    def do_merge_checks(self) -> None:
        """Perform merge checks against other pending merge requests."""
        if self.check_for_existing_backports():
            self.merge_label = defs.MERGE_CONFLICT_LABEL
        elif self.check_for_merge_conflicts():
            # if the MR can't be merged to target branch, try to force gitlab's status recheck
            # before we return our git-based conflicts status
            if self.session.is_production_or_staging:
                self.gl_project.mergerequests.list(iids=[self.iid], with_merge_status_recheck=True)
            self.merge_label = defs.MERGE_CONFLICT_LABEL
        elif self.check_for_other_merge_conflicts():
            self.merge_label = defs.MERGE_WARNING_LABEL

    def format_conflict_info(self):
        """Format the additional merge conflict output for comment output."""
        if self.merge_label == defs.MERGE_CONFLICT_LABEL:
            comment = 'This merge request cannot be merged to its target branch\n\n'
        elif self.merge_label == defs.MERGE_WARNING_LABEL:
            comment = ('There are other pending MRs that conflict with this one. '
                       'Please discuss possible merge solutions with the author(s) of the other '
                       'merge request(s).\n\n')
        else:
            comment = 'This MR can be merged cleanly to its target branch.'

        for entry in self.conflicts:
            if entry.startswith('MR'):
                comment += f'\n{entry}\n'
            else:
                for line in entry.split('\n'):
                    if line.startswith('CONFLICT'):
                        comment += f'* {line}  \n'

        return comment

    def update_mr(self, only_if_changed: bool = False) -> None:
        """Update webhook comment and labels following evaluation."""
        self.add_labels([self.merge_label])
        comments = self.session.get_comment_cache()
        comments.update_comment(self.url, self.merge_label, self.format_conflict_info(),
                                only_if_changed=only_if_changed)


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    if event_days := event_days_old(event):
        LOGGER.warning('Event is %s day(s) old, ignoring.', event_days)
        return

    merge_mr = MergeMR.new(session, event.mr_url,
                           confidential=event.rh_project.confidential,
                           linux_src=session.args.rhkernel_src,
                           kerneloscope_server_url=session.args.kerneloscope_server_url)
    merge_mr.do_git_setup()
    merge_mr.do_merge_checks()
    merge_mr.cleanup_worktree()
    merge_mr.update_mr()


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('MERGEHOOK')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory containing RH kernel projects git tree')
    parser.add_argument('--kerneloscope-server-url',
                        **common.get_argparse_environ_opts('KERNELOSCOPE_SERVER_URL'),
                        help='Kerneloscope server URL')
    parser.add_argument('--no-kerneloscope', action='store_true',
                        default=environ.get('NO_KERNELOSCOPE', ''),
                        help='Override use of kerneloscope, fall back to git log methods')
    args = parser.parse_args(args)
    if not args.rhkernel_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    session = SessionRunner.new('mergehook', args=args, handlers=HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
