"""A basic MR class using Graphql."""
import dataclasses
from functools import cached_property
from textwrap import dedent
from time import sleep
import typing

from cki_lib import misc
from cki_lib.logger import get_logger
from gitlab import GitlabHttpError
import prometheus_client as prometheus

from webhook import common
from webhook import defs
from webhook import fragments
from webhook.description import MRDescription
from webhook.users import UserCache

if typing.TYPE_CHECKING:
    from gitlab.v4.objects.merge_requests import ProjectMergeRequest
    from gitlab.v4.objects.projects import Project

    from webhook.rh_metadata import Branch as RH_Branch
    from webhook.rh_metadata import Project as RH_Project
    from webhook.session import BaseSession
    from webhook.users import User

LOGGER = get_logger('cki.webhook.base_mr')

METRIC_KWF_QUERY_RETRIES = prometheus.Counter(
    'kwf_query_retries',
    'Number of times query data was found on an MR only after retrying'
)

# https://docs.gitlab.com/ee/api/merge_requests.html#get-merge-request-dependencies
BLOCKS_ENDPOINT = '/projects/{project_id}/merge_requests/{mr_id}/blocks'


def check_mr_query_results(results: dict) -> bool:
    """Check the query results are useful."""
    if not results['project']['mr']:
        LOGGER.warning('Merge request does not exist?')
        return False
    return True


BASE_MR_FIELDS = """
fragment BaseMR on MergeRequest {
  approved
  author {
    ...GlUser
  }
  commitCount
  description
  global_id: id
  ...MrLabelsPaged
  state
  draft
  ...MrFiles @skip(if: $skip_files)
  preparedAt
  sourceBranch
  targetBranch
  title
  headPipeline {
    id
  }
  project {
    id
  }
}
"""

BASE_MR_FIELDS += fragments.GL_USER + fragments.MR_LABELS_PAGED + fragments.MR_FILES


@dataclasses.dataclass(repr=False)
class BaseMR:
    # pylint: disable=too-many-public-methods
    """Hold basic MR data and provide basic functions."""

    MR_QUERY_BASE = dedent("""
    query mrDetails($mr_id: String!, $namespace: ID!, $skip_files: Boolean = false, $after: String = "") {
      project(fullPath: $namespace) {
        mr: mergeRequest(iid: $mr_id) {
          ...BaseMR
        }
      }
    }
    """)  # noqa: E501

    MR_QUERY = MR_QUERY_BASE + BASE_MR_FIELDS

    session: 'BaseSession'
    url: defs.GitlabURL
    raw_data: dict
    _labels: list[defs.Label] = dataclasses.field(default_factory=list, init=False)

    def __repr__(self) -> str:
        """Describe yourself."""
        is_dependency = getattr(self, 'is_dependency', '???')

        # If the CommitsMixin is here then use its count since it is excluding merge commits.
        if hasattr(self, 'commits'):
            commits = len(getattr(self, 'commits'))
        else:
            commits = self.commit_count

        repr_str = f'MR {self.namespace}!{self.iid}'
        repr_str += f', approved: {self.approved}'
        repr_str += f', commits: {commits}'
        repr_str += f', state: {self.state.name.lower()}'
        repr_str += f', draft: {self.draft}'
        repr_str += f", dependency: {is_dependency}"
        repr_str += f", project: {self.project.name if self.project else 'None'}"
        repr_str += f", branch: {self.branch.name if self.branch else 'None'}"
        return f'<{repr_str}>'

    def __post_init__(self) -> None:
        """Set the labels from the input data."""
        self._labels = make_labels(self.session, self.raw_data, self.namespace, self.iid)
        LOGGER.info('Created %s', self)
        if not self.raw_data:
            LOGGER.warning('No MR data! raw_data is: %s', self.raw_data)

    @classmethod
    def new(
        cls,
        session: 'BaseSession',
        mr_url: str,
        *args,
        skip_files: bool = False,
        **kwargs
    ) -> 'BaseMR':
        """Do a query and use its results to construct a new BaseMR."""
        mr_url = defs.GitlabURL(mr_url)
        mr_data = cls.do_mr_query(session, mr_url, skip_files)
        return cls(session, mr_url, mr_data, *args, **kwargs)

    def update(self, skip_files: typing.Optional[bool] = None) -> None:
        """Run the MR_QUERY and replace self.raw_data."""
        if skip_files is None:
            skip_files = 'files' not in misc.get_nested_key(self.raw_data, 'project/mr', {})
        # Think about how this may impact cached_properties.
        self.raw_data = self.do_mr_query(self.session, defs.GitlabURL(self.url), skip_files)
        self._labels = make_labels(self.session, self.raw_data, self.namespace, self.iid)

    @classmethod
    def do_mr_query(
        cls,
        session: 'BaseSession',
        mr_url: defs.GitlabURL,
        skip_files: bool
    ) -> dict:
        """Run the MR_QUERY and return the results dict."""
        query_params = {
            'namespace': mr_url.namespace,
            'mr_id': str(mr_url.id),
            'skip_files': skip_files
        }
        query_result = cls.query(session, cls.MR_QUERY, query_params, operation_name='mrDetails')
        return misc.get_nested_key(query_result, 'project/mr', {})

    @property
    def approved(self) -> bool:
        """Return True if the MR is approved, otherwise False."""
        return self.raw_data.get('approved', False)

    @property
    def author(self) -> typing.Union['User', None]:
        """Return the MR author as a User."""
        author_dict = self.raw_data.get('author')
        return self.user_cache.get(author_dict) if author_dict else None

    @cached_property
    def author_can_merge(self) -> bool:
        """Return True if the MR author has permission to merge this MR, otherwise False."""
        return self.session.graphql.user_can_merge(
            self.namespace,
            self.global_id,
            self.author.username
        )

    @property
    def is_build_mr(self) -> bool:
        """Return True if this is a z-stream build MR in Draft state, otherwise False."""
        return (defs.ZSTREAM_BUILD_LABEL in self.labels and self.draft and
                self.branch and self.branch.zstream)

    @property
    def is_autobot_mr(self) -> bool:
        """Return True if this MR was created by our auto-backport infrastructure."""
        return self.author.username == defs.CKI_BACKPORT_BOT_ACCOUNT

    @property
    def commit_count(self) -> int:
        """Return the number of commits the MR has if known, or 0."""
        return int(self.raw_data.get('commitCount', 0))

    @cached_property
    def description(self) -> MRDescription:
        """Return the MRDescription object for this MR."""
        description_text = self.raw_data.get('description', '')
        return MRDescription(description_text, self.namespace, self.session.graphql)

    @property
    def draft(self) -> bool:
        """Return True if this MR is a draft, otherwise False."""
        return self.raw_data.get('draft', False)

    @property
    def global_id(self) -> defs.GitlabGID | str:
        """Return the GitlabGID of the MR if known, or ''."""
        if global_id := self.raw_data.get('global_id'):
            return defs.GitlabGID(global_id)

        return ''

    @property
    def head_pipeline_id(self) -> int:
        """Return the head pipeline ID if known, or 0."""
        head_pipeline = self.raw_data.get('headPipeline')
        return int(head_pipeline['id'].rsplit('/', 1)[-1]) if head_pipeline else 0

    @property
    def labels(self) -> list[defs.Label]:
        """Return the list of MR labels."""
        return self._labels

    @property
    def ready_for_qa(self) -> bool:
        """Return True if the MR has the readyForQA label on it."""
        return defs.READY_FOR_QA_LABEL in self.labels

    @property
    def ready_for_merge(self) -> bool:
        """Return True if the MR has the readyForMerge label on it."""
        return defs.READY_FOR_MERGE_LABEL in self.labels

    @property
    def merge_warning(self) -> bool:
        """Return True if the MR has merge warnings."""
        return defs.MERGE_WARNING_LABEL in self.labels

    @property
    def merge_conflict(self) -> bool:
        """Return True if the MR has merge conflicts."""
        return defs.MERGE_CONFLICT_LABEL in self.labels

    @property
    def state(self) -> defs.MrState:
        """Return the MrState."""
        return defs.MrState.from_str(self.raw_data.get('state', ''))

    @property
    def source_branch(self) -> str:
        """Return the source branch name of the MR if known, otherwise ''."""
        return self.raw_data.get('sourceBranch', '')

    @property
    def target_branch(self) -> str:
        """Return the target branch name of the MR if known, otherwise ''."""
        return self.raw_data.get('targetBranch', '')

    @property
    def title(self) -> str:
        """Return the title of the MR if known, otherwise ''."""
        return self.raw_data.get('title', '')

    @property
    def project_id(self) -> int:
        """Return the MR's project ID if known, or 0."""
        project_id = misc.get_nested_key(self.raw_data, 'project/id')
        return int(project_id.split('/')[-1]) if project_id else 0

    @property
    def branch(self) -> typing.Union['RH_Branch', None]:
        """Return the matching Branch object, or None."""
        return self.project.get_branch_by_name(self.target_branch) if self.project else None

    @property
    def files(self) -> list[str]:
        """Return the list of files affected by the MR."""
        return [path['path'] for path in self.raw_data.get('files', [])]

    @cached_property
    def gl_mr(self) -> 'ProjectMergeRequest':
        """Return a gl_mergerequest object."""
        return self.gl_project.mergerequests.get(self.url.id)

    @cached_property
    def gl_project(self) -> 'Project':
        """Return a gl_project object."""
        return self.session.get_gl_project(self.url.namespace)

    @property
    def has_depends(self) -> bool:
        """Return True if the MR description lists any dependencies, otherwise False."""
        return bool(self.description.depends or self.description.depends_mrs)

    @cached_property
    def iid(self) -> int:
        """Return the MR internal ID as an int."""
        return self.url.id

    @cached_property
    def namespace(self) -> str:
        """Return the MR namespace."""
        return self.url.namespace

    @cached_property
    def project(self) -> typing.Union['RH_Project', None]:
        """Return the matching rh_metadata.Project object, or None."""
        return self.session.rh_projects.get_project_by_namespace(self.url.namespace)

    @cached_property
    def user_cache(self) -> UserCache:
        """Return a UserCache object."""
        return self.session.get_user_cache(self.url.namespace)

    @cached_property
    def manage_jiras(self) -> bool:
        """Return if the project for this MR uses Jira."""
        return self.project.manage_jiras if self.project else False

    def add_labels(self, to_add: list[str], remove_scoped: bool = False) -> list[str]:
        """Use common code to add the list of labels to the MR."""
        fresh_labels = common.add_label_to_merge_request(self.gl_project, self.iid, to_add,
                                                         remove_scoped=remove_scoped)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_add).issubset(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_add: {to_add}'))
        return fresh_labels

    def remove_labels(self, to_remove: list[str]) -> list[str]:
        """Use common code to remove the list of labels from the MR."""
        fresh_labels = common.remove_labels_from_merge_request(self.gl_project, self.iid, to_remove)
        self.labels[:] = [defs.Label(label_str) for label_str in fresh_labels]
        if not set(to_remove).isdisjoint(self.labels):
            raise RuntimeError(('The MR does not have the expected labels.'
                                f' Current: {self.labels}, to_remove: {to_remove}'))
        return fresh_labels

    def labels_with_prefix(self, prefix: str) -> list[defs.Label]:
        """Return the list of scoped MR labels with the given prefix."""
        return [label for label in self.labels if label.scoped and label.prefix == prefix]

    def labels_with_scope(self, scope: defs.MrScope) -> list[defs.Label]:
        """Return the list of scoped MR labels with the given MrScope."""
        return [label for label in self.labels if label.scoped and label.scope is scope]

    def _blocking_data(self) -> list[dict[str, typing.Any]]:
        """Return the raw list of 'blocks' data for this MR."""
        gl_instance = self.session.get_gl_instance(self.url.host)
        blocks_endpoint = BLOCKS_ENDPOINT.format(project_id=self.project_id, mr_id=self.iid)
        return gl_instance.http_list(blocks_endpoint)

    def add_blocking_mr(self, mr_gid: defs.GitlabGID) -> dict[str, typing.Any] | None:
        """Add the MR global ID as a 'block' (dependency) & return the API result or None."""
        mr_gid = mr_gid.id
        gl_instance = self.session.get_gl_instance(self.url.host)
        blocks_endpoint = BLOCKS_ENDPOINT.format(project_id=self.project_id, mr_id=self.iid)

        # In the non-live case we just return an empty dict?
        if not self.session.is_production_or_staging:
            return {}

        try:
            result = gl_instance.http_post(f'{blocks_endpoint}?blocking_merge_request_id={mr_gid}')
        except GitlabHttpError as err:
            LOGGER.warning('Failed to add a block on %s for MR with GID %s: %s',
                           self.url, mr_gid, err)
            return None

        return result

    def remove_blocking_id(self, block_id: int) -> None:
        """Remove the 'block' (dependency) by its ID."""
        gl_instance = self.session.get_gl_instance(self.url.host)
        blocks_endpoint = BLOCKS_ENDPOINT.format(project_id=self.project_id, mr_id=self.iid)

        if not self.session.is_production_or_staging:
            return

        try:
            gl_instance.http_delete(f'{blocks_endpoint}/{block_id}')
        except GitlabHttpError as err:
            LOGGER.warning('Failed to remove block on %s with ID %s: %s', self.url, block_id, err)

    @staticmethod
    def query(
        session: 'BaseSession',
        query_string: str,
        query_params: typing.Optional[dict] = None,
        paged_key: typing.Optional[str] = None,
        check_function: typing.Optional[typing.Callable] = check_mr_query_results,
        query_retries: int = 2,
        query_retry_delay: int = 5,
        operation_name: typing.Optional[str] = None
    ) -> typing.Union[dict, None]:
        # pylint: disable=too-many-arguments,too-many-positional-arguments
        """Run the given query and process it with the given function."""
        query_params = query_params or {}
        for retry in range(query_retries):
            results = session.graphql.client.query(
                query_string,
                variable_values=query_params,
                paged_key=paged_key,
                operation_name=operation_name
            )
            if not results:
                LOGGER.warning('No results for query with params: %s\n%s', query_params,
                               query_string)
                return None
            if check_function and check_function(results):
                if retry:
                    LOGGER.warning('API returned data on the second try 🙄.')
                    METRIC_KWF_QUERY_RETRIES.inc()
                break
            if retry:
                LOGGER.info('Still no data after retry 🤷.')
                break
            LOGGER.info('No data found, trying again in %s seconds.', query_retry_delay)
            sleep(query_retry_delay)
        return results


def make_labels(
    session: 'BaseSession',
    mr_data: dict,
    namespace: str,
    mr_id: int
) -> list[defs.Label]:
    """Return the list of Label objects in the raw_data."""
    raw_labels = mr_data.get('labels', {})
    return session.graphql.get_all_mr_labels(namespace, mr_id, raw_labels) if \
        raw_labels else []
