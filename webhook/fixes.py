"""Query MRs for upstream commit IDs to compare against submitted patches."""
from dataclasses import dataclass
from dataclasses import field
from os import environ
import re
import sys
import typing

from cki_lib import logger

from webhook import common
from webhook import defs
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.base_mr_mixins import DependsMixin
from webhook.base_mr_mixins import GitRepoMixin
from webhook.base_mr_mixins import KerneloscopeMixin
from webhook.cdlib import extract_ucid
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.fixes')


@dataclass
class RHCommit:
    """Per-MR-commit data class for storing comparison data."""

    commit: set
    ucids: set = field(default_factory=set, init=False)
    fixes: str = ""


@dataclass
class Fixes:
    """Per-MR list of fixes and their categorization/status."""

    possible: dict
    in_mr: set
    in_omitted: set
    in_tree: set


@dataclass(repr=False)
class FixesMR(KerneloscopeMixin, GitRepoMixin, DependsMixin, CommitsMixin, BaseMR):
    """Represent the MR and possible Fixes."""

    ucids: set = field(default_factory=set, init=False)
    omitted: set = field(default_factory=set, init=False)

    def __post_init__(self):
        """Get the commits and basic MR data."""
        # Load the commits before the basic MR properties (super.__post_init__) because on a large
        # MR loading the commits could take some time so this way the MR state and labels are most
        # up to date once we start doing things with the data.
        self.commits  # pylint: disable=pointless-statement
        super().__post_init__()
        self.rhcommits = {}
        self.ucids = set()
        self.omitted = set()
        self.fixes = Fixes(possible={}, in_mr=set(), in_omitted=set(), in_tree=set())

    def find_intentionally_omitted_fixes(self, description):
        """Add intentionally omitted fixes hashes to self.omitted set."""
        fixes_pattern = r'^\s*(?P<pfx>Omitted-fix:)[\s]+(?P<hash>[a-f0-9]{8,40})'
        fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
        for line in description.split('\n'):
            gitref = fixes_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                self.omitted.add(githash)
        LOGGER.debug('List of %d Omitted-fixes: %s', len(self.omitted), self.omitted)

    def map_fixes_to_commits(self):
        """Map potential missing fixes to their corresponding MR commits."""
        for ucid, fixes in self.fixes.possible.items():
            for sha, rhcommit in self.rhcommits.items():
                for fix in fixes:
                    if ucid in rhcommit.ucids:
                        rhcommit.fixes += self.repo.git.log("-1", "--pretty=short", fix)
                        rhcommit.fixes += f'\n    RH-Fixes: {sha[:12]} '
                        rhcommit.fixes += f'("{rhcommit.commit.title}")\n\n'

    def fix_already_in_tree(self, fix, fixee):
        """Check to see if the fix is already included in the tree."""
        if not self.session.args.no_kerneloscope and self.branch.use_kerneloscope:
            if committed_as := self.find_already_backported(fix):
                return committed_as[0]['commit']
            return None

        our_commits = self.gl_project.search('commits', fix)
        for commit in our_commits:
            if fix in commit['message'] and f'Fixes: {fixee[:8]}' in commit['message']:
                return commit['id']

        return None

    def filter_fixes(self, mapped_fixes):
        """Filter potential fixes against included commits and Omitted-fix refs."""
        for fixee, fixes in mapped_fixes.items():
            LOGGER.info("Found potential missing upstream fixes for upstream commit %s", fixee)
            per_commit_fixes = []
            for fix in fixes:
                propose = True
                for ucid in self.ucids:
                    # fix is typically a short hash, ucid is always 40 char hash
                    if ucid.startswith(fix):
                        self.fixes.in_mr.add(ucid)
                        LOGGER.info("Found fix %s for %s in ucids", fix, fixee)
                        propose = False
                        continue
                for omit in self.omitted:
                    # fix is typically a short hash, omit can be 8-40 chars, match first 8 chars
                    if fix[:8] == omit[:8]:
                        self.fixes.in_omitted.add(omit)
                        LOGGER.info("Found fix %s for %s in omitted", fix, fixee)
                        propose = False
                        continue
                if propose:
                    if commit := self.fix_already_in_tree(fix, fixee):
                        self.fixes.in_tree.add(f'{fix} as {commit}')
                        LOGGER.info("Found fix %s already in tree as %s", fix, commit)
                        continue
                    LOGGER.info("Found fix %s for %s not included or referenced in MR", fix, fixee)
                    per_commit_fixes.append(fix)
            if per_commit_fixes:
                self.fixes.possible[fixee] = per_commit_fixes

    def find_missing_fixes_orig(self, ucid):
        """Return a list of commits that are fixes for the specified commit."""
        fixes_pattern = r'^(?P<hash>[a-f0-9]{8,40})'
        fixes_re = re.compile(fixes_pattern, re.IGNORECASE)

        fixes = []
        short_hash = ucid[:12]
        fixeslog = self.repo.git.log("--oneline", f'--grep=Fixes: {short_hash}', f'{short_hash}..')

        for line in fixeslog.splitlines():
            gitref = fixes_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                fixes.append(githash)

        return fixes

    def find_potential_missing_fixes(self):
        """Store potential missing upstream fixes hashes in fixes_mr.fixes mapping."""
        mapped_fixes = {}

        has_fixes = []
        for ucid in self.ucids:
            # Make sure the commit exists in our tree
            try:
                _ = self.repo.commit(ucid)
            except ValueError:
                LOGGER.info("Commit ID %s not found in upstream, skipping checks", ucid)
                continue

            if not self.session.args.no_kerneloscope and self.branch.use_kerneloscope:
                fixes = self.find_missing_fixes(ucid)
            else:
                fixes = self.find_missing_fixes_orig(ucid)
            has_fixes += fixes
            if fixes:
                mapped_fixes[ucid] = fixes

        LOGGER.info("Upstream commits with Fixes: %s", has_fixes)
        self.filter_fixes(mapped_fixes)
        if self.fixes.possible:
            LOGGER.info('Map of potential missing Fixes: %s', self.fixes)
        self.map_fixes_to_commits()

    def find_upstream_commit_ids(self):
        """Check upstream commit IDs."""
        for sha, commit in self.commits_without_depends.items():
            self.rhcommits[sha] = RHCommit(commit)

        for sha, rhcommit in self.rhcommits.items():
            try:
                found_refs = extract_ucid(rhcommit.commit.description.text)
            # pylint: disable=broad-except
            except Exception:
                found_refs = []

            for ref in found_refs:
                self.ucids.add(ref)
            rhcommit.ucids = found_refs

        LOGGER.debug('List of %d ucids: %s', len(self.ucids), self.ucids)

    def check_for_fixes(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        LOGGER.debug("Checking for possible missing upstream fixes in MR %s", self.iid)
        self.find_upstream_commit_ids()
        self.find_intentionally_omitted_fixes(self.description.text)

        # do NOT run validation if the commit count is over 2000
        if len(self.commits) <= defs.MAX_COMMITS_PER_MR:
            self.find_potential_missing_fixes()

    def build_fixes_comment(self):
        """Build a comment for possible missing Fixes commits."""
        fixes_msg = ''
        if self.fixes.possible:
            fixes_msg += "\nPossible missing Fixes detected upstream:  \n"
            fixes_msg += "```\n"
            for _, rhcommit in self.rhcommits.items():
                fixes_msg += rhcommit.fixes
            fixes_msg += "```\n"
            fixes_msg += ("These can be resolved by either backporting and adding the referenced "
                          "commit(s) to your MR, or by adding Omitted-fix: lines to your MR "
                          "description, where appropriate.\n")
        else:
            fixes_msg += f"No missing upstream fixes for MR {self.iid} found at this time.\n"

        if self.fixes.in_mr:
            fixes_msg += '\nUpstream Fixes included in MR:\n'
            fixes_msg += '\n'.join(f' - {fix}\n' for fix in self.fixes.in_mr)

        if self.fixes.in_omitted:
            fixes_msg += '\nUpstream Fixes omitted via MR description:\n'
            fixes_msg += '\n'.join(f' - {fix}\n' for fix in self.fixes.in_omitted)

        if self.fixes.in_tree:
            fixes_msg += '\nUpstream Fixes already in tree:\n'
            fixes_msg += '\n'.join(f' - {fix}\n' for fix in self.fixes.in_tree)

        return fixes_msg

    def report_results(self, only_if_changed: bool = False):
        """Report the results of our examination."""
        report = self.build_fixes_comment()

        # append quick label "Fixes" with scope "OK" or "Missing"
        fixes_label = 'Fixes::OK'
        if len(self.fixes.possible) > 0:
            fixes_label = f'Fixes::{defs.MISSING_SUFFIX}'

        self.add_labels([fixes_label])
        comments = self.session.get_comment_cache()
        comments.update_comment(self.url, fixes_label, report, only_if_changed=only_if_changed)


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    fixes_mr = FixesMR.new(session, event.mr_url, linux_src=session.args.linux_src,
                           kerneloscope_server_url=session.args.kerneloscope_server_url)
    fixes_mr.check_for_fixes()
    fixes_mr.report_results()


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('FIXES')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    parser.add_argument('--kerneloscope-server-url',
                        **common.get_argparse_environ_opts('KERNELOSCOPE_SERVER_URL'),
                        help='Kerneloscope server URL')
    parser.add_argument('--no-kerneloscope', action='store_true',
                        default=environ.get('NO_KERNELOSCOPE', ''),
                        help='Override use of kerneloscope, fall back to git log methods')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    session = SessionRunner.new('fixes', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
