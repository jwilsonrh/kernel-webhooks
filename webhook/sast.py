"""Get SAST status and results for a given MR."""

from dataclasses import dataclass
from enum import Enum
from re import match
from re import sub
from sys import argv
import typing
from urllib.parse import quote_plus
from urllib.parse import urlencode

from cki_lib.logger import get_logger
from cki_lib.session import get_session

from webhook import common
from webhook import defs
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.session import SessionRunner

if typing.TYPE_CHECKING:
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = get_logger('cki.webhook.sast')
SAST_BASE_URL_ENV_VAR = "KERNEL_SAST_AUTOMATION_SERVER_REST_API"
SAST_GL_PROJECTS = "GL_PROJECTS"
REPORT_HEADER = '**Kernel SAST Scan Report**'
MAX_NUMBER_FINDINGS_TO_DISPLAY = 30


class UrlSubitem(Enum):
    """Simple enum class to control of REST API calls to our backend."""

    STATUS = "status"
    RESULTS = "results"


@dataclass(repr=False)
class MR(CommitsMixin, BaseMR):
    """Represent the MR."""


def check_update_comment(session, comment_text, gl_mr, gl_project, new_label):
    """Update comment only current is different than the new one."""
    uwc_field = getattr(session, "update_webhook_comment", None)
    if not callable(uwc_field):
        LOGGER.error("session.update_webhook_comment is not callable")
        return
    if not isinstance(comment_text, str) or comment_text.strip() == "":
        comment_text = "<br/><br/>No findings available"
    update_comment = True
    report_text = REPORT_HEADER + f' ~"SAST::{new_label}"\n\n' + comment_text
    for discussion in gl_mr.discussions.list(iterator=True):
        note = discussion.attributes['notes'][0]
        if note['author']['username'] == gl_project.manager.gitlab.user.username and \
           REPORT_HEADER in note['body']:
            # this must be "startswith", because in session we have:
            # body = text + self.comment.gitlab_footer('updated')
            # so our text is only the beginning of the full comment
            if note['body'].startswith(report_text):
                update_comment = False
    if update_comment:
        session.update_webhook_comment(gl_mr, report_text,
                                       bot_name=gl_project.manager.gitlab.user.username,
                                       identifier=REPORT_HEADER)


def update_mr(session, gl_project, mr_id, new_label, comment_text):
    """Update the MR with a note of the results and possibly set the label scope."""
    if gl_project is None or mr_id is None or session is None or not match(r'^\d+$', f"{mr_id}"):
        LOGGER.error("Invalid parameters passed to update_mr: %s, %s, %s", gl_project, mr_id,
                     session)
        return

    mergerequests_field = getattr(gl_project, "mergerequests", None)
    if mergerequests_field is None:
        LOGGER.error("gl_project doesn't have mergerequests attribute")
        return
    get_field = getattr(gl_project.mergerequests, "get", None)
    if not callable(get_field):
        LOGGER.error("gl_project.mergerequests.get is not callable")
        return

    gl_mr = gl_project.mergerequests.get(mr_id)

    labels_field = getattr(gl_mr, "labels", None)
    if not isinstance(labels_field, list):
        LOGGER.error("gl_mr.labels is not a list")
        return

    if len(gl_mr.labels) > 0:
        for lbl in gl_mr.labels:
            if not isinstance(lbl, str):
                LOGGER.error("gl_mr.labels may only be strings")
                return

    current_label = next((lbl.rsplit('::', 1)[-1] for lbl in gl_mr.labels if
                          lbl.startswith('SAST::')), None)

    LOGGER.debug("current label: '%s', new label: '%s'", current_label, new_label)

    if current_label != new_label and isinstance(new_label, str):
        LOGGER.debug("SAST webhook update_mr: setting new label: SAST::%s", new_label)
        common.add_label_to_merge_request(gl_project, mr_id, [f"SAST::{new_label}"])

    check_update_comment(session, comment_text, gl_mr, gl_project, new_label)


def get_sast_item(url, subitem: UrlSubitem, mr_id):
    """Get the SAST scan information from ProdSec's servers."""
    if not match(r'^\d+$', f"{mr_id}"):
        LOGGER.error("The supplied MR id is not entirely numeric: %s", f"{mr_id}")
        return None
    if not match(r'https?:\/\/(?:w{1,3}\.)?[^\s.]+(?:\.[a-z]+)*(?::\d+)?((?:\/\w+)|(?:-\w+))*\/?' +
                 r'(?![^<]*(?:<\/\w+>|\/?>))', f"{url}"):
        LOGGER.error("The supplied base URL is not valid: %s", f"{url}")
        return None
    base_url = f"{url}/{subitem.value}/mr/{mr_id}"
    LOGGER.debug("SAST webhook get_sast_%s: getting from URL: %s", subitem.value, base_url)
    headers = {"accept": "application/json"}
    req = None
    try:
        s = get_session("kwf/webhook.sast", timeout=10)
        s.headers.update(headers)
        req = s.get(base_url)
    except Exception as e:
        LOGGER.error("SAST webhook get_sast_%s: exception during get: %s: %s", subitem.value,
                     base_url, e)
        return None
    if req is None:
        LOGGER.error("SAST webhook get_sast_%s: got None response: %s", subitem.value, base_url)
        return None
    LOGGER.debug("SAST webhook get_sast_%s: got response code %d", subitem.value, req.status_code)
    try:
        req.raise_for_status()
        LOGGER.debug("SAST webhook get_sast_%s: returning JSON", subitem.value)
        return req.json()
    except Exception as e:
        LOGGER.error("Unable to get Kernel SAST %s for MR (%d): %s", subitem.value, mr_id, e)
    LOGGER.debug("SAST webhook get_sast_%s: returning None", subitem.value)
    return None


def findings_to_gitlab_html(findings_json, sast_mr):
    """Convert the JSON structure with SAST findings into Gitlab HTML comment.

    This is nice and easy for the maintainers to view, analyze and understand.
    """
    final_str = ""
    if findings_json is None or not isinstance(findings_json, dict) or \
            "findings" not in findings_json or not isinstance(findings_json["findings"], list):
        LOGGER.error("Invalid findings_json passed: %s", findings_json)
        return final_str
    if sast_mr is None or not hasattr(sast_mr, "iid") or sast_mr.iid is None or \
            not isinstance(sast_mr.iid, int) or sast_mr.iid < 1:
        LOGGER.error("Invalid sast_mr object passed: %s", sast_mr)
        return final_str
    if sast_mr is None or not hasattr(sast_mr, "url") or sast_mr.url is None or \
            not isinstance(sast_mr.url, str):
        LOGGER.error("Invalid sast_mr object passed: %s", sast_mr)
        return final_str
    if not hasattr(sast_mr, "project") or sast_mr.project is None or \
            not hasattr(sast_mr.project, "name") or sast_mr.project.name is None or \
            not isinstance(sast_mr.project.name, str):
        LOGGER.error("Invalid sast_mr object passed: %s", sast_mr)
        return final_str
    for itm in findings_json["findings"][:MAX_NUMBER_FINDINGS_TO_DISPLAY]:
        try:
            itm = itm.replace("kernel-999/", "").replace("kernel-999", "")
            subi = itm.split("\n", 2)
            subi0 = sub(r'CWE-(\d+)', r'<a href="https://cwe.mitre.org/data/definitions/\1' +
                        r'.html" target="_blank">CWE-\1</a>', subi[0])
            fpth = subi[1].split(":", 2)
            encoded_jira_fp = urlencode({"summary": "False positive finding in kernel MR",
                                         "description": "Please, add the following OSH error " +
                                         f"from [{sast_mr.project.name} MR {sast_mr.iid}|" +
                                         f"{sast_mr.url}] to the list of known false " +
                                         f"positives:\n\n{subi[0]} {fpth[0]}:{fpth[1]}\n\n"
                                         "Reason: {TYPE_THE_REASON_FOR_FALSE_POSITIVE}"},
                                        quote_via=quote_plus)
            fp_url = "https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?" + \
                     "pid=12331126&issuetype=3&labels=RHEL&labels=SecArch&security=11697" + \
                     "&priority=10200&" + encoded_jira_fp
            encoded_jira_tp = urlencode({"summary": f"Kernel {sast_mr.project.name} MR " +
                                         f"{sast_mr.iid}: {subi[0]} {fpth[0]}:{fpth[1]}"},
                                        quote_via=quote_plus)
            tp_url = "https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?" + \
                     "pid=12332745&issuetype=1&labels=SAST&labels=Kernel&security=11694" + \
                     "&priority=10200&" + encoded_jira_tp
            final_str = final_str + f"""\n<details>
    <summary>
        {subi0} {fpth[0]}:{fpth[1]}&nbsp;&nbsp;&nbsp;<a href="{fp_url}">&#10007;</a>
        &nbsp;&nbsp;&nbsp;<a href="{tp_url}">&#10003;</a>
    </summary>
    <pre>
{subi[0]}
{subi[1]}
{subi[2]}
    </pre>
</details>"""
        except Exception as e:
            LOGGER.error("Error parsing finding: %s", e)
    if final_str != "":
        final_str = "The following list contains findings from performing a Static Analysis" + \
                    " Security Testing (SAST) on this MR. For more details on what needs to " + \
                    "be done, please review [our FAQ]" + \
                    "(https://docs.google.com/document/d/" + \
                    "1giNDNRzDh2C8EO5zdNaadACYGlWzhRgUkpMJujNFCZc). " + \
                    "This activity is currently non-gating, but the RHEL " + \
                    "Security Architects encourage you to perform it prior to " + \
                    "merging.\n\n" + final_str
    LOGGER.debug(final_str)
    return final_str


def process_mr(session: SessionRunner, sast_mr: MR) -> bool:
    """Process given MR object."""
    sast_status_label = "pending"
    report_text = None
    sast_status = get_sast_item(session.args.sast_rest_api_url, UrlSubitem.STATUS,
                                sast_mr.iid)
    LOGGER.debug("SAST webhook process_mr: sast_status = %s", sast_status)
    if sast_status is None or not isinstance(sast_status, dict) or \
            "status" not in sast_status or "amount" not in sast_status:
        LOGGER.error("Kernel SAST status returned for MR %d is wrong: %s",
                     sast_mr.iid, sast_status)
        update_mr(session, sast_mr.gl_project, sast_mr.iid, sast_status_label, report_text)
        return False
    sast_status_label = sast_status['status']
    LOGGER.debug("SAST webhook process_mr: new label = %s", sast_status_label)
    if sast_status["status"] == "finished":
        LOGGER.debug("SAST webhook process_mr: we have %d finding(s)",
                     sast_status['amount'])
        if sast_status["amount"] > 0:
            sast_findings = get_sast_item(session.args.sast_rest_api_url, UrlSubitem.RESULTS,
                                          sast_mr.iid)
            LOGGER.debug("SAST webhook process_mr: sast_findings = %s", sast_findings)
            if sast_findings is None or "findings" not in sast_findings:
                LOGGER.error("Kernel SAST findings returned for MR %d are wrong: %s",
                             sast_mr.iid, sast_findings)
                update_mr(session, sast_mr.gl_project, sast_mr.iid, "failed", report_text)
                return False
            if len(sast_findings["findings"]) == 1 and sast_findings["findings"][0] == "\n":
                sast_status['amount'] = 0
            report_text = findings_to_gitlab_html(sast_findings, sast_mr)
        if sast_status['amount'] > 0:
            sast_status_label = f"{sast_status_label}_w_findings"
        else:
            sast_status_label = f"{sast_status_label}_no_findings"
    update_mr(session, sast_mr.gl_project, sast_mr.iid, sast_status_label, report_text)
    return True


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    sast_mr = MR.new(session, event.mr_url)
    if process_mr(session, sast_mr):
        LOGGER.info("Finished event processing for %s, %s", event.kind.name, event.mr_url)


def nudger(session: SessionRunner) -> None:
    """Find all open MRs, nudge the webhook."""
    for gl_proj_path in session.args.gl_projects:
        LOGGER.debug("Nudger: gl_proj_path == %s", gl_proj_path)
        rh_project = session.rh_projects.get_project_by_namespace(gl_proj_path)
        if rh_project is None:
            raise ValueError(f"{gl_proj_path} is not a project from rh_metadata.yaml")
        if (rh_project.sandbox and session.is_production) or \
           (not rh_project.sandbox and session.is_staging):
            LOGGER.info("Skipping %s project %s in %s environment.",
                        'sandbox' if rh_project.sandbox else 'non-sandbox',
                        gl_proj_path, session.environment)
            continue
        gl_project = session.get_gl_project(gl_proj_path)
        gl_mrs = gl_project.mergerequests.list(all=True, state="opened")
        LOGGER.debug("Found gl_mrs == %r", gl_mrs)
        for gl_mr in gl_mrs:
            sast_mr = MR.new(session, gl_mr.web_url)
            process_mr(session, sast_mr)
        LOGGER.info("Finished on project: %s", gl_proj_path)
    LOGGER.info("Nudger finished")


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('SAST')
    parser.add_argument('--sast-rest-api-url',
                        **common.get_argparse_environ_opts(SAST_BASE_URL_ENV_VAR),
                        help='The URL to the SAST REST API automation server')
    gl_proj = common.get_argparse_environ_opts(SAST_GL_PROJECTS, is_list=True)
    if "required" in gl_proj:
        gl_proj["required"] = False
        gl_proj["default"] = None
    parser.add_argument('--gl-projects', **gl_proj,
                        help='The path to the Gitlab projects to nudge, e.g. ' +
                             '"redhat/rhel/src/kernel/rhel-9"')
    args = parser.parse_args(args)
    session = SessionRunner.new('sast', args=args, handlers=HANDLERS)
    LOGGER.debug("args.gl_projects type %s == %r", type(args.gl_projects), args.gl_projects)
    if args.gl_projects is not None and isinstance(args.gl_projects, list):
        nudger(session)
        return
    session.run()


if __name__ == "__main__":
    main(argv[1:])
