# pylint: disable=too-many-lines

"""Process all commits in the merge request for upstream references, diffs, etc."""
from dataclasses import dataclass
from dataclasses import field
import enum
from functools import cached_property
import os
import re
from subprocess import CalledProcessError
import sys
import typing

from cki_lib import logger
from gitlab.exceptions import GitlabGetError

from . import cdlib
from . import common
from . import defs
from . import kgit
from .base_mr import BaseMR
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import DependsMixin
from .base_mr_mixins import DiffsMixin
from .base_mr_mixins import GitRepoMixin
from .session import SessionRunner

if typing.TYPE_CHECKING:
    from .description import Commit
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.commit_compare')

COMMIT_COMPARE_LABEL_PREFIX = 'CommitRefs'
CONFIG_ARK_MISMATCH_LABEL = defs.Label('Config::ARK::Mismatch')
KABI_LABEL = defs.Label('KABI')

CONFIG_REPORT_HEADER = 'Upstream Config Differences'


def x86_filename_replacement(filename):
    """Map and replace x86 configs to match ARK."""
    filename = filename.replace("x86/x86_64", "x86")
    arkfilename = filename.replace("common", "rhel")
    commonfilename = filename.replace("rhel", "common")

    return commonfilename, arkfilename


def read_config_content(workdir, filename):
    """Read the content of a config file if it exists."""
    file_content = os.path.join(workdir, filename)

    if not os.path.exists(file_content):
        return None

    with open(file_content, 'r', encoding='ascii') as cr_fp:
        return cr_fp.read().rstrip()


def fetch_file_content(ark_worktree, commonfilename, arkfilename):
    """Fetch the content of a config file inside ARK worktree."""
    common_output = None
    ark_output = None

    common_output = read_config_content(ark_worktree, commonfilename)

    if not common_output:
        ark_output = read_config_content(ark_worktree, arkfilename)

    return common_output, ark_output


def compare_config_diffs(sha, output, common_output, ark_output):
    """Check if the current repository has the same configs of ARK repo."""
    LOGGER.debug("Content in common '%s' and in ark '%s'. The expected "
                 "output is '%s'.", common_output, ark_output, output)

    if not (common_output or ark_output):
        # If there is no CONFIG in ARK, downstream should disable (None) either.
        if not output:
            LOGGER.debug("Matched config change to common directory in ARK")
            return ''

        msg = (f'A CONFIG change in RHEL commit {sha} (`{output}`) does not match '
               'ARK, no matching common or rhel config file found.')
        LOGGER.debug(msg)
        return msg

    if output:
        # Considering the CONFIG exists and was changed, we need to compare
        # with common or ark dirs.
        if output == common_output:
            LOGGER.debug("Matched config change to common directory in ARK")
            return ''

        if output == ark_output:
            LOGGER.debug("Matched config change to ark directory in ARK")
            return ''

    # Any other combination is considered a mismatch.

    msg = (f'A CONFIG change in RHEL commit {sha} (`{output}`) does not match ARK '
           f'configs for future RHEL releases (redhat/configs/common: `{common_output}`, '
           f'redhat/configs/rhel: `{ark_output}`).')
    LOGGER.debug(msg)
    return msg


def get_config_change(path):
    """Extract the specific config change from a diff."""
    config_setting = ''
    changed_file = None

    if path['deletedFile']:
        changed_file = path['oldPath']

        config_setting = None
    elif path['renamedFile']:
        pass
    else:
        # File is new or it is a file change
        changed_file = path['newPath']

        config_setting = ''
        for line in path['diff'].split('\n'):
            if line.startswith('+') and 'CONFIG_' in line:
                config_setting += line[1:]

    if changed_file and not changed_file.startswith('redhat/configs/'):
        # Revert when it is not a CONFIG file.
        config_setting = ''
        changed_file = None

    return config_setting, changed_file


def find_kabi_hints(message, diff):
    """Check for hints this patch has kABI workarounds in it."""
    kabi_re = re.compile(r'kabi(?![a-zA-Z])', re.IGNORECASE)
    if kabi_re.findall(message):
        return True
    if kabi_re.findall(diff):
        return True
    for keyword in ("genksyms", "rh_reserved"):
        if keyword in message.lower():
            return True
        if keyword in diff.lower():
            return True
    return False


class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4
    RHELONLY = 5
    POSTED = 6
    CONFIG = 7
    UNRECOGNIZED = 8

    @property
    def description(self):
        """Return a formatted text description string."""
        match self.name:
            case 'NOUCID':
                text = "No UCID"
            case 'FULL':
                text = "100% match"
            case 'PARTIAL':
                text = "Partial"
            case 'DIFFS':
                text = "Diffs"
            case 'KABI':
                text = "kABI Diffs"
            case 'RHELONLY':
                text = "n/a"
            case 'POSTED':
                text = "n/a"
            case 'CONFIG':
                text = "Config"
            case 'UNRECOGNIZED':
                text = "Unrecognized UCID"

        return text

    @property
    def has_upstream_commit_hash(self):
        """Return true/false if the match type comes with an upstream commit hash."""
        match self.name:
            case 'NOUCID' | 'RHELONLY' | 'POSTED':
                return False
            case _:
                return True

    @property
    def footnote(self):
        """Return the footnote string associated with the match type."""
        match self.name:
            case 'NOUCID':
                text = ("No commit information found.  All commits are required to have a 'commit: "
                        "<upstream_commit_hash>' entry, or an 'Upstream Status: RHEL-only' entry "
                        "with an explanation why this change is not upstream. Alternatively, an "
                        "'Upstream Status: Posted' entry followed by a URL pointing to the posting "
                        "is also acceptable.  Please review this commit and appropriate metadata.  "
                        "Guidelines for these entries can be found in CommitRules,"
                        "https://red.ht/kwf_commit_rules.")
            case 'FULL':
                text = ""
            case 'PARTIAL':
                text = ("This commit is a partial backport of the referenced upstream commit ID, "
                        "and the portions backported match upstream 100%.")
            case 'DIFFS':
                text = ("This commit differs from the referenced upstream commit and should be "
                        "evaluated accordingly.")
            case 'KABI':
                text = ("This commit references a kABI work-around, and should be evaluated "
                        "accordingly.")
            case 'RHELONLY':
                text = ("This commit has Upstream Status as RHEL-only and has no corresponding "
                        "upstream commit.  The author of this MR should verify if this commit has "
                        "to be applied to [future versions of RHEL]"
                        "(https://gitlab.com/cki-project/kernel-ark). Reviewers should take "
                        "additional care when reviewing these commits.")
            case 'POSTED':
                text = ("This commit has Upstream Status as Posted, but we're not able to "
                        "auto-compare it.  Reviewers should take additional care when reviewing "
                        "these commits.")
            case 'CONFIG':
                text = "A CONFIG change was detected and does not match ARK."
            case 'UNRECOGNIZED':
                text = ("No upstream source commit found.  This commit references an upstream "
                        "commit ID, but its source of origin is not recognized.  Please verify "
                        "the upstream source tree.")
        return text


@dataclass
class RHCommit:
    """Per-MR-commit data class for storing comparison data."""

    commit: 'Commit'
    ucids: set = field(default_factory=set, init=False)
    match: int = Match.NOUCID
    notes: list = field(default_factory=list)


# The DependsMixin expects any additional fields to have kw_only set!
@dataclass(repr=False, kw_only=True)
class CommitsMR(GitRepoMixin, DependsMixin, CommitsMixin, DiffsMixin, BaseMR):
    # pylint: disable=too-many-public-methods,too-many-instance-attributes
    """Represent the MR, its revisions and approvals."""

    ark_branch: str = ''
    worktree_dir: str = ''
    rhcommits: list[RHCommit] = field(default_factory=list, init=False)
    # Comment table uses notes and nids to build footnotes.
    notes: list[str] = field(default_factory=list, init=False)
    nids: dict = field(default_factory=dict, init=False)
    # Updated by extract_upstream_commit_id() method.
    errors: str = ''
    # Updated by run_zstream_comparison_checks() method.
    zcompare_notes: str = ''
    # Updated by validate_commit_ids() method.
    commit_config_mismatches: str = ''

    @property
    def overall_commits_scope(self) -> defs.MrScope:
        """Return the overall MrScope of this CommitsMR."""
        if self.errors:
            return defs.MrScope.NEEDS_REVIEW
        if not self.approved:
            return defs.MrScope.MISSING

        return defs.MrScope.OK

    @property
    def approved(self) -> bool:
        """Return True if none of the RHCommits.match are Match.NOUCID."""
        # Look at the match status of all commits to determine CommitRefs label scope.
        return all(rhcommit.match is not Match.NOUCID for rhcommit in self.rhcommits)

    @property
    def needs_config_ark_label(self) -> bool:
        """Return True if any commits indicates the Config::ARK::Mismatch Label is needed."""
        return any(rhcommit.match is Match.CONFIG for rhcommit in self.rhcommits)

    @cached_property
    def dependencies_label(self) -> defs.Label:
        """Return the expected Dependencies:: label for this MR."""
        return cdlib.make_dependencies_label(self.gl_project, self.gl_mr)

    @property
    def needs_kabi_label(self) -> bool:
        """Return True if any commit indicates the KABI Label is needed, otherwise False."""
        return any(rhcommit.match is Match.KABI for rhcommit in self.rhcommits)

    @property
    def commit_compare_label(self) -> defs.Label:
        """Return the CommitRefs Label derived from the self.overall_commits_scope."""
        return self.overall_commits_scope.label(COMMIT_COMPARE_LABEL_PREFIX)

    @property
    def expected_labels(self) -> list[defs.Label]:
        """Return the list of Labels the MR should have."""
        labels = [self.commit_compare_label, self.dependencies_label]

        if self.needs_kabi_label:
            labels.append(KABI_LABEL)

        if self.needs_config_ark_label:
            labels.append(CONFIG_ARK_MISMATCH_LABEL)

        return labels

    def update_commit_match_data(self, rhcommit, matchtype) -> None:
        """Update given commit and footnotes mappings."""
        rhcommit.match = matchtype
        if rhcommit.match is Match.FULL:
            return
        if matchtype not in self.nids:
            self.notes += [matchtype.footnote]
            self.nids[matchtype] = str(len(self.notes))
        rhcommit.notes.append(self.nids[matchtype])

    def no_upstream_commit_data(self, rhcommit) -> None:
        """Handle a commit that has no upstream commit ID data."""
        if "RHELonly" in rhcommit.ucids:
            self.update_commit_match_data(rhcommit, Match.RHELONLY)
        elif "Posted" in rhcommit.ucids:
            self.update_commit_match_data(rhcommit, Match.POSTED)
        else:
            self.update_commit_match_data(rhcommit, Match.NOUCID)

    def extract_upstream_commit_id(self, commit) -> list[str]:
        """Extract the upstream commit ID from the given commit."""
        # pylint: disable=too-many-locals,too-many-branches,too-many-statements
        message_lines = commit.description.text.split('\n')
        errors = ""
        gitref_list = []
        have_exact_match = False
        # Pattern for 'git show <commit>' (and git log) based backports
        gl_pattern = r'^\s*(?P<pfx>commit)[\s]+(?P<hash>[a-f0-9]{8,40})$'
        gitlog_re = re.compile(gl_pattern)
        # Pattern for 'git cherry-pick -x <commit>' based backports
        cp_pattern = r'^\s*\((?P<pfx>cherry picked from commit)[\s]+(?P<hash>[a-f0-9]{8,40})\)$'
        gitcherrypick_re = re.compile(cp_pattern)
        # Look for 'RHEL-only' patches
        rhelonly_re = re.compile(r'^Upstream[ -][Ss]tatus: RHEL[ -]*[0-9.Zz]*[ -][Oo]nly')
        # Look for 'Posted' patches, posted upstream but not in a git tree yet
        posted_re = re.compile('^Upstream[ -][Ss]tatus: Posted')
        # Look for 'Posted' patches that are under embargo
        embargo_re = re.compile('^Upstream[ -][Ss]tatus: Embargo')
        # Look for 'Posted' patches, which are in a git tree we don't track (yet?)
        upstream_status_re = re.compile('^Upstream[ -][Ss]tatus: ')
        tree_prefixes = (
            'git://anongit.freedesktop.org/',
            'https://anongit.freedesktop.org/git/',
            'https://git.kernel.org/pub/scm/',
            'git://git.kernel.org/pub/scm/',
            'git://git.infradead.org/',
            'http://git.linux-nfs.org/',
            'git://linux-nfs.org/',
            'https://github.com/',
            'https://git.samba.org/'
        )
        linus_repos = (
            'https://github.com/torvalds/linux.git',
            'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
            'git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
            'https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git'
        )

        for line in message_lines:
            gitref = gitlog_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                hash_prefix = gitref.group('pfx')
                if line == f"{hash_prefix} {githash}" and len(githash) == 40:
                    if githash not in gitref_list:
                        gitref_list.append(githash)
                    have_exact_match = True
                else:
                    errors += f"\n* Incorrect format git commit line in {commit.short_sha}.  \n"
                if have_exact_match and errors:
                    errors = ""
                    continue
                if githash not in gitref_list:
                    gitref_list.append(githash)
                if line != f"{hash_prefix} {githash}":
                    errors += f"`Expected:` `{hash_prefix} {githash}`  \n"
                    errors += f"`Found   :` `{line}`  \n"
                if len(githash) < 40:
                    errors += f"Git hash only has {len(githash)} characters, expected 40.  \n"
                continue
            gitref = gitcherrypick_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                hash_prefix = gitref.group('pfx')
                if line == f"({hash_prefix} {githash})" and len(githash) == 40:
                    if githash not in gitref_list:
                        gitref_list.append(githash)
                    have_exact_match = True
                else:
                    errors += ("\n* Incorrect format git cherry-pick line in "
                               f"{commit.short_sha}.  \n")
                if have_exact_match and errors:
                    errors = ""
                    continue
                if githash not in gitref_list:
                    gitref_list.append(githash)
                if line != f"({hash_prefix} {githash})":
                    errors += f"`Expected:` `({hash_prefix} {githash})`  \n"
                    errors += f"`Found   :` `{line}`  \n"
                if len(githash) < 40:
                    errors += f"Git hash only has {len(githash)} characters, expected 40.  \n"
                continue
            if rhelonly_re.findall(line):
                gitref_list.append("RHELonly")
                continue
            if posted_re.findall(line):
                gitref_list.append("Posted")
                continue
            if embargo_re.findall(line):
                gitref_list.append("Posted")
                continue
            if upstream_status_re.findall(line):
                if any(prefix in line for prefix in tree_prefixes):
                    LOGGER.debug("Found tree_prefix in: %s", line)
                    if any(repo in line for repo in linus_repos):
                        LOGGER.warning("Patch contains Upstream Status: Linus ref w/o 40-char sha")
                        continue
                    gitref_list.append("Posted")
        if "RHELonly" in gitref_list:
            LOGGER.debug("This commit is marked as RHELonly, ignore all other found commit refs")
            gitref_list = ["RHELonly"]
            errors = ""
        # We return empty arrays if no commit IDs are found
        LOGGER.debug("Found upstream refs: %s", gitref_list)
        if len(gitref_list) > 5:
            LOGGER.info("Excessive number of commits found: %d, call it RHEL-only",
                        len(gitref_list))
            gitref_list = ["RHELonly"]
        if errors != "":
            LOGGER.warning("Upstream commit reference errors for submitted commit %s:\n%s",
                           commit.short_sha, errors)
        self.errors += errors
        return gitref_list

    def handle_stale_worktree(self) -> None:
        """Ensure we're not stomping on another run, and clean it up if it's old."""
        LOGGER.warning("Worktree already exists at %s!", self.worktree_dir)
        self.clean_up_temp_ark_branch()
        try:
            kgit.branch_delete(self.linux_src, f"{self.ark_branch}-save")
        except CalledProcessError:
            pass

    def prep_temp_ark_branch(self) -> None:
        """Set up the temporary branch/worktree to test upstream CONFIGs in it."""
        # kernel-ark remote is hard coded because this repo is the baseline.
        if self.ark_branch:
            # We already prepped the branch for another config check in this MR
            return

        target_branch = "kernel-ark/os-build"
        self.ark_branch = "kernel-ark-config-check"
        self.worktree_dir = (f"/{'/'.join(self.linux_src.strip('/').split('/')[:-1])}"
                             f"/{self.ark_branch}/")

        # Ensure we don't have a stale/failed checkout lingering
        if os.path.isdir(self.worktree_dir):
            self.handle_stale_worktree()

        # This is the lookaside cache we maintain for examining diffs between revisions of a
        # merge request, which we're going to create temporary worktrees off of
        LOGGER.info("Creating git worktree at %s with branch %s for upstream testing, please hold",
                    self.worktree_dir, self.ark_branch)
        kgit.worktree_add(self.linux_src, self.ark_branch, self.worktree_dir, target_branch)

    def evaluate_config(self, commit) -> None:
        """Compare and evaluate all the config files in diff with upstream."""
        mapped_config_changes = {}

        for path in commit.diff:
            config_setting, changed_file = get_config_change(path)

            if changed_file:
                mapped_config_changes[changed_file] = config_setting

        if not mapped_config_changes:
            return

        # Kernel ARK is hard coded because it is our baseline
        kgit.fetch_remote(self.linux_src, "kernel-ark")
        self.prep_temp_ark_branch()

        for filename, content in mapped_config_changes.items():
            commonfilename, arkfilename = x86_filename_replacement(filename)

            common_output, ark_output = fetch_file_content(self.worktree_dir,
                                                           commonfilename, arkfilename)

            mismatch = compare_config_diffs(commit.sha, content, common_output, ark_output)
            if mismatch:
                self.commit_config_mismatches += f'- {mismatch}  \n'

        LOGGER.debug(self.commit_config_mismatches)

    def clean_up_temp_ark_branch(self) -> None:
        """Clean up the git worktree and branches from the upstream worktree."""
        if not self.ark_branch:
            return
        kgit.worktree_remove(self.linux_src, self.worktree_dir)
        kgit.branch_delete(self.linux_src, self.ark_branch)
        LOGGER.debug("Removed worktree %s and deleted branch %s",
                     self.worktree_dir, self.ark_branch)

    def get_upstream_diff(self, ucid, filelist) -> tuple[str, str]:
        """Extract diff for upstream commit ID, optionally limiting to filelist."""
        try:
            commit = self.repo.commit(ucid)
        # pylint: disable=broad-except
        except Exception:
            LOGGER.debug("Commit ID %s not found in upstream", ucid)
            return None, ""

        diff = self.repo.git.diff(f"{commit}^", commit)
        # author_name = commit.author.name
        author_email = commit.author.email

        if len(filelist) == 0:
            return diff, author_email

        LOGGER.debug("Getting partial diff for %s", ucid)
        part_diff = cdlib.get_partial_diff(diff, filelist)

        return part_diff, author_email

    def validate_commit_ids(self) -> None:
        """Run validation checks on discovered upstream commit IDs."""
        # pylint: disable=too-many-branches,too-many-locals,too-many-statements
        self.commit_config_mismatches = ''

        for rhcommit in self.rhcommits:
            commit = rhcommit.commit
            no_ucid_invalid = True

            if any(ucid in ["-", "RHELonly", "Posted"] for ucid in rhcommit.ucids):
                self.no_upstream_commit_data(rhcommit)
                mydiff, _ = cdlib.get_diff_from_gql(commit.diff)
                if find_kabi_hints(commit.description.text, mydiff):
                    self.update_commit_match_data(rhcommit, Match.KABI)
                no_ucid_invalid = False

            if cdlib.is_rhconfig_commit(commit.diff) and \
               self.gl_project.id != defs.ARK_PROJECT_ID:
                self.evaluate_config(commit)
                if self.commit_config_mismatches:
                    self.update_commit_match_data(rhcommit, Match.CONFIG)
                    continue

            found_a_patch = False
            for ucid in rhcommit.ucids:
                if ucid in ("-", "RHELonly", "Posted"):
                    continue
                udiff, _ = self.get_upstream_diff(ucid, [])
                if udiff is None:
                    if no_ucid_invalid and not found_a_patch:
                        self.update_commit_match_data(rhcommit, Match.UNRECOGNIZED)
                    continue
                found_a_patch = True
                mydiff, filelist = cdlib.get_diff_from_gql(commit.diff)
                idiff = cdlib.compare_commits(udiff, mydiff)

                if len(idiff) == 0:
                    self.update_commit_match_data(rhcommit, Match.FULL)
                else:
                    diff = "```diff\n"
                    diff += f"--- Upstream {ucid}\n"
                    diff += f"+++ Backport {commit.sha}\n"
                    for entry in idiff[2:]:
                        diff += f"{entry}\n"
                    diff += "```\n"
                    LOGGER.debug("Upstream vs. backport patch interdiff:\n%s", diff)

                    udiff_partial, _ = self.get_upstream_diff(ucid, filelist)
                    idiff = cdlib.compare_commits(udiff_partial, mydiff)
                    if len(idiff) == 0:
                        self.update_commit_match_data(rhcommit, Match.PARTIAL)
                    else:
                        self.update_commit_match_data(rhcommit, Match.DIFFS)
                        if find_kabi_hints(commit.description.text, mydiff):
                            self.update_commit_match_data(rhcommit, Match.KABI)

        self.clean_up_temp_ark_branch()

    def config_differences_report(self) -> str:
        """Return the upstream config differences report."""
        if not self.commit_config_mismatches:
            return ''
        return f"@{self.author.username}, this warning does not imply that your CONFIG " \
               "change values are incorrect, but further review may be necessary. You " \
               "can resolve this thread if you feel your changes are correct for both " \
               "ARK and RHEL. If you see that you have changes to make (for example, " \
               "you may have to update your RHEL MR, or you may have to make further " \
               "changes in ARK) please make them and then resolve this thread.\n\n" \
               f"{self.commit_config_mismatches}\n"

    def run_zstream_comparison_checks(self) -> None:
        """Do some rudimentary validation of z-stream backports vs. their y-stream counterparts."""
        # pylint: disable=too-many-locals,too-many-branches,too-many-statements
        target = self.target_branch
        if target in ("main", "main-rt", "main-automotive", "os-build"):
            return

        if not self.branch or not self.branch.zstream:
            return

        ycommit_pattern = r'^Y-Commit: (?P<hash>[a-f0-9]{8,40})$'
        ycommit_re = re.compile(ycommit_pattern, re.IGNORECASE)
        yrefs = {}
        zcompare_notes = ""

        for rhcommit in self.rhcommits:
            cmsg_lines = rhcommit.commit.description.text.split('\n')
            ycommit_found = False
            for line in cmsg_lines:
                gitref = ycommit_re.match(line)
                if gitref:
                    githash = gitref.group('hash')
                    yrefs[githash] = rhcommit.commit
                    ycommit_found = True
                    break
            if not ycommit_found:
                LOGGER.debug("Z-Commit %s has no Y-Commit reference", rhcommit.commit.sha)
                zcompare_notes += f"Z-Commit {rhcommit.commit.sha} has no Y-Commit reference  \n"

        for ycid, zcid in yrefs.items():
            LOGGER.debug("Comparing Y-Commit: %s to Z-Commit: %s", ycid, zcid.sha)
            zdiff, _ = cdlib.get_diff_from_gql(zcid.diff)
            try:
                ycommit = self.gl_project.commits.get(ycid)
            except GitlabGetError:
                zcompare_notes += f"Y-Commit {ycid} listed for Z-Commit {zcid.sha} is invalid  \n"
                continue
            ydiff, _ = cdlib.get_submitted_diff(ycommit.diff(per_page=100, get_all=True))
            if len(ydiff) > len(zdiff)*3:
                zcompare_notes += (f"Y-Commit {ycommit.id} and Z-Commit {zcid.sha} do not "
                                   "match:  \n"
                                   f" - Y-stream commit is {len(ydiff)} lines, more than 3x the "
                                   "size of the z-stream patch, so no interdiff done. Please "
                                   "double-check this backport manually vs. Y-Stream "
                                   "counterpart.  \n\n")
                continue

            idiff = cdlib.compare_commits(ydiff, zdiff, strict_header=True)
            if idiff:
                LOGGER.debug("Y-Commit %s and Z-Commit %s do not match", ycid, zcid.sha)
                header = f"Y-Commit {ycommit.id} and Z-Commit {zcid.sha} do not match:  \n"
                diff = "```diff\n"
                diff += f"--- Y-Commit {ycommit.id}\n"
                diff += f"+++ Z-Commit {zcid.sha}\n"
                for entry in idiff[2:]:
                    diff += f"{entry}\n"
                diff += "```\n"
                zcompare_notes += header
                zcompare_notes += common.wrap_comment_table("", diff, "", "interdiff")
            else:
                LOGGER.debug("Y-Commit %s and Z-Commit %s match perfectly",
                             ycommit.id, zcid.sha)
                zcompare_notes += (f"Y-Commit {ycommit.id} and Z-Commit {zcid.sha} "
                                   "match 100%  \n")

        if zcompare_notes:
            if zcompare_notes.count('\n') > defs.TABLE_ENTRY_THRESHOLD:
                collapse_header = "<details><summary>Click to show/hide comparison</summary>\n\n"
                collapse_footer = "</details>\n\n"
                zcompare_notes = "\n\nZ-stream comparison report:  \n---\n" + collapse_header \
                                 + zcompare_notes + collapse_footer
            else:
                zcompare_notes = "\n\nZ-stream comparison report:  \n---\n" + zcompare_notes

        self.zcompare_notes = zcompare_notes

    def check_upstream_commit_ids(self) -> None:
        """Check for upstream commit ID references in submitted commits."""
        for commit in self.commits_with_diffs.values():
            rhcommit = RHCommit(commit)
            found_refs = self.extract_upstream_commit_id(commit)

            if not found_refs and cdlib.is_rhdocs_commit(commit.diff):
                found_refs = ["RHELonly"]

            rhcommit.ucids = found_refs
            self.rhcommits.append(rhcommit)

        self.errors = common.wrap_comment_table("", self.errors, "", "error details")

    def commit_id_report(self) -> str:
        """Print an upstream commit ID mapping report for gitlab."""
        # pylint: disable=too-many-locals,too-many-branches
        kerneloscope = "http://kerneloscope.usersys.redhat.com"
        show_full_match_note = False
        evaluated = len(self.rhcommits)
        pnum = 0
        report = (f'<br>\n\n**Upstream Commit ID Readiness Report**: '
                  f'~"{self.commit_compare_label}"\n\n')
        report += ("This report indicates how backported commits compare to the upstream source "
                   "commit.  Matching (or not matching) is not a guarantee of correctness.  KABI, "
                   "missing or un-backportable dependencies, or existing RHEL differences against "
                   "upstream may lead to a difference in commits. As always, care should be taken "
                   "in the review to ensure code correctness.\n")
        table_header = ("|P num   |Sub CID |UCIDs   |Match     |Notes   |\n"
                        "|:-------|:-------|:-------|:---------|:-------|\n")
        table_entries = ""
        for rhcommit in reversed(self.rhcommits):
            pnum += 1
            if rhcommit.match is Match.FULL:
                show_full_match_note = True
                continue
            table_entries += f"|{str(pnum)}|{rhcommit.commit.sha}|"
            if rhcommit.match.has_upstream_commit_hash:
                for ucid in rhcommit.ucids:
                    if ucid in ('-', '(...)', 'Posted') and len(rhcommit.ucids) > 1:
                        continue
                    table_entries += f"[{ucid[:8]}]({kerneloscope}/commit/{ucid})<br>"
            else:
                for ucid in rhcommit.ucids:
                    table_entries += f"{ucid[:8]}<br>"
            table_entries += f"|{rhcommit.match.description}|"
            table_entries += common.build_note_string(rhcommit.notes)

        footnotes = common.print_notes(self.notes)
        report += common.wrap_comment_table(table_header, table_entries, footnotes, "table")
        report += f"\n\nTotal number of commits analyzed: **{evaluated}**<br>"
        if self.commit_count > evaluated:
            report += f"* Skipped dependency commits: **{self.commit_count - evaluated}**<br>"
        if table_entries and show_full_match_note:
            report += "* Patches that match upstream 100% not shown in table<br>"
        if self.approved:
            if table_entries:
                report += "* Please note and evaluate differences from upstream in the table.  \n"
            report += "\nMerge Request **passes** commit ID validation, references all present.  \n"
        else:
            report += "\nThis Merge Request contains commits that are missing upstream commit ID " \
                    "references. Please review the table. \n" \
                    " \n" \
                    "To request re-evalution after resolving any issues with the commits in the " \
                    "merge request, add a comment to this MR with only the text:  " \
                    "request-commit-id-evaluation \n"

        return report

    def do_all_checks(self) -> None:
        """Do all the commit validation checks, figure out expected labels, etc."""
        LOGGER.info("Running upstream commit ID validation on MR %s/!%s",
                    self.namespace, self.iid)
        LOGGER.debug("Merge request description:\n%s", self.description.text)
        self.check_upstream_commit_ids()
        self.validate_commit_ids()
        self.run_zstream_comparison_checks()

    def update_mr(self) -> None:
        """Update the MR with the results of our examination."""
        current_labels = self.labels
        LOGGER.info('Current labels: %s', current_labels)

        # Add any expected labels to the MR which are not already set.
        LOGGER.info('Expected commit-compare labels: %s', self.expected_labels)
        to_add = [lbl for lbl in self.expected_labels if lbl not in current_labels]
        if to_add:
            LOGGER.info('Adding labels: %s', to_add)
            self.add_labels(to_add, remove_scoped=True)
        else:
            LOGGER.info('No labels to add.')

        # Remove the KABI and/or Configs::Ark::Mismatch label if they are no longer expected.
        to_remove: list[defs.Label] = [
            label for label in (KABI_LABEL, CONFIG_ARK_MISMATCH_LABEL) if
            label not in self.expected_labels and
            label in current_labels
        ]
        if to_remove:
            LOGGER.info('Removing unneeded labels: %s', to_remove)
            self.remove_labels(to_remove)
        else:
            LOGGER.info('No unneeded labels to remove.')

        # Update the config mismatches note, if needed.
        if config_report := self.config_differences_report():
            LOGGER.info('Reporting config differences.')
            report_header = f'**{CONFIG_REPORT_HEADER}**: ~"{CONFIG_ARK_MISMATCH_LABEL}"\n\n'
            self.session.update_webhook_comment(self.gl_mr, report_header + config_report,
                                                bot_name=self.session.gl_user.username,
                                                identifier=CONFIG_REPORT_HEADER)
        else:
            LOGGER.info('No config differences to report.')

        # Generate the full report and update the MR comment with it.
        LOGGER.info('Reporting commit ID readiness.')
        report_header = (f'**{self.session.webhook.comment_header}**: '
                         f'~"{self.commit_compare_label}"\n\n')
        report = report_header + self.commit_id_report()
        report += self.errors + self.zcompare_notes

        self.session.update_webhook_comment(self.gl_mr, report,
                                            bot_name=self.session.gl_user.username,
                                            identifier="Upstream Commit ID Readiness")


def process_merge_request(
    session: SessionRunner,
    mr_url: defs.GitlabURL
) -> None:
    """Process the given merge request and update it."""
    commits_mr = CommitsMR.new(
        session,
        mr_url,
        linux_src=session.args.linux_src
    )

    commits_mr.do_all_checks()
    commits_mr.update_mr()


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)

    process_merge_request(session, event.mr_url)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('COMMIT_COMPARE')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    session = SessionRunner.new('commit_compare', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
