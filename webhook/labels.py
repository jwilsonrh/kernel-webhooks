"""Gitlab Labels module."""
import dataclasses
import re
import typing

from cki_lib.logger import get_logger
from cki_lib.misc import is_production_or_staging
from gitlab.exceptions import GitlabCreateError
from gitlab.v4.objects.groups import Group
from gitlab.v4.objects.labels import GroupLabel
from gitlab.v4.objects.labels import ProjectLabel
from gitlab.v4.objects.projects import Project

from webhook import defs
from webhook.cache import BaseCache
from webhook.common import load_yaml_data

if typing.TYPE_CHECKING:
    import pathlib.Path

    from webhook.rh_metadata import Project as RH_Project
    from webhook.session import BaseSession

LOGGER = get_logger('cki.webhook.labels')

GLLabel = typing.Union[GroupLabel, ProjectLabel]
FilePath = typing.Union['pathlib.Path', str]
LabelsYamlData = typing.Set['YamlLabel']


@dataclasses.dataclass(frozen=True)
class YamlLabel:
    """Label data from yaml."""

    name: str
    color: str
    description: str = ''
    devaction: str = ''
    regex: typing.Optional[re.Pattern] = None
    regex_field_id: typing.Optional[int] = None

    @classmethod
    def new(cls, yaml_dict: typing.Dict) -> typing.Self:
        """Construct a YamlLabel instance."""
        return cls(**yaml_dict | {'regex': re.compile(yaml_dict['name'])}) if \
            yaml_dict.get('regex') else cls(**yaml_dict)

    def match(self, label_name: str) -> typing.Union[typing.Self, None]:
        """Return a copy with an updated name/description if label_name matches, otherwise None."""
        if label_name == self.name:
            return self
        if match := self.regex.match(label_name) if self.regex else None:
            return dataclasses.replace(
                self, name=label_name, description=self.description.replace('%s', match.group(1))
            )
        return None


def get_labels_yaml_data(labels_yaml_path: FilePath) -> LabelsYamlData:
    """Return the dict of label definitions from the given labels_yaml_path."""
    yaml_data = load_yaml_data(labels_yaml_path)
    if not yaml_data or not yaml_data.get('labels'):
        raise RuntimeError(f'No label data found in {labels_yaml_path}')
    return {YamlLabel.new(label_dict) for label_dict in yaml_data['labels']}


class LabelsCache(BaseCache):
    """A cache of Label data."""

    # This makes more sense as a set but that bumps into a python-gitlab quirk:
    # https://github.com/python-gitlab/python-gitlab/pull/2811
    _data_type = list

    def __init__(
        self,
        session: 'BaseSession',
        labels_yaml_path: typing.Optional[FilePath] = None,
        **kwargs
    ) -> None:
        """Set and load the labels_yaml attribute."""
        self.session = session
        self.labels_yaml = get_labels_yaml_data(labels_yaml_path or defs.LABELS_YAML_PATH)
        super().__init__(**kwargs)

    def _populate(
        self,
        namespaces: typing.Optional[typing.Iterable[str]] = None,
        check_labels: bool = True,
        prune_labels: bool = False,
        **_
    ) -> typing.Set[GLLabel]:
        """Return the set of all valid GL Label objects for the given namespaces."""
        if namespaces:
            rh_projects = [self.session.rh_projects.get_project_by_namespace(ns) for
                           ns in namespaces]
        else:
            rh_projects = self.session.rh_projects.get_projects_by_environment()

        data = []
        for gl_obj in set(self.get_gl_obj(self.session, rh_project) for rh_project in rh_projects):
            obj_labels = \
                typing.cast(typing.List[GLLabel], gl_obj.labels.list(get_all=True, per_page=100))
            if check_labels or prune_labels:
                checked_labels = []
                for gl_label in obj_labels:
                    if updated_label := self.check_gl_label(gl_label, prune=prune_labels):
                        checked_labels.append(updated_label)
                obj_labels = checked_labels
            LOGGER.info('Found %d labels for %s.', len(obj_labels), repr(gl_obj))
            data.extend(obj_labels)
        return data

    def get_label_from_yaml(self, label_name: str) -> typing.Union[YamlLabel, None]:
        """Return the first YamlLabel in labels_yaml that matches, otherwise None."""
        for yaml_label in self.labels_yaml:
            if label := yaml_label.match(label_name):
                return label
        return None

    def check_gl_label(self, gl_label: GLLabel, prune: bool = False) -> typing.Union[GLLabel, None]:
        """Check the given Gitlab label matches the labels.yaml definition and fix it if asked."""
        if not (yaml_label := self.get_label_from_yaml(gl_label.name)):
            if prune:
                LOGGER.info("Pruning unexpected label '%s' (%s)", gl_label.name, gl_label.id)
                if is_production_or_staging():
                    gl_label.delete()
            else:
                LOGGER.warning('Ignoring unexpected label %s', repr(gl_label))
            return None
        LOGGER.debug("Found yaml match for label '%s'", gl_label.name)
        return self.update_label(gl_label, yaml_label)

    def rh_project_labels(self, rh_project: 'RH_Project') -> typing.List[GLLabel]:
        """Return a list of the labels in the cache which are relevant to the given rh_project."""
        if rh_project.group_labels:
            label_id_attr = 'group_id'
            # python-gitlab sets up group_id & project_id as strings.
            gl_id = str(rh_project.group_id)
        else:
            label_id_attr = 'project_id'
            gl_id = str(rh_project.id)
        return [label for label in self.data if getattr(label, label_id_attr, None) == gl_id]

    def get_label(
        self,
        rh_project: 'RH_Project',
        label_name: str,
        create_missing: bool = False
    ) -> typing.Union[GLLabel, None]:
        """Return the given label from the cache; creating it first if it does not seem to exist."""
        rh_project_labels = self.rh_project_labels(rh_project)
        gl_label = next((label for label in rh_project_labels if label.name == label_name), None)
        if not gl_label and create_missing:
            gl_label = self.create_label(rh_project, label_name)
            self.data.append(gl_label)
        return gl_label

    @staticmethod
    def get_gl_obj(sess: 'BaseSession', rh_proj: 'RH_Project') -> typing.Union['Group', 'Project']:
        """Return the Gitlab object needed for labeling of the given rh_project."""
        if rh_proj.group_labels:
            gl_obj = sess.get_gl_group(rh_proj.group_namespace)
        else:
            gl_obj = sess.get_gl_project(rh_proj.namespace)
        LOGGER.debug('%s uses labels from %s', rh_proj, repr(gl_obj))
        return gl_obj

    def create_label(self, rh_project: 'RH_Project', label_name: str) -> GLLabel:
        """Create the requested label in the provided RH_Project and return it."""
        if not (yaml_label := self.get_label_from_yaml(label_name)):
            raise ValueError(f"'{label_name}' is not a valid label name")
        if gl_label := self.get_label(rh_project, label_name, create_missing=False):
            LOGGER.warning("Label '%s' already exists, nothing to create.", repr(gl_label))
            return gl_label
        gl_obj = self.get_gl_obj(self.session, rh_project)
        if is_production_or_staging():
            try:
                gl_label = gl_obj.labels.create(
                    {'name': yaml_label.name,
                     'description': yaml_label.description,
                     'color': yaml_label.color}
                )
            except GitlabCreateError as err:
                if err.response_code != 409:  # 409 means a label with this name already exists.
                    raise
                LOGGER.warning("Label '%s' already exists? Nothing to create.", label_name)
                return gl_obj.labels.get(label_name)
        else:
            gl_label = self._fake_gl_label(gl_obj, yaml_label)
        LOGGER.info('Created %s', repr(gl_label))
        return gl_label

    @staticmethod
    def update_label(gl_label: GLLabel, yaml_label: YamlLabel) -> GLLabel:
        """Update the given Gitlab label as needed to match the yaml."""
        needs_fixing = False
        for attr in ('color', 'description'):
            gl_label_attr = getattr(gl_label, attr)
            yaml_label_attr = getattr(yaml_label, attr)
            if gl_label_attr != yaml_label_attr:
                LOGGER.warning("'%s' %s does not match: gl: '%s', yaml: '%s'", gl_label.name, attr,
                               gl_label_attr, yaml_label_attr)
                needs_fixing = True
                setattr(gl_label, attr, yaml_label_attr)
        if needs_fixing:
            LOGGER.info('Fixing %s', repr(gl_label))
            if is_production_or_staging():
                gl_label.save()
        return gl_label

    @staticmethod
    def _fake_gl_label(gl_obj: typing.Union[Group, Project], y_label: YamlLabel) -> GLLabel:
        """Return a dummy GL group or project label object."""
        label_class = GroupLabel if isinstance(gl_obj, Group) else ProjectLabel
        LOGGER.debug('Creating fake %s from yaml: %s', label_class.__name__, y_label)
        return label_class(
            manager=gl_obj.manager,
            attrs={'name': y_label.name,
                   'color': y_label.color,
                   'description': y_label.description,
                   'fake': True}
        )
